/*
fix-display-google-ads

e.g.
	https://www.thefreedictionary.com/haketilo
*/

/* Goal: to open an iframe with URL `https://googleads.g.doubleclick.net/pagead/ads (removing values as I add them to the code)
	&client=ca-pub-2694630391511205
	&output=html
	&h=90
	&slotname=5649217835
	&adk=1845851797
	&adf=2644933334
	&pi=t.ma~as.5649217835
	&w=578
	&lmt=1677012517
	&rafmt=12
	&channel=4658665838%2B8899949764
	&format=578x90
	&url=https%3A%2F%2Fwww.thefreedictionary.com%2Fhaketilo
	&kw_type=broad
	&kw=haketilo
	&wgl=1
	&dt=1677012516362
	&bpp=1
	&bdt=2200
	&idt=150
	&shv=r20230215
	&mjsv=m202302130101
	&ptt=9
	&saldr=aa
	&abxe=1
	&correlator=1581098294662
	&frm=20
	&pv=2
	&ga_vid=128977250.1677012469
	&ga_sid=1677012518
	&ga_hid=129210652
	&ga_fc=1
	&u_tz=0
	&u_his=2
	&u_h=350
	&u_w=1000
	&u_ah=350
	&u_aw=1000
	&u_cd=24
	&u_sd=1
	&adx=87
	&ady=206
	&biw=1000
	&bih=350
	&scr_x=0
	&scr_y=488
	&eid=44759876%2C44759842%2C44759927%2C44777877%2C44774292%2C44779794%2C31072500
	&oid=2
	&pvsid=3727594060616752
	&tmod=541013427
	&nvt=2
	&ref=https%3A%2F%2Fwww.thefreedictionary.com%2F
	&fc=896
	&brdim=0%2C0%2C0%2C0%2C1000%2C0%2C1000%2C350%2C1000%2C350
	&vis=1
	&rsz=%7C%7CoeE%7C
	&abl=CS
	&pfx=0
	&fu=256
	&bc=31
	&ifi=1
	&uci=a!1
	&fsb=1
	&xpc=NQBvNDTlsv
	&p=https%3A//www.thefreedictionary.com
	&dtd=1184`
	*/

function getClientVal(scriptText) {
	let arr = [];
	const it = scriptText.matchAll("ca-pub-[^\\\\]*")
	for (val of it) {
		arr.push(val[0]);
	}
	// TODO: get a random value out of arr?
}

/* gdpr is set (I assume) depending on whether the user and website are in GDPR
	jurisdiction. clientVal is a value from the text of
	`fetch("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js")`
	(The referrer matters! Opening that URL in your browser as a normal page
	will not show the values; you have to use fetch. At least it seems like you
	don't need to use cors-bypass.) */
function constructFrameLink(gdpr, clientVal) {
	let url = new URL("https://googleads.g.doubleclick.net/pagead/ads");
	if (gdpr == true) {
		url.searchParams.set("gdpr", 1);
		url.searchParams.set("gdpr_consent", getCookie("euconsent-v2"));
		return null; // TODO: make sure they actually consented?
	} else {
		url.searchParams.set("gdpr", 0);
	}
	url.searchParams.set("client", clientVal);
	url.searchParams.set("output", "html");
	return url.toString();
}
