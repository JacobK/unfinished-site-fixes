// for use on https://web.archive.org/web/20150217224303/https://www.cardpartner.com/app/fsf

for (imgElem of document.getElementsByTagName("img")) {
  if (imgElem.getAttribute("ssgsrc")) {
    imgElem.src = imgElem.getAttribute("ssgsrc");
  }
}
