/*
checkout-roller-app
https://checkout.roller.app/**
e.g.
https://checkout.roller.app/battlehouselasertagplano/products/battlecheckout#/sessions/payment
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// From https://stackoverflow.com/a/15724300 , by kirlich (https://stackoverflow.com/users/7635/kirlich) and Arseniy-II (https://stackoverflow.com/users/8163773/arseniy-ii), licensed CC BY-SA 4.0
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function genUUID() {
	return crypto.randomUUID();
}

const correlationId = genUUID();
const group = document.URL.split('/')[5].split('#')[0];

async function configurations() {
	const response = await fetch(`https://api.roller.app/api/venues/configurations?group=${group}`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:122.0) Gecko/20100101 Firefox/122.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Api-Key": getCookie("Current-Venue"),
		    "x-origin-id": "1",
		    "x-cell-id": "a",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://checkout.roller.app/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function costSettings() {
	const response = await fetch("https://api.roller.app/api/costSettings", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:122.0) Gecko/20100101 Firefox/122.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Api-Key": getCookie("Current-Venue"),
		    "X-CorrelationId": correlationId,
		    "X-Cell-Id": "a",
		    "x-origin-id": "1",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://checkout.roller.app/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// dateString is in YYYYMMDD format
async function availabilities(dateString) {
	const response = await fetch(`https://api.roller.app/api/products/availabilities/widget?endDateIndex=${dateString}&group=${group}&startDateIndex=${dateString}`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:122.0) Gecko/20100101 Firefox/122.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Api-Key": getCookie("Current-Venue"),
		    "X-CorrelationId": correlationId,
		    "X-Cell-Id": "a",
		    "x-origin-id": "1",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://checkout.roller.app/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function me() {
	const response = await fetch("https://api.roller.app/api/venues/me", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:122.0) Gecko/20100101 Firefox/122.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Api-Key": getCookie("Current-Venue"),
		    "X-CorrelationId": correlationId,
		    "X-Cell-Id": "a",
		    "x-origin-id": "1",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://checkout.roller.app/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return reponseJson;
}
/* sessionStart of 1830 is 6:30, sessionEnd of "1930" is 7:30, presumably; no
	idea why one is quoted and one isn't. phoneNumber is a string with no
	dashes, etc. */
async function createBooking(firstName, lastName, phoneNumber, emailAddress, postalCode, acceptMarketing = false, productId, bookingDate, sessionStart, sessionEnd) {
	const reponse = await fetch("https://api.roller.app/api/bookings/draft/create", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:122.0) Gecko/20100101 Firefox/122.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "X-Api-Key": getCookie("Current-Venue"),
		    "X-CorrelationId": correlationId,
		    "X-Cell-Id": "a",
		    "x-origin-id": "1",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://checkout.roller.app/",
		"body": JSON.stringify({
		  "user": {
			"firstName": firstName,
			"lastName": lastName,
			"phone": phoneNumber,
			"email": emailAddress,
			"postCode": "0",
			"acceptMarketing": false
		  },
		  "orders": [
			{
			  "quantity": 1,
			  "productId": 915217,
			  "bookingDate": "20240216",
			  "costModifiers": [],
			  "sessionStart": 1830,
			  "sessionEnd": "1930"
			}
		  ],
		  "discounts": [],
		  "costModifiers": null,
		  "bookingTimerId": null,
		  "payFullAmountIgnoringDeposit": false,
		  "giftcards": [],
		  "timeTakenMilliseconds": 233153.73,
		  "consumerWidgetGroupId": 18119,
		  "formResponses": [],
		  "paymentTypeId": 0
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
