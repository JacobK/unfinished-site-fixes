/*
	identifier: tiktok-comments
	long name: Comments for TikTok
	description: show replies to comments on TikTok videos
	URL pattern: 
*/

/*
	There's something strange about how requests are made in the official web
	app. The referrer is set to "https://www.tiktok.com/" instead of the page
	you're actually on, and copying an API request as fetch() and pasting it in
	the console (while still in the context of the proprietary scripts or not)
	results in an OPTIONS (maybe "preflight"?) request and then a CORS error. I
	can't figure out how to make the request from JavaScript properly (but you
	can right click and resend the GET request in the network tab and that will
	not give the error (but the page or console JavaScript can't see that)).
*/


//const itemId = location.href.split("/")[5];  // might be less reliable

const sigiState = JSON.parse(document.getElementById("SIGI_STATE").textContent);
const itemId = Object.entries(sigiState.ItemModule)[0][0];
const userId = Object.entries(sigiState.ItemModule)[0][1].textExtra[0].userId;

let lastResponseValue = null; // for debugging

// get the replies to a comment
function getCommentReplies(commentId) {
	fetch("https://us.tiktok.com/api/comment/list/reply/?aid=1988&app_language=ja-JP&app_name=tiktok_web&browser_language=en-US&browser_name=Mozilla&browser_online=true&browser_platform=Linux%20x86_64&browser_version=5.0%20%28X11%29&channel=tiktok_web&comment_id="+
	commentId+
	"&cookie_enabled=true&count=3&current_region=JP&cursor=0&device_id="+
	userId+
	"&device_platform=web_pc&focus_state=true&fromWeb=1&from_page=video&history_len=3&is_fullscreen=false&is_page_visible=true&item_id="+
	itemId+
	"&os=linux&priority_region=&referer=&region=US&screen_height=768&screen_width=1366&tz_name=America%2FChicago&webcast_language=en", {
		"credentials": "include",
		"headers": {
			"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:98.0) Gecko/20100101 Firefox/98.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://www.tiktok.com/", /* The referrer is notably NOT
			the page I was on, which looked more like
*///			"https://www.tiktok.com/@*/video/*". */
		"method": "GET",
		"mode": "cors"
	}).then(async function(response){
		responseJson = await response.json();
		lastResponseValue = responseJson;
		responseJson.comments.forEach(function(comment){
			console.log(comment.user.nickname, comment.text);
		});
	});
}

document.querySelectorAll(".tiktok-1oxhn3k-PReplyActionText").forEach(function(element){
	element.addEventListener("click", function(){
		getCommentReplies(element.parentElement.parentElement.parentElement.querySelectorAll(".tiktok-14tia6e-DivCommentContentContainer")[0].id);
	});
});
