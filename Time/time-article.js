/*
	identifier: time-article
	long name: Articles for Time
	description: Allow reading articles on time.com
	URL pattern: https://time.com/**
	example URLs:
		https://time.com/6173639/democracy-big-techs-dominance-shoshana-zuboff/
		https://time.com/4458554/best-video-games-all-time/
	comment: Strangely, https://time.com/6173639/ and https://time.com/4458554/ are valid URLs as well, and they shows the same stories in a way that actually reads well without JavaScript. Perhaps it would be better to redirect there so that when people share links they work without Haketilo.
*/

// Revert the "position" CSS attribute from "fixed".
document.body.style.position = "revert";

// TODO: properly show images here: https://time.com/4458554/best-video-games-all-time/


