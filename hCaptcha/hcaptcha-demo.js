/*
hcaptcha-demo
https://accounts.hcaptcha.com/demo
*/

const assetUrl = "https://newassets.hcaptcha.com/captcha/v1/b1c18b1/static"; // Can be found as `assetUrl:"https://newassets.hcaptcha.com/captcha/v1/b1c18b1/static"` in https://js.hcaptcha.com/1/api.js but maybe not if the URL query string contains "custom=True".
//const recaptchaCompat = "true"; // also in api.js

const siteKey = document.querySelector("div.h-captcha").getAttribute("data-sitekey"); // assumes site key is the same for all divs on a page
const siteOrigin = document.URL.split("/").slice(0,3).join("/"); // can probably be reused on any site!

let apiJsUrl = null;
for (script of document.scripts) {
	const match = script.src.match(/https:\/\/js.hcaptcha.com\/1\/api.js/);
	if (match) {
		apiJsUrl = script.src;
	}
}
const apiJsParams = new URLSearchParams(apiJsUrl.split("?")[1]);

// The "id" value in the iframe URLs seem to be different every time, even though the HTML, JavaScript, and JSON leading up to it seem to be consistent. Thus, I conclude that it is either generated randomly, or generated like an OTP code, in a way that the server knows what the code should be. I'm guessing it's the former.
// example IDs (in chronological order): ["02m92yr33lfl", "03pygb1i3iv9", "0nceqtjwt65", "0lbdo83rok5p", "06ojhj1ubrf", "0rj4m25qpxdl", "0e0ldsa1f4ju", "0txa5nixytxj", "09cllnm4q4ai", "0tlmlrtoxy8", "0i74euiyes7l", "0tdce6d808yr", "0ttbnmojq8j", "06r9dq0ilupo", "0or2cqiij9gb", "0pr3gihljewq", "0y5bgasdl7w"]
// `new URLSearchParams(document.querySelector("iframe").src.split("#")[1]).get("id");` can get the ID once you've already run the nonfree code
// Some are length 12, some are length 11.
/*
ids = ["02m92yr33lfl", "03pygb1i3iv9", "0nceqtjwt65", "0lbdo83rok5p", "06ojhj1ubrf", "0rj4m25qpxdl", "0e0ldsa1f4ju", "0txa5nixytxj", "09cllnm4q4ai", "0tlmlrtoxy8", "0i74euiyes7l", "0tdce6d808yr", "0ttbnmojq8j", "06r9dq0ilupo", "0or2cqiij9gb", "0pr3gihljewq", "0y5bgasdl7w"];
for (id of ids) {
	let line = `${id}\t${id.length}`;
  const decoded = atob(id);
	line += `\t${decoded}`;
	for (i = 0; i < decoded.length; i++) {
		line += `\t${decoded.charCodeAt(i)}`;
	}
	console.info(line);
}
*/

/*
These IDs were harvested by running the nonfree hCaptcha client, and the
code after the IDs gets a list of chars contained in the ids.
*/
const exampleIds = [
  "02m92yr33lfl",
  "03pygb1i3iv9",
  "0nceqtjwt65",
  "0lbdo83rok5p",
  "06ojhj1ubrf",
  "0rj4m25qpxdl",
  "0e0ldsa1f4ju",
  "0txa5nixytxj",
  "09cllnm4q4ai",
  "0tlmlrtoxy8",
  "0i74euiyes7l",
  "0tdce6d808yr",
  "0ttbnmojq8j",
  "06r9dq0ilupo",
  "0or2cqiij9gb",
  "0pr3gihljewq",
  "0y5bgasdl7w",
  "0gqh2a2za3xi" // from Discord, much later than the example before
]
function getAllChars(strArr) {
	let allChars = "";
	for (str of strArr) { // for every example id
		for (let i = 0; i < str.length; i++) { // iterate through every char in the id
			let inAllChars = false;
			// make sure the char isn't already in allChars
			for (let j = 0; j < allChars.length; j++) {
				if (allChars.at(j) == str.at(i)) {
					inAllChars = true;
					break; // exit the inner loop
				}
			}
			if (!inAllChars) {
				allChars += str.at(i);
			}
		}
	}
	return allChars;
}
const allChars = getAllChars(exampleIds);

// function from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}




function generateId() {
	const length = getRandomIntInclusive(11, 12);
	let idString = "0";
	for (let i = 1; i < length; i++) {
		idString += allChars.at(getRandomIntInclusive(0, allChars.length - 1));
	}
	return idString;
}

const id = generateId();

window.addEventListener("message", (event) => {
	if (event.origin !== "https://newassets.hcaptcha.com") {
		console.error("Bad origin:", event.origin);
		console.error("message missed:", event);
		return;
	}
	
	if (false) {
		// do something
	} else {
		console.error("unknown message:", event);
	}
	
}, false);

// There are 2 iframes, and since one of them is related directly to the document body, I'm assuming that it is global. For debugging, we will load both iframes and log al messages.

// initialize the assumed-to-be-one-per-page iframe
function initGlobalIframe() {
	const frame = document.createElement("iframe");
	frame.src = `${assetUrl}/hcaptcha.html#frame=challenge&id=${id}&host=${document.domain}&sentry=true&reportapi=${encodeURIComponent(apiJsParams.get("reportapi"))}&recaptchacompat=true&custom=${apiJsParams.get("custom")}&hl=en&tplinks=on&sitekey=${siteKey}&theme=light&origin=${encodeURIComponent(siteOrigin)}`;
	document.body.appendChild(frame);
	return frame;
}

// initialize a probably one-per-form iframe
function initLocalIframe(div) {
	const formElement = div.closest("form");
	const frame = document.createElement("iframe");
	frame.src = `${assetUrl}/hcaptcha.html#frame=checkbox&id=${id}&host=${document.domain}&sentry=true&reportapi=${encodeURIComponent(apiJsParams.get("reportapi"))}&recaptchacompat=true&custom=${apiJsParams.get("custom")}&hl=en&tplinks=on&sitekey=${siteKey}&theme=light&origin=${encodeURIComponent(siteOrigin)}`;
	hcaptchaDiv.appendChild(frame); /* Should this be `div` instead of
		`hcaptchaDiv`? Or maybe `formElement`? */
	return frame;
}


const hcaptchaGlobalIframe = initGlobalIframe();
for (hcaptchaDiv of document.querySelectorAll("div.h-captcha")) {
	const hcaptchaIframe = initLocalIframe(hcaptchaDiv);
}

