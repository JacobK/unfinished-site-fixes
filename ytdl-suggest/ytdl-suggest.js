/*
ytdl-suggest
https://jbilcke-hf-ai-tube.hf.space/watch
*/

/* Much easier than writing code specific to each site, we can instruct the user
	to use youtube-dl for sites that we know work with it. Perhaps in the future
	we could have a button to launch mpv with the video URL. This could be a 
	value of youtube-dl tracking sites the generic extractor works on, as
	suggested at
	<https://libreddit.miaoute.net/r/youtubedl/comments/efn91p/has_anyone_compiled_a_list_of_every_website/fc1j8se/?context=3> */

document.write(`Use the following command to download the video:
<br>
yt-dlp --write-description --write-info-json --write-annotations --write-thumbnail --sub-langs ${navigator.language.split("-")[0]} --write-subs --write-auto-sub --write-comments --sponsorblock-mark all "${document.URL}"`);
