/*
gethuman-search-fix
https://gethuman.com/
https://gethuman.com/search/*
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This event also seems to be triggered when pressing enter from the text box
document.querySelector('form.search[action="/search"]').querySelector("button").addEventListener("click", function(ev){
	document.location.href = `/search/${ev.target.closest("form").querySelector('input[name="q"]').value}`;
	ev.preventDefault();
})
