// run at https://incompetech.com/music/royalty-free/isrc_to_name.php

// getRandomInt is from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
// licensed under CC0 or MIT (unclear, see https://developer.mozilla.org/en-US/docs/MDN/About#Copyrights_and_licenses)
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function getRandomLink() {
	return document.getElementsByTagName("a")[getRandomInt(document.getElementsByTagName("a").length)].href;
}

getRandomLink(); // TODO: map this to a button on the page
