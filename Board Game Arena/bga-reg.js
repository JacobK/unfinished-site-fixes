/*
bga-reg
https://en.boardgamearena.com/
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

function getRequestToken() {
	for (script of document.scripts) {
		let match = script.textContent.match(/requestToken: '(.*)'/);
		if (match) {
			return match[1];
		}
	}
}

async function registerAccount(emailAddress, username, password) {
	const response = await fetch("https://en.boardgamearena.com/account/account/register_email.html", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:125.0) Gecko/20100101 Firefox/125.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Request-Token": getRequestToken(),
		    "Content-Type": "application/x-www-form-urlencoded",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://en.boardgamearena.com/",
		"body": `email=${formUrlEncode(emailAddress)}&username=${formUrlEncode(username)}&password=${formUrlEncode(password)}a&takeover=true&takeover_username=&dojo.preventCache=${new Date().getTime()}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function submitRegisterForm() {
	const regAttempt = await registerAccount(document.getElementById("register_email").value, document.getElementById("register_username").value, document.getElementById("register_password").value);
	if (regAttempt.data.success == true) {
		document.location = regAttempt.redirect;
	} else if (regAttempt.error) {
		alert(regAttempt.error);
	} else {
		console.error(regAttempt);
	}
}

document.getElementById("main-content").style.visibility = "revert";

document.getElementById("register_button").addEventListener("click", submitRegisterForm);
