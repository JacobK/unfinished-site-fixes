// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

function getRequestToken() {
	for (script of document.scripts) {
		let match = script.textContent.match(/requestToken: '(.*)'/);
		if (match) {
			return match[1];
		}
	}
}

async function login(emailAddress, password) {
	await fetch("https://en.boardgamearena.com/account/account/login.html", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:125.0) Gecko/20100101 Firefox/125.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Request-Token": getRequestToken(),
		    "Content-Type": "application/x-www-form-urlencoded",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://en.boardgamearena.com/account",
		"body": `email=${formUrlEncode(emailAddress)}&password=${formUrlEncode(password)}&rememberme=on&redirect=join&request_token=${redacted}&form_id=loginform&dojo.preventCache=1716749517812`,
		"method": "POST",
		"mode": "cors"
	});
}


async function resetPassword(emailAddress) {
	const response = await fetch(`https://en.boardgamearena.com/account/account/lostpassword.html?email=${encodeURIComponent(emailAddress)}&dojo.preventCache=${new Date().getTime()}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:125.0) Gecko/20100101 Firefox/125.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Request-Token": getRequestToken(),
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://en.boardgamearena.com/account?page=lostpassword",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
