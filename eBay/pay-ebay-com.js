/*
pay-ebay-com
https://pay.ebay.com/rgxo
*/

// SPDX-FileCopyrightText: 2023-2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

function getSessionId() {
	for (script of document.scripts) {
		const match = script.textContent.match(/"sessionId":"([^"]*)"/);
	  if (match) {
		return match[1];
	  }
	}
	return document.getElementById("clientSideExperiments").getAttribute("data-si");
}

function getAppInitialState() {
	for (script of document.scripts) {
		const regMatch = script.textContent.match(/window.__APP_INITIAL_STATE__ = ({.*)/);
		if (regMatch) {
			return JSON.parse(regMatch[1]);
		}
	}
}
const appInitialState = getAppInitialState();

/* countryCode is e.g. "US". phoneNumber has no dashes or parenthesis, and it
	does not have the +1 at the front, but it is a string. phoneCountryCode is
	e.g. "US". */
async function addAddress(emailAddress, firstName, lastName, countryCode, city, zipCode, phoneNumber, phoneCountryCode, stateCode, addressLineOne, addressLineTwo = "") {
	const response = await fetch("https://pay.ebay.com/rgxo/ajax?action=addAddress", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://pay.ebay.com/rgxo?action=view&sessionid=1449359949",
		"body": JSON.stringify({
			"emailConfirm": emailAddress,
			"lastName": lastName,
			"country": countryCode,
			"city": city,
			"addressType": "SHIPPING",
			"postalCode": zipCode,
			"makePrimary": "false",
			"firstName": firstName,
			"phoneNumber": phoneNumber,
			"stateOrProvince": stateCode,
			"phoneCountryCode": phoneCountryCode,
			"disableValidation": "false",
			"addressLine1": addressLineOne,
			"addressLine2": addressLineTwo,
			"email": emailAddress,
			"sessionid": getSessionId(),
			"srt": appInitialState.context.logisticsContext.srt,
			"pageType": "ryp"
		}),
		"method": "POST",
		"mode": "cors"
	});
}

/* zipCode is a string btw, 5 digits. cardExpiryDate is in the form
	"YYYY-MM-01". A lot of this information was automatically copied by the
	official client app, but shippingSameAsBilling was set to false anyway.
	cardNumber is a string with no spaces. */
async function addPaymentMethod(emailAddress, countryCode, city, zipCode, lastName, cardExpiryDate, phoneNumber, addressLineOne, stateCode, firstName, cardNumber, securityCode, addressLineTwo = "") {
	const response = await fetch("https://pay.ebay.com/rgxo/ajax?action=addPaymentInstrument", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://pay.ebay.com/rgxo?action=view&sessionid=1449359949",
		"body": JSON.stringify({
			"emailConfirm": emailAddress,
			"country": countryCode,
			"city": city,
			"addressType": "BILLING",
			"postalCode": zipCode,
			"cardHolderLastName": lastName,
			"cardExpiryDate": cardExpiryDate,
			"phoneNumber": phoneNumber,
			"paymentMethodId": "CC",
			"addrLine1": addressLineOne,
			"addrLine2": addressLineTwo,
			"state": stateCode,
			"cardHolderFirstName": firstName,
			"shippingSameAsBilling": "false",
			"email": emailAddress,
			"cardNumber": cardNumber,
			"securityCode": securityCode,
			"": "",
			"srt": appInitialState.context.logisticsContext.srt,
			"pmMethod": "CC",
			"sessionid": getSessionId(),
			"pageType": "ryp"
		}),
		"method": "POST",
		"mode": "cors"
	});
}

/* should be equivalent to unchecking the box that says
	"Get exclusive offers and promotions from eBay." */
async function disableMarketing() {
	await fetch("https://pay.ebay.com/rgxo/ajax?action=switchMarketingOptin", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://pay.ebay.com/rgxo?action=view&sessionid=1449359949",
		"body": JSON.stringify({
			"marketingOptIn": false,
			"sessionid": getSessionId(),
			"srt": appInitialState.context.logisticsContext.srt,
			"pageType": "ryp"
		}),
		"method": "POST",
		"mode": "cors"
	});
}

// function not tested
async function confirmCheckout() {
	const formElement = document.getElementById("page-form");
	formElement.action = `https://pay.ebay.com/rgxo?action=confirm&sessionid=${getSessionId()}`;
	formElement.submit();
}
