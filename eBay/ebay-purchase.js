/*
ebay-purchase
https://www.ebay.com/itm/***
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

let srt = null;
for (script of document.scripts) {
	const regMatch = script.textContent.match(/"shopactionsview_act_bin":"([^"]*)",/);
	if (regMatch) {
		srt = regMatch[1];
	}
}

const splitUrl = document.URL.split("/");
let itemId = null;
if (splitUrl.length == 5) {
	itemId = splitUrl[4].split("?")[0];
} else if (splitUrl.length > 5) {
	itemId = splitUrl[5].split("?")[0];
} else {
	console.error("unknown URL pattern");
}

let viewCartSrt = null;
for (script of document.scripts) {
	const regMatch = script.textContent.match(/{"view_cartfe":"([^"]*)"}/);
	if (regMatch) {
		viewCartSrt = regMatch[1];
	}
}


const addOnOptions = [];
if (document.getElementById("vas-container")) {
	const vasFieldDiv = document.getElementById("vas-container");
	for (vasFieldItem of vasFieldDiv.querySelectorAll("div.vas-field-item")) {
		const viTrackingJson = JSON.parse(vasFieldItem.querySelector("button").getAttribute("data-vi-tracking"));
		addOnOptions.push(viTrackingJson.eventProperty.addOnType);
	}
}

async function checkoutWithItem() {
	const quantity = 1;
	const eligibleServices = {};
	const selectedServices = {};
	for (serviceOption of addOnOptions) {
		eligibleServices[serviceOption] = [""];
		selectedServices[serviceOption] = ["-1"];
	}
	const response = await fetch(`https://www.ebay.com/act/bin?srt=${srt}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "text/plain;charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://www.ebay.com/itm/144655451682",
		"body": JSON.stringify({
			"itemId": itemId,
			"variationId": null,
			"quantity": quantity,
			"eligibleServices": eligibleServices,
			"moduleGroups": "PERFORM_ACTION",
			"selectedServices": selectedServices,
			"defaultActionUrl": `https://cart.payments.ebay.com/sc/add?item=iid%3A${itemId}%2Cqty%3A${quantity}&srt=${viewCartSrt}&ssPageName=CART%3AATC`,
			"srt": srt
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function buyItNow() {
	const checkoutJson = await checkoutWithItem();
	document.location.assign(checkoutJson.meta.screenFlowDestination.URL);
}

async function newBuyItNow() {
	document.location.assign(`https://pay.ebay.com/rgxo?action=create&rypsvc=true&pagename=ryp&item=${itemId}&TransactionId=-1&rev=24&quantity=1&qty=1`);
}
