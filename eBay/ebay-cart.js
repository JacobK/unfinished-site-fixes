/*
ebay-cart
https://cart.payments.ebay.com/
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

function getCartSrt() {
	let srt = null;
	for (script of document.scripts) {
		const regMatch = script.textContent.match(/"sessionToken":"([^"]*)",/);
		if (regMatch) {
			srt = regMatch[1];
		}
	}
	return srt;
}

async function verifyCart() {
	const response = await fetch("https://cart.payments.ebay.com/api/xo", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://cart.payments.ebay.com/",
		"body": JSON.stringify({
			"enableEligibilityCheck": "true",
			"carttime": Date.now(),
			"srt": getCartSrt()
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function main() {
	verifyCart();
	document.location.assign("https://cart.payments.ebay.com//api/xo?guestCheckoutEligible=true");
}

// If you are using Tor, then you'll have to do an extra verification step that I've not written a script for (and which may be quite hard to write a script for)
