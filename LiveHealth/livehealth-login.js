/*
	identifier: livehealth-login
	long name: Login for LiveHealth Online
	description: allow logging in to LiveHealth Online
	URL pattern: https://startlivehealthonline.com/loginConsumer.htm
*/

async function getCsrf() {
	let token = null;
	// next 2 lines based on koszko's code
	await fetch('https://startlivehealthonline.com/touchSession.htm')
    .then(resp => token = resp.headers.get('csrf-token'));
    return token;
}

// We wrap everything in an async function so that we can use await
async function main() {

let csrfInput = document.createElement("input");
csrfInput.type = "hidden";
csrfInput.name = "csrfToken";
csrfInput.value = await getCsrf();
document.getElementsByName("loginPage")[0].insertAdjacentElement("afterend", csrfInput); 
/* The official script seems to dynamically add this element as you submit, but
	I don't think it really matters, as long as the request sends with the csrf
	token. */

// code copied from official script, makes the page visible
/* BEGIN COPIED CODE */
// Frame busting code, as recommended by "Busting Frame Busting: A Study of
	// Clickjacking Vulnerabilities on Popular Sites", by Rydstedt, Bursztein,
	// Boneh, and Jackson, May 25, 2010
	
		
		if (self == top) {
			document.documentElement.style.visibility = "visible";
		}
/* END COPIED CODE */
		else {
			console.log("Hey! You're not supposed to visit this page inside an iframe!");
			alert("Hey! You're not supposed to visit this page inside an iframe!");
		}

}

main();
