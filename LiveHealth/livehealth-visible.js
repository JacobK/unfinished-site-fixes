/*
	identifier: livehealth-visible
	long name: Show Pages on LiveHealth
	description: show pages on LiveHealth (for pages that don't need or don't
		have yet any other scripts)
	URL patterns:
		https://startlivehealthonline.com/app/mem/upcoming_appointments.htm
		https://startlivehealthonline.com/appointment.htm
		https://startlivehealthonline.com/landing.htm
*/

// code copied from official script, makes the page visible
/* BEGIN COPIED CODE */
// Frame busting code, as recommended by "Busting Frame Busting: A Study of
	// Clickjacking Vulnerabilities on Popular Sites", by Rydstedt, Bursztein,
	// Boneh, and Jackson, May 25, 2010
	
		
		if (self == top) {
			document.documentElement.style.visibility = "visible";
		}
/* END COPIED CODE */
		else {
			console.log("Hey! You're not supposed to visit this page inside an iframe!");
			alert("Hey! You're not supposed to visit this page inside an iframe!");
		}
