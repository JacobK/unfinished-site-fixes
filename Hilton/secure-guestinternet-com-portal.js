/*
secure-guestinternet-com-portal
https://secure.guestinternet.com/portal/juno/brands/hilton/alpha/index.html
*/

// note: may leak user agent even if anti-fingerprinting is enabled (I don't know if this is the case.)

const ewsKey = "KRELFQQIWWCJ0WZBFFOKHFGXUT1EPVEE"; // from <https://secure.guestinternet.com/portal/juno/brands/hilton/alpha/vendors~app.5d73ce.js> (`;var r="KRELFQQIWWCJ0WZBFFOKHFGXUT1EPVEE"},`)

const searchParams = new URLSearchParams(document.location.search);

async function getDeviceInfo() {
	const response = await fetch("https://login.globalsuite.net/Index", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0",
		    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Upgrade-Insecure-Requests": "1",
		    "Sec-Fetch-Dest": "document",
		    "Sec-Fetch-Mode": "navigate",
		    "Sec-Fetch-Site": "cross-site"
		},
		"method": "GET",
		"mode": "cors"
	});
	const responseUrl = response.url;
	const params = new URLSearchParams(responseUrl.split('?')[1]);
	return params;
}

async function loginWithAccessCode(accessCode) {
	const response = await fetch(`https://secure.11os.com/Guest/Portal/CreateUserViaAccessCode6JSONP?AccessCode=${encodeURIComponent(accessCode)}&AcctSessionId=&AuditSessionId=&CalledStationId=&EwsKey=${ewsKey}&GatewayId=${gatewayId}&LoginUrl=&LoginUser=true&NasDeviceIp=&PasswordOpt=&PortalSessionId=&UserInfo=${encodeURIComponent(navigator.userAgent)}&UserIpOpt=${userIp}&UserMac=${userMac}&UserNameOpt=&orgNumber=${orgNumber}&orgPageId=0&callback=angular.callbacks._9`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Alt-Used": "secure.11os.com",
		    "Sec-Fetch-Dest": "script",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "cross-site"
		},
		// no referrer
		"method": "GET",
		"mode": "cors"
	});
	const responseText = await response.text(); // response is in the form of JS
	const responseJson = JSON.parse(responseText.match(/{.*}/)[0]);
	return responseJson;
}

async function findClosestSite() {
	const response = await fetch("https://secure.11os.com/ElevenWS/DevApi/Dbo.svc/OrganizationDbo/FindClosestSiteAnonymous", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "text/raw",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "cross-site"
		},
		"body": `"${searchParams.get("ORG")}"`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function getManyData(siteNum) {
	const response = await fetch("https://secure.11os.com/ElevenWS/DevApi/Dbo.svc/OrgValueDbo/RetrieveManyAnonymous", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "text/raw",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "cross-site"
		},
		"body": `"${siteNum}"`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function loyaltyLoginStart(lastName, roomNumber) {
	const response = await fetch(`https://secure.11os.com/ElevenOS/WebApi/hilton/honors/Loyalty?AcctSessionId=&AuditSessionId=&CalledStationId=&EwsKey=${ewsKey}&GatewayId=${searchParams.get("ORG")}&LastName=${lastName}&LoginId=&LoginUrl=${encodeURIComponent(searchParams.get("login_url"))}&NasDeviceIp=&PortalSessionId=${searchParams.get("PSID")}&RoomNo=${roomNumber}&UserInfo=${encodeURIComponent(navigator.userAgent)}&UserIpOpt=${searchParams.get("SIP")}&UserMac=${searchParams.get("MA").replaceAll(":", "")}&orgNumber=${searchParams.get("ORG")}&orgPageId=0&callback=angular.callbacks._0`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Alt-Used": "secure.11os.com",
		    "Sec-Fetch-Dest": "script",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "cross-site"
		},
		"method": "GET",
		"mode": "cors"
	});
	const responseText = response.text();
	const responseJson = JSON.parse(responseText.match(/{.*}/)[0]);
	return responseJson;
}

async function createSession(guestData) {
	const response = await fetch(`https://secure.11os.com/ElevenOS/WebApi/hilton/honors/Create?AcctSessionId=&AuditSessionId=&CalledStationId=&EwsKey=${ewsKey}&Folio=&GatewayId=${searchParams.get("ORG")}&GuestData=${encodeURIComponent(JSON.stringify(guestData))}&LastName=&LoginUrl=${encodeURIComponent(searchParams.get("login_url"))}&LoginUser=true&MarketingContact=&MarketingOptIn=false&NasDeviceIp=&PasswordOpt=&PortalSessionId=${searchParams.get("PSID")}&PromoCodeOpt=&RoomNo=&ServicePlanCode=standard_bsg&ServiceQuantity=1&UserInfo=${encodeURIComponent(navigator.userAgent)}&UserIpOpt=${searchParams.get("SIP")}&UserMac=${searchParams.get("MA").replaceAll(":", "")}&UserNameOpt=&orgNumber=${searchParams.get("ORG")}&orgPageId=0&callback=angular.callbacks._1`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Alt-Used": "secure.11os.com",
		    "Sec-Fetch-Dest": "script",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "cross-site"
		},
		"method": "GET",
		"mode": "cors"
	});
	const responseText = response.text();
	const responseJson = JSON.parse(responseText.match(/{.*}/)[0]);
	return responseJson;
}

async function main() {
	const siteJson = await findClosestSite();
	const siteData = await getManyData(siteJson.Number);
//	const loginJson = await loyaltyLoginStart(yourLastName, yourRoomNumber);
//	if loginJson.LoggedIn is true, then you don't need to do anything else
	const sessionJson = await createSession(loginJson.guestdata);
	document.location.href = sessionJson.LandingPageUrl;
}
