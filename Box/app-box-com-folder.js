/*
app-box-com-folder
https://***.app.box.com/folder/*
*/

// SPDX-FileCopyrightText: 2023,2024 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

// This can be used now, with heavy dev console usage.

// From https://stackoverflow.com/a/15724300 , by kirlich (https://stackoverflow.com/users/7635/kirlich) and Arseniy-II (https://stackoverflow.com/users/8163773/arseniy-ii), licensed CC BY-SA 4.0
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

let prefetchedData = null; // This variable isn't actually used.
for (const script of document.scripts) {
    const match = /Box.prefetchedData = ({([^;]|[^}];)+})/.exec(script.textContent); // looks for "Box.prefetchedData = " in the script files and then grabs the json text after that.
    if (!match)
	continue;
    prefetchedData = JSON.parse(match[1]);
}

let config = null;
for (const script of document.scripts) {
    const match = /Box.config = ({([^;]|[^}];)+})/.exec(script.textContent); // looks for "Box.config = " in the script files and then grabs the json text after that.
    if (!match)
	continue;
    config = JSON.parse(match[1]);
}

let postStreamData = null;
for (const script of document.scripts) {
    const match = /Box.postStreamData = ({([^;]|[^}];)+})/.exec(script.textContent); // looks for "Box.postStreamData = " in the script files and then grabs the json text after that.
    if (!match)
	continue;
    postStreamData = JSON.parse(match[1]);
}

// get domain from URL
const domain = document.location.href.split("/")[2];

function getFolderId() {
	return document.URL.match(/https?:\/\/.*.app.box.com\/folder\/(\d*)/)[1];
}

const thisPageFolderId = getFolderId();

async function getFolderExtraInfo(folderId) {
	const response = await fetch(`https://${document.domain}/app-api/enduserapp/folder/${folderId}/extras`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
//		    "X-Box-Client-Name": "enduserapp",
//		    "X-Box-Client-Version": "21.56.2",
//		    "traceparent": [redacted string],
		    "Alt-Used": "utdallas.app.box.com",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": [redacted string],
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function getFolderMetadata(fileIdsArray) {
	let fileIdsString = "";
	for (id of fileIdsArray) {
		fileIdsString += `fileIDs[]=${id}&`;
	}
	fileIdsString = fileIdsString.slice(0,-1); // Remove last '&' character
	const response = await fetch(`https://${document.domain}/index.php?${fileIdsString}&rm=preview_get_files_metadata`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
//		    "X-Box-Client-Name": "enduserapp",
//		    "X-Box-Client-Version": "21.56.2",
//		    "X-Rep-Hints": "[pdf][jpg?dimensions=1024x1024&paged=false]",
//		    "traceparent": [redacted string],
//		    "Alt-Used": "utdallas.app.box.com",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": [redacted string],
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function getFolderItems(fileIdsArray) {
	let fileIdsString = "";
	for (id of fileIdsArray) {
		fileIdsString += `itemTypedIDs[]=${id}&`;
	}
	fileIdsString = fileIdsString.slice(0,-1); // Remove last '&' character
	const response = await fetch(`https://utdallas.app.box.com/app-api/enduserapp/folder/${thisPageFolderId}/items?${fileIdsString}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
//		    "X-Box-Client-Name": "enduserapp",
//		    "X-Box-Client-Version": "21.56.2",
//		    "traceparent": [redacted string],
//		    "Alt-Used": "utdallas.app.box.com",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": [string redacted],
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function setCsrf(typedIdsArr) {
	const response = await fetch("https://utdallas.app.box.com/app-api/sign-web/find-sign-metadata?populate_status=1", {
		"credentials": "include",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "application/json",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
//		    "X-Box-Client-Name": "enduserapp",
//		    "X-Box-Client-Version": "21.132.0",
		    "Request-Token": config.requestToken,
		    "X-Request-Token": config.requestToken,
		    "x-csrf-token": getCookie("csrf-token"),
//		    "traceparent": [redacted string],
		    "Alt-Used": "utdallas.app.box.com",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://utdallas.app.box.com/folder/0",
		"body": JSON.stringify({
			"typedIds": typedIdsArr // e.g. ["d_############","d_############","d_############"] (with '#' replaced with various digits)
		}),
		"method": "POST",
		"mode": "cors"
	});
	return getCookie("csrf-token");
}

// fileId is a string, without "f_" at the beginning
async function getTokens(fileId) {
	const response = await fetch("https://utdallas.app.box.com/app-api/enduserapp/elements/tokens", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "application/json",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
//		    "X-Box-Client-Name": "enduserapp",
//		    "X-Box-Client-Version": "21.132.0",
		    "Request-Token": config.requestToken,
		    "X-Request-Token": config.requestToken,
		    "x-csrf-token": getCookie("csrf-token"),
//		    "traceparent": [redacted string],
		    "Alt-Used": "utdallas.app.box.com",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": `https://utdallas.app.box.com/file/${[redacted string]}`,
		"body": JSON.stringify({
			"fileIDs": [
				fileId
			]
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}


// fileId is without "f_" at the beginning; fileVersion can be 0
function getDlUrl(fileId, fileVersion, accessToken) {
	return `https://dl.boxcloud.com/api/2.0/files/${fileId}/content?preview=true&version=${fileVersion}&access_token=${accessToken}&box_client_name=box-content-preview&box_client_version=2.102.0&encoding=gzip`
}
