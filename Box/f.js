// use with https://***.app.box.com/f/*
// These are file upload pages rather than general folders
// See HAR file from 2022-07-09 (private)

// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

let lastResponseValue = null; // for debugging

const formID = document.URL.split("/")[4];
const csrfToken = document.cookie.split(";")[0].split("=")[1];

// Gets information about the current user, probably not important 
function currentUser() {
	fetch(`https://${document.domain}/app-api/enduserapp/current-user`, {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json;version=1",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-CSRF-TOKEN": csrfToken, // TODO: refer to the cookie by name; the cookie is "csrf-token"
		    "X-Box-Client-Name": "forms",
		    "X-Box-Client-Version": "prod",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		    // Note: There is no referrer
		},
		"method": "GET",
		"mode": "cors"
	}).then(async function(response){
		let responseJson = await response.json();
		lastResponseValue = responseJson;
	});
}

// Get all the information about the form, but there's a lot of data and it's pretty complicated
function fileRequest() {
	fetch(`https://${document.domain}/app-api/file-request-web/public/file-request?urlId=${formID}`, {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json;version=1",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-CSRF-TOKEN": csrfToken, // TODO: refer to the cookie by name; the cookie is "csrf-token"
		    "X-Box-Client-Name": "forms",
		    "X-Box-Client-Version": "prod",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		    // Note: There is no referrer
		},
		"method": "GET",
		"mode": "cors"
	}).then(async function(response){
		let responseJson = await response.json();
		lastResponseValue = responseJson;
		let replacement_html = `\
		<!DOCTYPE html>
		<html>
		<body>
			<h2>${responseJson.form.content.components["group-0"].label}</h2>
			<p>${responseJson.form.content.components["group-0"].description}</p>
		`
		
		responseJson.form.content.components["group-0"].items.forEach(function(key){
			const item = responseJson.form.content.components[key];
			replacement_html += `<h4>${item.label}</h4>`;
			if (item.description) {
				replacement_html += `<p>${item.description}</p>`;
			}
			/* TODO: Set a global var (or element ID) at some point so we know
				how to actually submit this when the user presses the button */
			/* TODO: Get hint text from
				the JavaScript, something that looks like
				`{id:\"boxFileRequest.emailTextField.placeholderMessage\",defaultMessage:\"Enter your email\"},`.
				Presumably, the form could specify a custom message here,
				but the form I used for testing didn't do that. */
			if (item.type == "textField" && item.textType == "email") {
				replacement_html += `<input type="email" placeholder="Enter your email">`;
			} else if (item.type == "textField" && item.textType == "text" && item.multiline == true) {
				replacement_html += `<textarea placeholder="Add additional information"></textarea>`;
			} else if (item.type == "textField" && item.textType == "text") {
				replacement_html += `<input type="text" placeholder="Enter a response">`;
			} else if (item.type == "uploadField") {
				/* I suspect we are supposed to be able to upload multiple files
					here. */
				replacement_html += `<input multiple="" type="file">`; // Should have an id too, but I have no idea how to get it.
			} else {
				console.error("I don't know how to handle this item type! The form could be missing inputs when sent!");
				alert("I don't know how to handle this item type! The form could be missing inputs when sent!");
			}
		});
		
		replacement_html += `<button type="submit">Submit</button>
							</body>`;
		
		// next 3 lines based on code by Wojtek Kosior in the Box fix
		const parser = new DOMParser();
		const alt_doc = parser.parseFromString(replacement_html, "text/html");
		document.documentElement.replaceWith(alt_doc.documentElement);
		
		// Add an onclick for:
			// formResponse(responseJson);
	});
}

// Start submitting a response, `fileResponseJson` is the response json from fileRequest()
function formResponse(fileRequestJson) {
	fetch(`https://${document.domain}/app-api/file-request-web/public/form-response`, {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json;version=1",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "X-CSRF-TOKEN": csrfToken,
		    "X-Box-Client-Name": "forms",
		    "X-Box-Client-Version": "prod",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		    // note: no referrer
		},
		"body": {
			"fileRequestURL": formID,
			"formVersionId": fileRequestJson.form.versionId,
			"values": {
				// TODO: fill this in correctly
				/* The IDs of the inputs: the input string (or an object if a file) */
				/* e.g. 
				string-field-id: "submitted string",
				file-field-id: {
					"type": "files",
					"data": [{
						"local_path": "name_of_submitted_file.pdf",
						"size": 184733
					}]
				}, */
				"folder": {
					"type": "folder",
					"id": fileRequestJson.folder.id
				}
			}
		},
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		let responseJson = await response.json();
		lastResponseValue = responseJson;
		// content(responseJson);
	});
}

// get the upload URL for the file(s)
function content(formResponseJson) {
	fetch(`https://${document.domain}/api/2.1/files/content`, {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Authorization": `Bearer ${formResponseJson.uploadToken}`,
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		    // note: no referrer
		},
		"body": {
			"name": /* file_name.pdf */,
			"parent": {
				"id": formResponseJson.formResponse.folder.id
			},
			"description": formResponseJson.formResponse["element-description-required"],
			"size": 184733
		},
		"method": "OPTIONS",
		"mode": "cors"
	});
}

// TODO
function contentUploadFile() {
	/* Send the files in a format that I've never seen before (Looks like a pdf
		in-between plaintext (I uploaded a pdf so that's why it's pdf and not
		some othe type)) */
}

function formResponseFinish(contentUploadFileJson) {
	fetch(`https://${document.domain}/app-api/file-request-web/public/form-response/${/* TODO: key from formResponse() */}:file?formVersionId=${/* TODO: same as earlier formVersionId*/}`, {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json;version=1",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "X-CSRF-TOKEN": "j5lE1MQY2UMa4NpzxLlpyumnG04H-BytETqqCQ0Mwzw",
		    "X-Box-Client-Name": "forms",
		    "X-Box-Client-Version": "prod",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": {
			"elementId": /* TODO: element id of file */
			"localPath": /* TODO: file_name.pdf */
			"size": /* TODO: 184733 */
			"file":  {
				"type": "file",
				"id": contentUploadFileJson.id
			}
		},
		"method": "PUT",
		"mode": "cors"
	});
}


currentUser(); // just calling to try to mimic the official app :P
fileRequest();
