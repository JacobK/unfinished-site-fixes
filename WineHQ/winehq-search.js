// This fix requires whitelisting scripts in LibreJS, if you have LibreJS installed.
// example URL: https://www.winehq.org/search?q=SuperTuxKart

let cx = null;
for (const script of document.scripts) {
	// also based on code from Wojtek Kosior (in the Google Sheets fix)
	// regex help from here: https://stackoverflow.com/questions/375104/how-can-i-match-a-quote-delimited-string-with-a-regex
	// My understanding of the regex is that it's matching `var cx = '` and then grabbing a character that's not a quote (^ = not I think) an infinite (*) amount of times (as long as it's possible), and then it matches `'` outside of the text that's grabbed to be stored in match.
    const match = /var cx = '([^']*)'/.exec(script.textContent); // looks for "var cx = " in the script files and then grabs the quoted text after that.
    if (!match)
	continue;
    cx = match[1];
}
// cx is "partner-pub-0971840239976722:w9sqbcsxtyf" for me and it's possible that this won't change (e.g. if it's a way to identify WineHQ to Google, and not the user to Google).
if (!cx) { // For people who don't have the scripts whitelisted in LibreJS, since we're 
	cx = "partner-pub-0971840239976722:w9sqbcsxtyf";
}



await fetch(`https://cse.google.com/cse/cse.js?cx=${cx}`, {
    "credentials": "omit",
    "headers": {
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "script",
        "Sec-Fetch-Mode": "no-cors",
        "Sec-Fetch-Site": "cross-site"
    },
    "method": "GET",
    "mode": "no-cors" // I had to set this from "cors" to "no-cors" manually for some reason; it didn't come like that as part of the "Copy as Fetch" option. (Apparently this makes the request send, but it still doesn't allow reading the text. :P)
}).then(async function(response){
	const responseText = await response.text();
	console.log(responseText); // This currently returns an empty string, even when `content.cors.disable` is enabled. I suspect the reason has to do with CORS, because getting the text on any winehq page works, bug getting the text on third party documents does not work.
	// Hmm, actually I get the same error even when running this fetch at "http://cse.google.com/dishulajf;o" (just garbage text to get a 404 instead of a redirect)
});


// So, I guess <script> can bypass CORS but fetch() cannot, so the WineHQ search page requests scripts from Google by way of <script> tags, and executes those scripts which run some functions that set some variables, allowing the main page's scripts to see the data. I'm not sure we can support this site without bypassing CORS. :P
// On the other hand, it's possible I don't really know what I'm talking about.
