/*
 * SPDX-FileCopyrightText: 2016 Bryan Richter
 * SPDX-FileCopyrightText: 2024 Jacob K
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
snowdrift-coop-payment-info
https://snowdrift.coop/payment-info
*/

// copied and modified from <https://codeberg.org/snowdrift/snowdrift/src/branch/master/website/templates/page/payment-info.julius>, licensed AGPL-3.0-or-later

const variables = {};

function getVariable(scriptText, variableMatch) {
	const matchArr = scriptText.match(variableMatch);
	return matchArr[1];
}

function getAutoScript() {
	let autoScript = null;
	for (script of document.scripts) {
		if (script.src.match(/https:\/\/snowdrift.coop\/static\/tmp\/autogen-/)) {
			return script;
		}
	}
}

async function getAllVariables() {
	const autoScript = getAutoScript();
	const distScriptResp = await fetch(autoScript.src);
	const distScript = await distScriptResp.text();
	variables.publishableKey = getVariable(distScript, /key:'([^']*)'/);
	variables.img_logo_small_png = getVariable(distScript, /image:'([^']*)'/);
	variables.userEmail = getVariable(distScript, /email:'([^']*)'/);
	variables.tokenId = getVariable(distScript, /tokField=document.querySelector\('([^']*)'\)/);
	variables.paymentFormId = getVariable(distScript, /;document.querySelector\('([^']*)'\).submit\(\)/);
	variables.paymentButtonId = getVariable(distScript, /btn=document.querySelector\('([^']*)'\)/);
}

function runScript() {
//	window.addEventListener('load', function() {
		var handler = StripeCheckout.configure({
		    key: variables.publishableKey,
		    image: variables.img_logo_small_png,
		    locale: 'auto',
		    amount: 0,
		    name: 'Snowdrift.coop',
		    description: 'Register a credit card to pledge',
		    zipCode: true,
		    // ^ Helps prevent fraud; recommended by Stripe
		    panelLabel: 'Register',
		    allowRememberMe: false,
		    email: variables.userEmail,
		    token: function(token) {
		        var tokField = document.querySelector(variables.tokenId);
		        tokField.value = token.id;
		        document.querySelector(variables.paymentFormId).submit();
		    }
		});

		var btn = document.querySelector(variables.paymentButtonId);
		btn.addEventListener('click', function(ev) {
		    handler.open();
		    ev.preventDefault();
		});
		btn.classList.remove('hidden');

		// Close Checkout on page navigation
		window.addEventListener('popstate', function() {
		    handler.close();
		});
//	});
}

async function main() {
	await getAllVariables();
	return runScript();
}
main();
