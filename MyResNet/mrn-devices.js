/*
mrn-devices
https://myresnet.com/account/devices
*/


function userInformation() {
	fetch("https://myresnet.com/account/devices/user-information", {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
//		    "X-NewRelic-ID": "REDACTEDReDACTedReDACt==", // redacted value
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"method": "GET",
		"mode": "cors"
	});
}

function listDevices() {
	fetch("https://myresnet.com/account/devices/list-devices", {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
//		    "X-NewRelic-ID": "REDACTEDReDACTedReDACt==", // redacted value
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"method": "GET",
		"mode": "cors"
	});
}

function updateStatus(subName) {
	fetch("https://myresnet.com/account/devices/update-status", {
		"credentials": "include",
		"headers": {
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": `status=Inactive&sub_name=${subName}`,
		"method": "POST",
		"mode": "cors"
	});
}

function connectionStatus(subName) {
	fetch(`https://myresnet.com/account/devices/connection-status?sub_name=${subName}`, {
		"credentials": "include",
		"headers": {
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"method": "GET",
		"mode": "cors"
	});
}
