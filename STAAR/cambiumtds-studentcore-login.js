/*
cambiumtds-studentcore-login
https://login6.cambiumtds.com/student_core/V43/Pages/LoginShell.aspx
*/

function getPageJsonVar(variableName) {
	for (const script of document.scripts) {
		const matchArr = script.textContent.match(`${variableName} = (.*);\n`);
		if (matchArr) {
			return JSON.parse(matchArr[1]);
		}
	}
}

async function login() {
	const response = await fetch(`https://login6.cambiumtds.com/student_core/V43/Pages/API/MasterShell.axd/loginStudent?timestamp=${Date.now()}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://login6.cambiumtds.com/student_core/V43/Pages/LoginShell.aspx?c=Texas_PT&a=Student",
		"body": JSON.stringify({
			"keyValues": {
				"FirstName": "GUEST",
				"ID": "GUEST"
			},
			"sessionId": "GUEST Session",
			"forbiddenApps": [],
			"browserValidation": {
				"challenge": getPageJsonVar("tds.BrowserInfo").challenge,
				"checkSum": getPageJsonVar("tds.BrowserInfo").challenge
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
