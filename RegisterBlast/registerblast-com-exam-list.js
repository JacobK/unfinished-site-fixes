/*
registerblast-com-exam-list
https://www.registerblast.com/utdallas/Exam/List
*/
/* Warning: This scripts sets the HTML of the page via outerHTML with text from
	the server! Is this safe? */
/* Note: Each selection will be sent to the server to get the next selection
	options. Actually, maybe it will be easier to not do that. */
/* Hey! You need to sign in before you run this script. I have no idea what
	will happen if you don't.
		Also, we should maybe consider asking RegisterBlast if we can simply
	re-use that for Haketilo. That would be much easier.
		https://www.registerblast.com/utexas/Exam/List is different enough that
	I think it may have to be handled explicitly. Or maybe it's possible to
	handle it generally but I just can't see how to do it. */
/* Warning: The confirmation page does not show the exam time! Make sure you
	got it correct (possibly by looking at the network log)! The reciept does
	show the time though. */

function getObjectsArr() {
	for (script of document.scripts) {
		/* The following regex assumes there will not be an array anywhere
			inside of the outer array. */
		const matchArr = script.textContent.match(/var objects = (\[[^\]]*\]);/);
		if (matchArr) {
			return JSON.parse(matchArr[1]);
		}
	}
}

/* It annoys me greatly that I can think of no better way to do this. */
function getStepNameFromStepNum(stepNum) {
	switch (stepNum) {
		case 1:
			return "GroupChosen";
		case 2:
			return "ExamChosen";
		case 3:
			return "DateChosen";
	}
}

/* opt is short for option, and it's the value chosen for the current step.
	step is the current step. */
async function getNextObjects(opt, stepNum) {
	const stepName = getStepNameFromStepNum(stepNum);
	let url = `https://www.registerblast.com/utdallas/Exam/${stepName}/${opt}`;
	if (stepNum < 3) {
		url += `/${stepNum}`;
	}
	const response = await fetch(url, {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		// Referrer will be set by the browser.
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

/* opt is the chosen exam option. */
async function getDateStepDates(opt) {
	const response = await fetch(`https://www.registerblast.com/utdallas/Exam/GetDateStepDates/${opt}`, {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		// Referrer will be set by browser.
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

function getFancyDate(monthDayYearDate) {
	const dateArr = monthDayYearDate.split("-");
	if (dateArr[0].length == 1) {
		dateArr[0] = "0" + dateArr[0];
	}
	if (dateArr[1].length == 1) {
		dateArr[1] = "0" + dateArr[1];
	}
	const dateObj = new Date(`${dateArr[2]}-${dateArr[0]}-${dateArr[1]}T00:00:00`);
	return dateObj.toLocaleString('en-us', { weekday:"long", year:"numeric", month:"long", day:"numeric"});
}

async function addDatesToSelect(selectElem, opt) {
	let defaultOption = document.createElement("option");
	defaultOption.value = "";
	defaultOption.setAttribute("disabled", "");
	defaultOption.setAttribute("selected", "");
	defaultOption.innerText = "Choose a date";
	selectElem.appendChild(defaultOption);
	const dates = await getDateStepDates(opt);
	for (date of dates) {
		let newOption = document.createElement("option");
		const fancyDate = getFancyDate(date);
		newOption.value = fancyDate;
		newOption.innerText = fancyDate;
		selectElem.appendChild(newOption);
	}
}

function addTimesToSelect(selectElem, times) {
	for (time of times) {
		let newOption = document.createElement("option");
		newOption.value = time;
		newOption.innerText = time;
		selectElem.appendChild(newOption);
	}
	selectElem.name = "Time";
}

async function renderObject(obj) {
	const step = obj.number;
	let newElem = document.createElement("li");
	document.getElementById("StepList").appendChild(newElem);
	newElem.outerHTML = obj.html;
	newElem = document.getElementById("StepList").lastElementChild; /* Setting
		outerHTML does not actually change the variable, so we have to find
		the element again in the DOM. */
	let saveButton = document.createElement("button");
	if (newElem.querySelector(".select")) {
		newElem.querySelector(".select").appendChild(saveButton);
	} else if (newElem.querySelector("input")) {
		newElem.querySelector("input").parentElement.appendChild(saveButton);
	}
	let step2Opt = null;
	if (step == 2) {
		document.getElementById("Exam2").name = "Exam";
	}
	if (step > 2) {
		step2Opt = document.getElementById("step2").querySelector("select").value;
	}
	if (step == 3) {
		let dateSelectElem = document.createElement("select");
		dateSelectElem.id = "Date3Select";
		dateSelectElem.name = "Date";
		dateSelectElem.setAttribute("required", "");
		newElem.querySelector("input").before(dateSelectElem);
		newElem.querySelector("input").remove();
		await addDatesToSelect(dateSelectElem, step2Opt);
		dateSelectElem.name = "Date";
	}
	saveButton.addEventListener("click", function(event) {
		event.preventDefault();
		event.target.parentElement.querySelector("select").setAttribute("disabled", "");
		let opt = event.target.parentElement.querySelector("select").value;
		if (step == 3) {
			opt = `${step2Opt}?ps=&date=${opt}&codes=-1`; // TODO: support codes and "ps" other than the values here
		}
		getNextObjects(opt, step).then(function(returnVal) {
			if (step == 1) {
				renderObject(returnVal[0]);
			} else if (step == 2) {
				renderObjects(returnVal);
			} else if (step == 3) {
				addTimesToSelect(document.getElementById("Time4"), returnVal);
			}
		});
		event.target.remove();
	});
	saveButton.innerText = "Send Selection to Server";
}

function renderObjects(objArr) {
	for (obj of objArr) {
		renderObject(obj);
	}
}

if (!document.URL.match(/https:\/\/www.registerblast.com\/[^\/]*\/Exam\/List/)) {
	throw new Error("This isn't the right URL scheme! Sorry!");
}

renderObject(getObjectsArr()[0]);

// remove "disabled" attribute on submit so the data goes to the server
document.querySelector("form").addEventListener("submit", function(){
	for (selectElem of document.querySelectorAll("select")) {
		selectElem.removeAttribute("disabled");
	}
});