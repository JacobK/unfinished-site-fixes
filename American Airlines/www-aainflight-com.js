/*
www-aainflight-com
https://www.aainflight.com
*/

// get device ID and flight info
async function device() {
	const response = await fetch(`https://www.aainflight.com/device?random=${Date.now()}`, {  // random was "1686235701313" on 2023-06-08 and the number seems to be increasing with every request, so this is not a random value but rather (I'm guessing) a time value to seed a random generator somewhere
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0",
		    "Accept": "application/json",
		    "Accept-Language": "en",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://www.aainflight.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// movies require Widevine it seems; trailers do not

function drawPage(deviceInfo, showButton) {
	document.open();
	const viasatUrl = `https://sponsoredaccess.viasat.com/americanairlines/?deviceId=${deviceInfo.guid}/#/connect`
	document.write(`<h1><a href="${viasatUrl}">Click here to go to Viasat for free Wi-Fi, maybe</a></h1>`);
	document.write(`<h1>device ID: ${deviceInfo.guid}</h1>`);
	if (showButton == true) {
		document.write(`<button id="auto">Click here to enable auto-refresh</button>`);
	}
	document.write(`<h2>flight number: ${deviceInfo.flightNumber}</h2>`);
	document.write(`<p>origin: ${deviceInfo.flightOrigin}<br>
						destination: ${deviceInfo.flightDestination}<br>
						minutes remaining: ${deviceInfo.flightMinutesRemaining}<br> <!-- Does not match official app for some reason (e.g. 10 mins in official vs 23 mins in mine). -->
						altitude: ${deviceInfo.altitude}<br>
						latitude: ${deviceInfo.latitude}<br>
						longitude: ${deviceInfo.longitude}<br>
						heading: ${deviceInfo.heading}</p>`);
	document.write(`<p>${JSON.stringify(deviceInfo)}</p>`);
}

async function main() {
	const deviceInfo = await device();
	drawPage(deviceInfo, true);
	document.getElementById("auto").addEventListener("click", function(){
		drawPage(deviceInfo);
		setInterval(async function(){
			const deviceInfoNew = await device();
			drawPage(deviceInfoNew, false);
		}, 10000)
	});
}

main();
