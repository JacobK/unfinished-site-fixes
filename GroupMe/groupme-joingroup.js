/*
groupme-joingroup
https://groupme.com/join_group/**
https://web.groupme.com/join_group/**
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

alert("Currently, joining a group by going to this kind of link is not supported; paste the link in groupme-signin. You will be redirected there now.");
document.location.assign("https://web.groupme.com/signin");
