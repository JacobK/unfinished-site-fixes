/*
	example URL: https://groupme.com/en-US/
*/

// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

// Only use this code if you can also do something useful with /signup
document.getElementsByClassName("download big button get_the_app")[0].href = document.getElementsByClassName("download big button get_the_app")[0].getAttribute("data-signupurl");
document.getElementsByClassName("download big button get_the_app")[0].textContent = document.getElementsByClassName("download big button get_the_app")[0].getAttribute("data-signuptext");

