/*
	example URL: https://web.groupme.com/signup
*/

// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

let lastResponseValue = null; // for debugging

let registrationId = null;
let registrationLongPin = null;

let publicKey = null;
let apiJsUrl = null;
// for loop based on code from google_drive_files.js by Wojtek Kosior in 2021
for (const script of document.scripts) {
    const match = /^https:\/\/client-api.arkoselabs.com\/v2\/([^\/]*)\/api.js$/.exec(script.src);
    if (!match)
	continue;
	
	apiJsUrl = match[0];
	publicKey = match[1];
}

const hiddenDiv = document.createElement("div");
hiddenDiv.setAttribute("aria-hidden", "true");
document.body.appendChild(hiddenDiv);
const enforcementIframe = document.createElement("iframe");
enforcementIframe.className = "Z6wXJz-G-6vcu3I0ZoM_B lightbox";
enforcementIframe.title = "Verification challenge";
enforcementIframe.setAttribute("enforcement-frame", "enforcement-frame");

let tokenString = null;

// automatically set the token string
// copied and modified from https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage (MIT license or public domain, not sure; see https://developer.mozilla.org/en-US/docs/MDN/Writing_guidelines/Attrib_copyright_license for details)
window.addEventListener("message", (event) => {
  // Do we trust the sender of this message?
  if (event.origin !== "https://client-api.arkoselabs.com") {
  	console.error("Bad origin:", event.origin);
    return;
  }

	if (event.data.split(": ")[0] == "arkoselabsTokenString") {
		tokenString = event.data.split(": ")[1];
	}

}, false);

let frameUrl = null;
fetch(apiJsUrl, {
//    "credentials": "omit",
    "headers": {
//        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "script",
        "Sec-Fetch-Mode": "no-cors",
        "Sec-Fetch-Site": "cross-site"
    },
//    "referrer": "https://web.groupme.com/",
    "method": "GET",
    "mode": "cors"
}).then(async function(response){
	const responseText = await response.text();
	lastResponseValue = responseText;
	const match = /ENFORCEMENT_URL="([^"]*)";/.exec(responseText);
	if (match) {
		enforcementIframe.src = match[1];
	}
	hiddenDiv.appendChild(enforcementIframe);
});

// emailAddress is the email address as a string, and regionCode is the country or region code as a string, without the "+".
function emailCreate(emailAddress, regionCode) {
	fetch("https://v2.groupme.com/registrations/email_create", {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
	//        "X-Access-Token": "Iu4mdF+ejtp9y11VdrwgA2cGUzs04xLt74OyC6CNAbs=", // can't figure out how this is generated
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"registration": {
				"mfa": false,
				"country_or_region_code": regionCode,
				"platform": "Web",
				"email": emailAddress
			}
		}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		const responseJson = await response.json();
		lastResponseValue = responseJson;
		if (responseJson.meta.code == 201) {
			registrationId = responseJson.response.registration.id;
			registrationLongPin = responseJson.response.registration.long_pin;
		} else {
			console.error("An error occurred when trying to create an account with an email address:", responseJson.meta.code);
		}
	});
}

// uses registrationId and registrationLongPin
// emailAddress is a string containing the email address, name is a string containing the name, and phoneNumber is a string contianing the phone number (including the +1), and password is a string containing the password
function register(emailAddress, name, phoneNumber, password) {
	fetch(`https://v2.groupme.com/v2/registrations/${registrationId}/${registrationLongPin}`, {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "X-Verify-Id": "webapp",
		    "X-Verify-Token": tokenString,
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"registration": {
				"mfa": false,
				"platform": "Web",
				"email": emailAddress,
				"name": name,
				"phone_number": phoneNumber,
				"password": password
			}
		}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		responseJson = await response.json();
		if (responseJson.meta.code == 200) {
			// ok
		} else {
			console.error("register request contains an error", responseJson);
		}
	});
}

function sendPin(emailAddress, name, phoneNumber, password) {
	fetch(`https://v2.groupme.com/registrations/${registrationId}/${registrationLongPin}/send_pin`, {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
				"registration": {
					"mfa": false,
					"platform": "Web",
					"email": emailAddress,
					"name": name,
					"phone_number": phoneNumber,
					"password": password
				}
			}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		responseJson = await response.json();
		if (responseJson.meta.code == 200) {
			// ok
		} else {
			console.error("sendPin request contains an error", responseJson);
		}
	});
}

// pin is a string with the pin
function confirmPin(pin) {
	fetch(`https://v2.groupme.com/registrations/${registrationId}/${registrationLongPin}/confirm`, {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"registration": {
				"pin": pin
			}
		}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		responseJson = await response.json();
		if (responseJson.meta.code == 200) {
			console.warn("The PIN was accepted, but this program does not support anything further.");
		} else if (responseJson.meta.code == 429) {
			alert(responseJson.meta.errors[0]);
			console.error(responseJson.meta.errors[0]);
		} else {
			console.error("sendPin request contains an error", responseJson);
		}
	});
}
