/*
https://client-api.arkoselabs.com/v2/**
https://client-api.arkoselabs.com/v2/49D02870-26F8-42F2-8619-0157104B9DEE/enforcement.redactedmd5sum.html
*/

// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

const publicKey = document.URL.split("/")[4];
const enforcementValue = document.URL.split("/")[5].split(".")[1];
const inIframe = window.location !== window.parent.location;

// This is inside an iframe...
	// https://client-api.arkoselabs.com/v2/49D02870-26F8-42F2-8619-0157104B9DEE/enforcement.redactedmd5sum.html
// This function is very similar to gt2() from my octocaptcha script...
async function getCaptchaToken() {
	const bda = "???"; // TODO: Add a better value here, bda is encoded JSON that can be read with atob()
	const rnd = Math.random();
	const response = await fetch(`https://client-api.arkoselabs.com/fc/gt2/public_key/${publicKey}`, {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": `https://client-api.arkoselabs.com/v2/${publicKey}/enforcement.redactedmd5sum.html`,
		"body": `bda=${bda}&public_key=${publicKey}&site=https%3A%2F%2Fweb.groupme.com&userbrowser=${encodeURIComponent(navigator.userAgent)}&style_theme=default&rnd=${rnd}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson.token;
}

getCaptchaToken().then(function(token){
	if (inIframe) {
		window.parent.postMessage(`arkoselabsTokenString: ${token}`, "https://web.groupme.com")
	} else {
		console.log(token);
	}
});
