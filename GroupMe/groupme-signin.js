/*
	identifier: groupme-signin
	long name: Sign In for GroupMe
	description: Allow signing in to GroupMe with free software.
	URL pattern: https://web.groupme.com/signin
*/

// SPDX-FileCopyrightText: 2022-2024 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

/*
Known side effect of this script:
You may get lots of extra GroupMe PINs sent to your phone number, much much more than you send. One guess as to why this happens is that I often misstype the PIN or forget the verificationCode step and refresh the page.
*/

/*
If you get CORS errors, the problem might just be that you are logged out?
*/

/* The GroupMe official API docs at <https://dev.groupme.com/docs/v3> are
	actually useful! It's how I figured out how to join a group! Also, there is
	some unofficial documentation at
	<https://github.com/groupme-js/GroupMeCommunityDocs/blob/master/files.md>
	and related URLs. It helped me realize that the file.groupme.com domain
	exists, and then I did a Google search for "site:file.groupme.com" and found
	a link to a document direct link, which had the format of
	`https://file.groupme.com/v1/${groupId}/files/${fileId}?access_token=${getCookie("token")}&omit-content-disposition=true`. 
	Reading the Beeper bridge source code might be helpful as well; they seem to
	support downloading files. */

// TODO: Let the user know that messages do not auto-update, ideally via a visible toggle that allows the user to enable it if they wish.

//let xAccessToken = null; // to be set by accessTokens() later, I think?
// We use the cookie "token" like the official app now; that way the user can refresh the page and stay logged in.
const deviceId = "redacted"; // seems to be an md5sum, redacted
let verificationCode = null; // set in accessTokens()

let lastResponseValue = null; // for debugging

function setCookie(cookieName, cookieValue, domain) {
	if (domain) {
		document.cookie = `${cookieName}=${cookieValue};domain=${domain}`;
	} else {
		document.cookie = `${cookieName}=${cookieValue}`;
	}
}

function genUUID() {
	return crypto.randomUUID();
}

// From https://stackoverflow.com/a/15724300 , by kirlich (https://stackoverflow.com/users/7635/kirlich) and Arseniy-II (https://stackoverflow.com/users/8163773/arseniy-ii), licensed CC BY-SA 4.0
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function accessTokens(username, password) {
	requestJson = {
		"username": username,
		"password": password,
		"grant_type": "password",
		"app_id": "groupme-web",
		"device_id": deviceId, // This also seems to be generated client-side, and it's the same across sessions.
	};
	
	if (verificationCode != null) {
		requestJson.verification = { "code": verificationCode };
		// This object is supposed to only be included when the client thinks it is verified.
	}
	
	fetch("https://v2.groupme.com/access_tokens", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
//		    "X-Access-Token": xAccessToken, // This seems to be generated client-side, and the same within a session.
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
//		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify(requestJson),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		const responseJson = await response.json();
		lastResponseValue = responseJson;
		if (responseJson.response.verification) {
			verificationCode = responseJson.response.verification.code;
		}
		setCookie("token", responseJson.response.access_token, ".groupme.com");
	});
}

function initiate() {
	fetch(`https://api.groupme.com/v3/verifications/${verificationCode}/initiate`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
//		"referrer": "https://web.groupme.com/",
		"body": "{\"verification\":{\"method\":\"sms\"}}",
		"method": "POST",
		"mode": "cors"
	});
}

// input is a string with the PIN
function verificationConfirm(pin) {
	fetch(`https://api.groupme.com/v3/verifications/${verificationCode}/confirm`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
//		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"verification": {
				"pin": pin
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
}

function verifyDateOfBirth(dob, verificationCode) {
	fetch(`https://api.groupme.com/v3/verifications/${verificationCode}/verify/dob`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
//		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"verification": {
				"date_of_birth": dob // in YYYY-MM-DD format
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
}

function confirmDateOfBirth(dob, verificationCode) {
	fetch(`https://api.groupme.com/v3/verifications/${verificationCode}confirm/dob`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "null",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
//		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"verification": {
				"date_of_birth": dob // in YYYY-MM-DD format
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
}

function getAccessToken() {
	return getCookie("token");
}

function installations() {
	fetch("https://api.groupme.com/v3/installations", {
		"credentials": "omit",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(), // access token got from access_tokens()
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"installation": {
				"client_id": deviceId,
				"platform": "Web",
				"model": navigator.userAgent,
				"app_version": "7.3.8",
				"locale": "en",
				"language": "en-US"
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
}

function themes() {
	fetch("https://web.groupme.com/assets/themes.json?version=7.3.8-20220826.10", {
		"credentials": "include",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://web.groupme.com/signin",
		"method": "GET",
		"mode": "cors"
	});
}

async function me() {
	const response = await fetch("https://api.groupme.com/v3/users/me", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

function powerups() {
	fetch("https://powerup.groupme.com/powerups", {
		"credentials": "omit",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
}

function groups() {
	fetch("https://api.groupme.com/v3/groups?omit=memberships&page=1&per_page=100", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
}

function groupsWithMemberships(page = 1) {
	fetch(`https://api.groupme.com/v3/groups?page=${page}&per_page=10`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
}

// groupId can be found from groups() and fileId can too, if the attachment is from the latest message. accessToken is X-Access-Token from the header.
// groupId can be a conversation id for direct messages
function getAttachmentUrl(groupId, fileId) {
	return `https://file.groupme.com/v1/${groupId}/files/${fileId}?access_token=${getAccessToken()}`;
	// can add "&omit-content-disposition=true" to the end if you want
}

async function chats() {
	const response = await fetch("https://api.groupme.com/v3/chats?page=1&per_page=100", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	lastResponseValue = responseJson;
	return responseJson;
}

function messages(groupId) {
	fetch(`https://api.groupme.com/v3/groups/${groupId}/messages?acceptFiles=1&limit=100`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
}

function relationships() {
fetch("https://api.groupme.com/v4/relationships?include_blocked=true&since=2020-08-18T15:21:22.518807Z", {
    "credentials": "omit",
    "headers": {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "X-Access-Token": getAccessToken(),
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site"
    },
    "referrer": "https://web.groupme.com/",
    "method": "GET",
    "mode": "cors"
});
}

async function checkLogin() {
	const userInfoJson = await me();
	if (userInfoJson.meta.code == 200) {
		alert("You are logged in.");
		return true;
	} else {
		alert("You are not logged in.");
		return false;
	}
}

async function getDirectMessages(userId) {
	const response = await fetch(`https://api.groupme.com/v3/direct_messages?acceptFiles=1&limit=50&other_user_id=${userId}`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
//		    "X-Requested-With": "GroupMeWeb/7.8.16-20230715.1",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// Thanks to: https://stackoverflow.com/a/26298948
// Thanks to: https://github.com/groupme-js/GroupMeCommunityDocs/blob/master/files.md
// TODO: Understand this code better
/*
async function uploadFile(fileInputElem) {
	file = fileInputElem.files[0];
	req = new XMLHttpRequest();
	req.open("POST", "https://neverssl.com", true);
	req.send(f.text())
	return;
	arrBuff = null;
	reader = new FileReader();
	await reader.readAsArrayBuffer(file);
	
	req.onload = (event) => {
		responseJson = await event.target.response.json();
		console.log(responseJson)
		return responseJson;
	};

	reader.onload = function(e) {
		let contents = e.target.result;
		arrBuff = contents;
		req.send(ab);
	};
	
}
*/

// Thanks to: https://stackoverflow.com/a/48176648
// Thanks to: https://github.com/groupme-js/GroupMeCommunityDocs/blob/master/files.md
async function uploadFile(groupId, fileInputElem) {
	file = fileInputElem.files[0];
	const response = await fetch(`https://file.groupme.com/v1/${encodeURIComponent(groupId)}/files?name=${encodeURIComponent(file.name)}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:125.0) Gecko/20100101 Firefox/125.0",
			"Host": "file.groupme.com",
			"Accept-Language": "en-US,en;q=0.5",
			"X-Access-Token": getAccessToken(),
			"Content-Type": "application/json",
			"Connection": "close"
		},
//		"referrer": "https://neverssl.com/",
		"body": file, //await file.text(), // Using .text() badly corrupted the file when I tried it
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function checkFileUploadStatus(status_url) {
	const response = await fetch(status_url, {
		"headers": {
			"X-Access-Token": getAccessToken()
		}
	});
	const responseJson = await response.json();
	return responseJson;
}

function constructFileAttachment(fileId) {
	return {
		"type":"file",
		"file_id":fileId
	}
}

/* recipientId is the ID of the recipient, which can be seen in group chat
	message metadata. messageBody is the text of the message. */
async function sendDirectMessage(recipientId, messageBody, attachmentArr) {
	if (!attachmentArr) {
		attachmentArr = [];
	}
	const response = await fetch("https://api.groupme.com/v3/direct_messages", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
//		    "X-Requested-With": "GroupMeWeb/7.8.16-20230715.1",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"direct_message": {
				"text": messageBody,
				"attachments": attachmentArr,
				"recipient_id": recipientId,
				"source_guid": null // random letters and numbers, sometimes begins with "android-", gets generated server-side if you send null
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// Hey, be careful with this function!
async function sendSeveralDirectMessages(recipientIdArr, messageBody) {
	id = recipientIdArr.shift();
	const responseJson = await sendDirectMessage(id, messageBody);
	if (recipientIdArr.length == 0) {
		return;
	} if (responseJson.meta.errors) {
		console.error(responseJson.meta);
		return;
	} else if (responseJson.meta.code == 201 && recipientIdArr.length > 0) {
		return await sendSeveralDirectMessages(recipientIdArr, messageBody);
	} else {
		console.error("unknown error");
		return;
	}
}

async function sendDMWithFile(conversationId, fileInputElem, recipientId, fileTitle) {
	const fileJobJson = await uploadFile(conversationId, fileInputElem);
	const fileStatusJson = await checkFileUploadStatus(fileJobJson.status_url);
	if (false && fileStatusJson.status == "completed") {
		return sendDirectMessage(recipientId, fileTitle, [constructFileAttachment(fileStatusJson.file_id)])
	} else {
		alert("Message not sent - see console.");
		console.error("File not finished uploading, run the following code:");
		console.log(`checkFileUploadStatus("${fileJobJson.status_url}");`);
		console.log(`sendDirectMessage("${recipientId}", "${fileTitle}", [constructFileAttachment(FILE_ID_STRING_HERE)]);`);
	}
}

// For some reason this isn't working for me
function sendGroupMessage(groupId, messageBody) {
	fetch(`https://api.groupme.com/v3/groups/${groupId}/messages`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"message": {
				"text": messageBody,
				"attachments": [],
				"source_guid": genUUID()
			}
		}),
		"method": "POST",
		"mode": "cors"
	});
}

async function printDirectMessagesToConsole(userId) {
	const directMessagesJson = await getDirectMessages(userId);
	console.info(`Printing messages from user ${userId}`);
	for (messageJson of directMessagesJson.response.direct_messages) {
		console.info(`[${new Date(messageJson.created_at*1000)}] <${messageJson.name}> ${messageJson.text}`);
	}
}

async function showChats() {
	document.getElementById("chatsDiv").innerHTML = "";
	const chatsJson = await chats();
	for (chat of chatsJson.response) {
		const chatDiv = document.createElement("div");
		chatDiv.setAttribute("data-userid", chat.other_user.id);
		chatDiv.setAttribute("data-conversationid", chat.last_message.conversation_id);
		const profileImg = document.createElement("img");
		profileImg.src = chat.other_user.avatar_url;
		profileImg.width = "100";
		profileImg.alt = `profile picture of ${chat.other_user.name}`;
		chatDiv.appendChild(profileImg);
		const otherUsernameHeader = document.createElement("h5");
		otherUsernameHeader.innerText = chat.other_user.name;
		chatDiv.appendChild(otherUsernameHeader);
		const lastMessageP = document.createElement("p");
		lastMessageP.innerText = chat.last_message.text;
		chatDiv.appendChild(lastMessageP);
		const timestampDisplay = document.createElement("p");
		timestampDisplay.innerText = new Date(chat.last_message.created_at * 1000);
		chatDiv.appendChild(timestampDisplay);
		const getMoreMessagesButton = document.createElement("button");
		getMoreMessagesButton.innerText = `get more messages from ${chat.other_user.name} (logged to console)`;
		getMoreMessagesButton.addEventListener("click", function() {
			printDirectMessagesToConsole(chatDiv.getAttribute("data-userid")); // TODO: instead replace chatDiv's innerHTML with an interface that shows the direct messages
		});
		chatDiv.appendChild(getMoreMessagesButton);
		const replyBox = document.createElement("textarea");
		chatDiv.appendChild(replyBox);
		const fileBox = document.createElement("input");
		fileBox.type = "file";
		chatDiv.appendChild(fileBox);
		const replyButton = document.createElement("button");
		replyButton.innerText = `reply to ${chat.other_user.name}`;
		replyButton.addEventListener("click", function(ev) {
			if (fileBox.files.length == 1) {
				sendDMWithFile(chatDiv.getAttribute("data-conversationid"), fileBox, chatDiv.getAttribute("data-userid"), replyBox.value);
			} else if (fileBox.files.length > 1) {
				alert("Too many files attached");
			} else if (replyBox.value && replyBox.value != "") {
				sendDirectMessage(chatDiv.getAttribute("data-userid"), replyBox.value);
				ev.target.remove();
			} else {
				alert("message box is empty");
			}
		});
		chatDiv.appendChild(replyButton);
		document.getElementById("chatsDiv").appendChild(chatDiv);
	}
}

async function requestExport() {
	await fetch("https://api.groupme.com/v3/exports", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": getAccessToken(),
//		    "X-Requested-With": "GroupMeWeb/7.8.16-20230715.1",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://web.groupme.com/",
		"body": JSON.stringify({
			"conversations": [ // TODO: get all conversation IDs (The ones with '+' symbols are DMs, always with the user's own ID first, then the ID of the person they are messaging.)
				{"id": "########"},
				{"id": "########"},
				{"id": "########+########"},
				{"id": "########+########"}
			],
			"filters": ["profile","device","powerup","mixpanel","analytics","message","meta","likes","calendar","poll","image","video","file"],
			"attachmentsFromOthers": true
		}),
		"method": "POST",
		"mode": "cors"
	});
}

async function previewGroup(groupId, joinToken) {
	const response = await fetch(`https://v2.groupme.com/groups/${groupId}/preview/${joinToken}`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Access-Token": "",
//		    "X-Requested-With": "GroupMeWeb/7.9.1-20230822.5"
		},
		"referrer": "https://web.groupme.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = response.json();
	return responseJson;
}

async function joinGroup(groupId, joinToken) {
	const response = await fetch(`https://api.groupme.com/v3/groups/${groupId}/join/${joinToken}`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "same-site",
		    "X-Access-Token": getAccessToken(),
		    "Pragma": "no-cache",
		    "Cache-Control": "no-cache"
		},
		"referrer": "https://web.groupme.com/",
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

function unknownErrorAlert() {
	alert("An unknown error occurred; see network log for details.");
}

async function joinGroupByLink(joinLinkElem) {
	const joinLink = joinLinkElem.value; // get joinLink from input elem
	
	// joinLink format is `https://web.groupme.com/join_group/${groupId}/${joinToken}`
	const joinLinkRegExp = new RegExp("^https://(?:web.)?groupme.com/join_group/([^/]*)/([^/]*)$");
	/* "^" represents the start of a string, and "$" represents the end of a
		string. "?:" makes the parenthesis form a "non-capturing group", which
		doesn't end up in the match array. */
	const matchArr = joinLink.match(joinLinkRegExp);
	if (!matchArr) {
		alert("That doesn't look like a join link.");
		return false;
	}
	const groupId = matchArr[1]; /* matchArr[0] is the entire link, matchArr[1]
		is the first parenthesis match, matchArr[2] is the second parenthesis
		match, etc. */
	const joinToken = matchArr[2];
	
	joinLinkElem.value = ""; // clear input box so the same link isn't submitted twice by mistake
	
	const groupInfo = await previewGroup(groupId, joinToken);
	if (groupInfo.meta.code != 200) {
		unknownErrorAlert(); // TODO: explicitly handle some error codes and make this branch an "else if" instead of an "if"
	}
	const userConfirmed = confirm(`Join group "${groupInfo.response.group.name}"? There are currently ${groupInfo.response.group.members_count} members, out of ${groupInfo.response.group.max_members} maximum allowed. The description is "${groupInfo.response.group.description}".`);
	if (!userConfirmed) {
		return false;
	}
	
	const joinGroupResponseJson = await joinGroup(groupId, joinToken);
	if (joinGroupResponseJson.meta.code == 20100) {
		alert(`The group, "${joinGroupResponseJson.response.group.name}", was successfully joined! You should also recieve a text message with a confirmation.`);
		return true;
	} else {
		unknownErrorAlert();
		return false;
	}
}

/* function I used for testing
function test(a, b, c) {
  if (c) {
    console.log("true");
  } else if (c == false) {
    console.log("false");
  } else {
    console.log("assume true");
  }
}


*/

async function createGroup(groupName, description, shareLinkEnabled = true) {
	if (!groupName) {
		return false; // can't make a group with no name
	}
	
	const groupJson = {
		"name": groupName
	}
	if (description) {
		groupJson.description = description;
	}
	if (shareLinkEnabled) {
		groupJson.share = true;
	} else {
		groupJson.share = false;
	}
	
	const response = await fetch(`https://api.groupme.com/v3/groups`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "same-site",
		    "X-Access-Token": getAccessToken(),
		    "Pragma": "no-cache",
		    "Cache-Control": "no-cache"
		},
		"referrer": "https://web.groupme.com/",
		"method": "POST",
		"body": JSON.stringify(groupJson),
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function createGroupByName(groupName) {
	if (!groupName) {
		alert("You need to enter a group name first!");
		return false;
	}
	
	document.getElementById("newGroupButton").style.display = "none";
	const groupResponseJson = await createGroup(groupName);
	document.getElementById("newGroupButton").style.display = "revert";
	document.getElementById("newGroupNameInput").value = "";
	
	
	if (groupResponseJson.meta.code == 201 && groupResponseJson.response.share_url) {
		console.info("group share link:", groupResponseJson.share_url);
		alert(`Group ${groupResponseJson.response.name} has been created, and the share link is <${groupResponseJson.response.share_url}>. The share link has also been logged to the console.`);
	} else if (groupResponseJson.meta.code == 201) {
		alert(`Group ${groupResponseJson.response.name} has been created, but there is no share link. Maybe you'll get a text message with more information, or maybe you'll need to add functionality to this app to support chatting in groups.`);
	} else {
		alert(`It seems there has been an error (error code ${groupResponseJson.meta.code}). It does not look like the group was created successfully.`);
	}
}

async function addMemberToGroupById(groupId, memberId, nickname) {
	const joinJson = {
		"members": [
			{
				"nickname": nickname,
				"user_id": memberId
			}
		]
	}
	
	const response = await fetch(`https://api.groupme.com/v3/groups/${groupId}/members/add`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "same-site",
		    "X-Access-Token": getAccessToken(),
		    "Pragma": "no-cache",
		    "Cache-Control": "no-cache"
		},
		"referrer": "https://web.groupme.com/",
		"method": "POST",
		"body": JSON.stringify(joinJson),
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

/* instructions
accessTokens(phonenum, password);
initiate();
verificationConfirm(pin); // Make sure this is a string!
accessTokens(phonenum, password), this time with verification
If necessary,
	verifyDateOfBirth(dob, verificationCode);
	confirmDateOfBirth(dob, verificationCode);
accessTokens(phonenum, password), again with verification
*/

function enableAutoRefresh() {
	setInterval(showChats, 15000);
	document.getElementById("enableAutoRefreshButton").remove();
}

/* Replace current page contents. */
const replacement_html = `\
<!DOCTYPE html>
<html>
<body>
username: <input type="text" id="usernameInput" placeholder="username">
password: <input type="password" id="passwordInput" placeholder="password">
<button id="loginButton">login</button>
<button id="initiateButton">initiate</button>
PIN: <input type="text" id="pinInput" placeholder="PIN">
<button id="confirmButton">confirm PIN</button>
<button id="loginButton2">login</button>
<br>
<button id="checkLogin">check login</button>
<!-- <button id="enableAutoRefreshButton">enable auto refresh (will erase unsent replies!)</button> -->

<br>

<input type="text" id="joinLinkInput" placeholder="join link" />
<button id="joinButton">join group by link</button>

<br>

<input type="text" id="newGroupNameInput" placeholder="group name" />
<button id="newGroupButton">create group</button>

<br>

<button id="getChatsButton">get chats</button>

<div id="chatsDiv">

</div>
</body>
</html>
`;

// a few lines of code from app-box-com-fix (C) Wojtek Kosior 2022
/*
 * We could instead use document.write(), but browser's debugging tools would
 * not see the new page contents.
 */
const parser = new DOMParser();
const alt_doc = parser.parseFromString(replacement_html, "text/html");
document.documentElement.replaceWith(alt_doc.documentElement);

document.getElementById("loginButton").addEventListener("click", function(){
	accessTokens(document.getElementById("usernameInput").value, document.getElementById("passwordInput").value);
});

document.getElementById("loginButton2").addEventListener("click", function(){
	accessTokens(document.getElementById("usernameInput").value, document.getElementById("passwordInput").value);
});

document.getElementById("initiateButton").addEventListener("click", function(){
	initiate();
});

document.getElementById("confirmButton").addEventListener("click", function(){
	verificationConfirm(document.getElementById("pinInput").value);
});

document.getElementById("getChatsButton").addEventListener("click", function(){
	showChats();
});

document.getElementById("checkLogin").addEventListener("click", function() {
	checkLogin();
});

document.getElementById("joinButton").addEventListener("click", function() {
	joinGroupByLink(document.getElementById("joinLinkInput")); /* TODO: allow
		navigating to real join links and handling them the same way GroupMe
		does */
});

document.getElementById("newGroupButton").addEventListener("click", function() {
	createGroupByName(document.getElementById("newGroupNameInput").value);
});

//document.getElementById("enableAutoRefreshButton").addEventListener("click", enableAutoRefresh);
