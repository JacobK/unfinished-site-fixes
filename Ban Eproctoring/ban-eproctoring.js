// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later



/*
ban-eproctoring
https://www.baneproctoring.com/
*/

const twitterInstance = "https://twitter.com";

// get the scorecard data, returns an array of objects representing colleges
async function scorecardFetch() {
    const response = await fetch("https://data.fightforthefuture.org/eproctoring/scorecard.json", {
        "credentials": "omit",
        "headers": {
    //        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:107.0) Gecko/20100101 Firefox/107.0",
            "Accept": "application/json, text/plain, */*",
            "Accept-Language": "en-US,en;q=0.5",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "cross-site"
        },
    //    "referrer": "https://www.baneproctoring.com/",
        "method": "GET",
        "mode": "cors"
    });
	const responseJson = await response.json();
	return responseJson;
}

// takes a scorecard JSON array and puts it on the page
function renderScorecard(scorecardArr) {
	const scorecard = document.createElement("div");
	document.querySelector("section#scorecard").querySelector("div.container").appendChild(scorecard);
	for (college of scorecardArr) {
		const row = document.createElement("div");
		row.classList.add("row");
		row.classList.add("mb-md-1");
		addNameToRow(row, college.name);
		addTwitterToRow(row, college.twitter);
		if (college.email && college.email2) {
			addEmailToRow(row, college.email, college.email2);
		} else if (college.email2) {
			addEmailToRow(row, college.email2);
		} else {
			addEmailToRow(row, college.email);
		}
		addStatusToRow(row, college.using_fr);
		scorecard.appendChild(row);
	}
}

// adds a name string to a row div element
function addNameToRow(row, name) {
	const nameDiv = document.createElement("div");
	nameDiv.classList.add("p-4");
	nameDiv.classList.add("bg-dark");
	nameDiv.classList.add("col-md-5");
	const nameStrong = document.createElement("strong");
	nameStrong.innerText = name;
	nameDiv.appendChild(nameStrong);
	row.appendChild(nameDiv);
}

function addTwitterToRow(row, twitter) {
	const twitterDiv = document.createElement("div");
	twitterDiv.setAttribute("data-v-e32fba24", "");
	twitterDiv.classList.add("text-center");
	twitterDiv.classList.add("text-nowrap");
	twitterDiv.classList.add("py-4");
	twitterDiv.classList.add("d-md-flex");
	twitterDiv.classList.add("bg-dark");
	twitterDiv.classList.add("col-md-2");
	const twitterLink = document.createElement("a");
	twitterLink.setAttribute("data-v-e32fba24", "");
	twitterLink.href = `${twitterInstance}/${twitter}`;
	twitterLink.target = "_blank";
	twitterLink.innerText = "Twitter";
	twitterDiv.appendChild(twitterLink);
	row.appendChild(twitterDiv);
}
// TODO: add the message from the original tweet

function addEmailToRow(row, email1, email2) {
	const emailDiv = document.createElement("div");
	emailDiv.setAttribute("data-v-e32fba24", "");
	emailDiv.classList.add("text-center");
	emailDiv.classList.add("text-nowrap");
	emailDiv.classList.add("py-4");
	emailDiv.classList.add("d-md-flex");
	emailDiv.classList.add("bg-dark");
	emailDiv.classList.add("col-md-2");
	const emailLink = document.createElement("a");
	emailLink.setAttribute("data-v-e32fba24", "");
	if (email2) {
		emailLink.href = `mailto:${email1}?cc=${email2}`;
	} else {
		emailLink.href = `mailto:${email1}`;
	}
	emailLink.innerText = "email";
	emailDiv.appendChild(emailLink);
	row.appendChild(emailDiv);
}
// TODO: add the subject and bcc from the official script

function addStatusToRow(row, isUsing) {
	const usingDiv = document.createElement("div");
	usingDiv.setAttribute("data-v-e32fba24", "");
	usingDiv.classList.add("text-center");
	usingDiv.classList.add("p-4");
	usingDiv.classList.add("d-flex");
	usingDiv.classList.add("col-md-3");
	usingDiv.classList.add("status");
	const usingStrong = document.createElement("strong");
	usingStrong.setAttribute("data-v-e32fba24", "");
	if (isUsing == true) {
		usingStrong.innerText = "ARE USING";
		usingDiv.classList.add("yes");
	} else if (isUsing == false) {
		usingStrong.innerText = "WON'T USE";
		usingDiv.classList.add("no");
	} else { // Is this branch actually reachable?
		usingStrong.innerText = "MIGHT USE"; // TODO: confirm this assumption that this is what should be shown in this case
	}
	usingDiv.appendChild(usingStrong);
	row.appendChild(usingDiv);
}

function main() {
	scorecardFetch().then((scorecardJson) => {
        if (document.querySelector("section#scorecard").querySelector("div.scorecard")) { // if the scorecard already exists
        	document.querySelector("section#scorecard").querySelector("div.scorecard").remove();
        }
        renderScorecard(scorecardJson);
    }, (errorMessage) => {
        console.error(errorMessage);
    });
}

main();
