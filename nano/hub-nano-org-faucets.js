/*
hub-nano-org-faucets
https://hub.nano.org/faucets
*/

async function getItems() {
	const response = await fetch("https://hub.nano.org/items.json", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "content-type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://hub.nano.org/faucets",
		"body": "{\"action\":\"ACTION_ITEMS_GET\",\"topLevelCategoryId\":215,\"selectedCategoryIds\":[],\"limit\":20}",
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

const itemsDiv = document.querySelector("main").querySelector("div.items");
const items = await getItems();
