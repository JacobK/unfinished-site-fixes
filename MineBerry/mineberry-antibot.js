/*
mineberry-antibot
https://mineberry.org/antibot/
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

fetch(`complete.php${window.location.search}`);
