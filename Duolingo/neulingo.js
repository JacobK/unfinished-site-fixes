
// get the first placement test challenge
await fetch("https://www.duolingo.com/2017-06-30/sessions", {
    "credentials": "include",
    "headers": {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Content-Type": "application/json;charset=utf-8",
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjYzMDcyMDAwMDAsImlhdCI6MCwic3ViIjo5MzgyNTA5NzR9.YKX6cFE1f2PGlaR7O5ZR6gMukJEjN-vS7EB9z9MoYRA",
        "X-Amzn-Trace-Id": "User=938250974",
        "X-Requested-With": "XMLHttpRequest",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin"
    },
    "referrer": "https://www.duolingo.com/placement/es", // not sure if I should keep this line
    "body": "{\"challengeTypes\":[\"assist\",\"characterIntro\",\"characterMatch\",\"characterPuzzle\",\"characterSelect\",\"characterTrace\",\"completeReverseTranslation\",\"definition\",\"dialogue\",\"form\",\"freeResponse\",\"gapFill\",\"judge\",\"listen\",\"listenComplete\",\"listenMatch\",\"match\",\"name\",\"listenComprehension\",\"listenIsolation\",\"listenTap\",\"readComprehension\",\"select\",\"selectPronunciation\",\"selectTranscription\",\"tapCloze\",\"tapClozeTable\",\"tapComplete\",\"tapCompleteTable\",\"tapDescribe\",\"translate\",\"typeCloze\",\"typeClozeTable\",\"typeCompleteTable\"],\"fromLanguage\":\"en\",\"isFinalLevel\":false,\"isV2\":false,\"juicy\":true,\"learningLanguage\":\"es\",\"smartTipsVersion\":2,\"type\":\"PLACEMENT_TEST\",\"speakIneligibleReasons\":\"recognizer_unavailable\"}",
    "method": "POST",
    "mode": "cors"
});y
