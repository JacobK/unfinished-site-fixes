// example URL: https://gamebanana.com/mods/44307

const modId = document.URL.split("/")[4];

// fetch the json info about the mod
const ProfilePageFetch = await fetch(`https://gamebanana.com/apiv8/Mod/${modId}/ProfilePage`, {
    "credentials": "include",
    "headers": {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin"
    },
    "method": "GET",
    "mode": "cors"
});
const ProfilePageResponse = await ProfilePageFetch.json();

// list all the download URLs in the console
ProfilePageResponse._aFiles.forEach(function(element){
	console.log(element._sFile, element._sDownloadUrl);
});
