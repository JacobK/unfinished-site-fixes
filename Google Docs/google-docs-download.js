// SPDX-FileCopyrightText: 2017 Nathan Nichols
// SPDX-FileCopyrightText: 2021,2023 Jacob K
//
// SPDX-License-Identifier: MIT

/*
Copyright (C) 2017 Nathan Nichols
Changes made by Jacob K in 2021, 2023, under the same license.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL NATHAN NICHOLS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

function parseURL(a){
	let url = new URL(a);
	var result = Object.create(null);
	for(let i of url.searchParams) {
	    result[i[0]] = i[1];
	}
	return JSON.stringify(result, undefined, 4); 
}
var docType; // could be document, spreadsheets, presentation
var docTypeSpecificOptions = ''; // radio buttons specific to the current docType
var id;
var url;
var opts_form = ''; // visible output
var publishedToWeb = false; // whether or not the document has been "published to the web"

function form_check(){
	console.log("boop");
	var format = "";
	var input = document.getElementsByTagName("input");
	if(input[input.length-1].value != "Expected 3 letter file extension"){
		format = input[input.length-1].value;
	} else{
		for(var i = 0; i < input.length-1; i++){
			console.log(input[i].value);
			if(input[i].value != "" && input[i].checked == true){
				format = input[i].value;
				break;
			}
			format = input[0].value; // This is applied at the very end of each loop so that if the "break;" line is never triggered (because no radio buttons are checked and nothing was typed), then the first button is what's used.
		}
		if(format == ""){
			return 0;		
		}
	}
	
	if (publishedToWeb) { // i.e. if the url contains the e section (checked in check_valid()
		// Supposed to look like this:
		// https://docs.google.com/DOCTYPE/d/e/ID/pub?output=FORMAT
		// See Vedran Šego's answer here: https://stackoverflow.com/questions/24255472/download-export-public-google-spreadsheet-as-tsv-from-command-line
		// The gid is ignored at the moment, so formats that only support one page will probably get only the first page rather than what's linked to
		var new_url = 'https://docs.google.com/'+docType+'/d/e/'+id+'/pub?output='+format;
	} else {
		// Supposed to look like this:
		// https://docs.google.com/DOCTYPE/export?format=FORMAT&id=ID // DOCTYPE can be "document", "presentation", or "spreadsheets"
		// Thanks to Alyssa Rosenzweig.
		var new_url = 'https://docs.google.com/'+docType+'/export?format='+format+'&id='+id;
	}
	document.location.assign(new_url);
	return 0;
}

// see if its a document or not.
function check_valid(){
	id = "no ID found.";
	var url_arr = document.location.href.split("/");
	for(var i = 0; i < url_arr.length-1; i++){
		if(url_arr[i] == "d"){
			docType = url_arr[i-1];
			console.log(docType);
			if (docType == "document") {
				docTypeSpecificOptions = '	<input type="radio" name="format" value="odt"> OpenDocument Text ("odt")<br>'+
					'	<input type="radio" name="format" value="pdf"> Portable Document Format ("pdf")<br>'+
					'	<input type="radio" name="format" value="epub"> ePub electronic publication ("epub")<br>'+
					'	<input type="radio" name="format" value="txt"> Plain text ("txt")<br>'+
					'	<input type="radio" name="format" value="docx"> Microsoft Word Document ("docx")<br>'+
					'	<input type="radio" name="format" value="rtf"> Rich Text Format ("rtf")<br>'+
					'	<input type="radio" name="format" value="zip"> Zipped web page ("zip")<br>'+
					'	<input type="radio" name="format" value="html"> HyperText Markup Language ("html")<br>';
			} else if (docType == "spreadsheets") {
				docTypeSpecificOptions = '	<input type="radio" name="format" value="ods"> OpenDocument Spreadsheet ("ods")<br>'+
					'	<input type="radio" name="format" value="pdf"> Portable Document Format ("pdf")<br>'+
					'	<input type="radio" name="format" value="csv"> Comma-separated values ("csv") (one page only)<br>'+
					'	<input type="radio" name="format" value="tsv"> Tab-separated values ("tsv") (one page only)<br>'+
					'	<input type="radio" name="format" value="xlsx"> Microsoft Excel Spreadsheet ("xlsx")<br>'+
					'	<input type="radio" name="format" value="zip"> Zipped web page ("zip")<br>';
			} else if (docType == "presentation") {
				docTypeSpecificOptions = '	<input type="radio" name="format" value="odp"> OpenDocument Presentation ("odp")<br>'+
					'	<input type="radio" name="format" value="pdf"> Portable Document Format ("pdf")<br>'+
					'	<input type="radio" name="format" value="pptx"> Microsoft PowerPoint Presentation ("pptx")<br>'+
					'	<input type="radio" name="format" value="txt"> Plain text ("txt")<br>'+
					'	<input type="radio" name="format" value="png"> Portable Network Graphics ("png") (one page only)<br>'+
					'	<input type="radio" name="format" value="svg"> Scalable Vector Graphics ("svg") (one page only)<br>'+
					'	<input type="radio" name="format" value="jpg"> JPEG image ("jpg") (one page only)<br>';
			} else {
				docTypeSpecificOptions = '	<input type="radio" name="format" value="pdf"> Portable Document Format ("pdf")<br>'+
					'	<input type="radio" name="format" value="txt"> Plain text ("txt")<br>'+
					'	<input type="radio" name="format" value="odt"> OpenDocument Text ("odt")<br>'+
					'	<input type="radio" name="format" value="ods"> OpenDocument Spreadsheet ("ods")<br>'+
					'	<input type="radio" name="format" value="odp"> OpenDocument Presentation ("odp")<br>'+
					'	<input type="radio" name="format" value="zip"> Zipped web page ("zip")<br>';
			}
			console.log(docTypeSpecificOptions);
			if(url_arr[i+1] == "e"){ // The "e" in the url seems to mean the document was "published to the web" rather than simply "shared".
				id = url_arr[i+2];
				publishedToWeb = true;
			} else {
				id = url_arr[i+1];
			}
			break;
		}
		if(url_arr[i].indexOf("id=") != -1){
			id = document.location.href.substring(url_arr[i].indexOf("id=")+3,document.location.href.length);
			break;
		}
	}
	console.log(id);
	var contains_apostrophe = "";//"You don't really want to store your data in a cloudy service do you?";
	opts_form =
				'	<br>'+
				'	<h1>Google Drive without non-free Javascript</h1>'+
				'	'+contains_apostrophe+'<br><br><br>'+
				'	<div id="opts">'+
				'	<b>Please select the format to download in:</b><br><br>'+
					docTypeSpecificOptions+'<br>'+
					'	Or try to get the document in a different format, '+
					'	("png", "avi", "pdf", etc..):<br><br>'+
					'	<input type="text" value="Expected 3 letter file extension"><br><br>'+
					'	<button id="submit">Go fetch the document<br>'+
				'	</div>'+
				''+
				''+
				'';
	console.log(opts_form);
	if(id == "no ID found."){
		// do nothing because it could be an info page or something
		console.log("no id found.");
		return false;
	}
	else {
		return true;
	}
}



function main(){
	
	if(check_valid()){	
		console.log("---------------------------------"+Date.now()+":Detected Google Drive."+"---------------------------------");
		window.stop();
		console.log("window stopped");
		document.head.innerHTML = "";
		console.log("opts_form:");
		console.log(opts_form);
		document.body.innerHTML = opts_form;
		document.body.style.textAlign = "center";
		document.getElementById("opts").style.textAlign = "left";	
		//document.getElementsById("opts").style.float = "left";	
		document.body.style.marginTop = "2%"
		document.body.style.marginLeft = "25%"
		document.body.style.marginRight = "25%"
		document.getElementById("submit").addEventListener("click",form_check);
	}
	return 0;
}
console.log("test");
main();
