/*
chat-aisafety-info
https://chat.aisafety.info/
*/

let lastResponseValue = null;

async function chatRequest(message) {
	const response = await await fetch("https://chat.stampy.ai:8443/chat", {
		"credentials": "omit",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:120.0) Gecko/20100101 Firefox/120.0",
		    "Accept": "text/event-stream",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "cross-site"
		},
		"referrer": "https://chat.aisafety.info/",
		"body": JSON.stringify({
			"sessionId":crypto.randomUUID(),
			"history":[{
				"role":"user",
				"content":message
			}],"settings":{"mode":"default"}
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	lastResponseValue = responseText;
	return responseText;
}

async function chat(message) {
	const fullLog = await chatRequest(message);
	const streamMatches = fullLog.matchAll(/data: {"state": "streaming", "content": "(.*)"}/g);
	let token = "";
	let fullString = "";
	streamMatches.next();
	while (token = streamMatches.next().value[1]) {
		fullString += token;
	}
	return fullString;
}
