/*
www-usajobs-gov-job
https://www.usajobs.gov/job/*
e.g.
https://www.usajobs.gov/job/773582100
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* Without running any JavaScript, shrinking your browser window makes the
	"Apply" button show up. For some reason the non-desktop version of the site
	(i.e. !`@media screen and (min-width: 62em)`) doesn't use JavaScript to show
	a apply button. */

const desktopApplyButton = document.querySelector(".usajobs-joa-actions__apply.usaj-hidden");
desktopApplyButton.style.setProperty("display", "revert", "important"); /*
	Setting the property to "revert" reverts *all* the CSS rules applied, not
	just the one that actually hides the button. Ideally, we'd only remove the
	last CSS element, but I'm not sure how to do that so for now it looks
	broken. TODO: investigate how to fix this */
desktopApplyButton.innerText = "Apply";

/* Show all revealable sections - TODO: Remove the "Read more" buttons and fix
	the transparencey */
for (elem of document.querySelectorAll("[data-object='reveal-more']")) {
  elem.style.height = "";
}
