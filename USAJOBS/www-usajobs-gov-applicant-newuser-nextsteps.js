/*
www-usajobs-gov-applicant-newuser-nextsteps
https://www.usajobs.gov/applicant/newuser/nextsteps/
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* This request seems to have removed the message about needing to create a
	profile even if it was already created. See
	`www.usajobs.gov_Archive [24-02-05 15-43-41].har` for further investigation.
	At one point during that log I got a popup that I accidentally clicked out
	of before I saw what it was. I don't think it was a frame. */
async function getUserPrefs() {
	const response = await fetch("https://www.usajobs.gov/Account/RetrieveUserPreferences", {
		"credentials": "include",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:121.0) Gecko/20100101 Firefox/121.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
	//    "referrer": "https://www.usajobs.gov/applicant/newuser/nextsteps/",
		"body": "referrer=https%3A%2F%2Fwww.usajobs.gov%2Fapplicant%2Fnewuser%2F",
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson.
}
