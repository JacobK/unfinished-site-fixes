/*
www-usajobs-gov-applicant-application-resumes
https://www.usajobs.gov/applicant/application/773582100/resumes/
should be https://www.usajobs.gov/applicant/application/<*>/resumes/
e.g.
https://www.usajobs.gov/applicant/application/773582100/resumes/
use with:
htmx // NOTE: ERROR WITH MANY REPEATED REQUESTS TODO: FIX THIS
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

function checkHidden(elem) {
	return elem.classList.contains("hidden");
}

function hideElement(elem) {
	return elem.classList.add("hidden");
}

function unhideElement(elem) {
	return elem.classList.remove("hidden");
}

function switchHidden(elem) {
	if (checkHidden(elem)) {
		unhideElement(elem);
	} else {
		hideElement(elem);
	}
}

function enableToggling() {
	/* Based on web searches, I think this is a Bootstrap custom attribute, but
		I can't seem to get it to work with an actual Bootstrap script. */
	for (toggleElem of document.querySelectorAll("[data-toggle]")) {
		if (!toggleElem.getAttribute("data-target")) { /* If there's no target,
			throw an error and skip this elem */
			console.error("Toggle element missing target", toggleElem);
			continue;
		}
		if (toggleElem.getAttribute("data-toggle") == "toggle") {
			const targetElemId = toggleElem.getAttribute("data-target");
			if (!document.getElementById(targetElemId)) { /* If the target is not on
				the page, also throw an error. */
				console.error("Toggle element target missing from page", toggleElem);
				continue;
			}
			const targetElem = document.getElementById(targetElemId);
			toggleElem.addEventListener("click", function (ev) {
				switchHidden(targetElem);
				ev.preventDefault();
			});
		} else if (toggleElem.getAttribute("data-toggle") == "modal") {
			const targetElemIds = toggleElem.getAttribute("data-target").split(',');
			toggleElem.addEventListener("click", function (ev) {
				unhideElement(document.getElementById(targetElemIds[0]));
				hideElement(document.getElementById(targetElemIds[1]));
				ev.preventDefault();
			});
		} else if (toggleElem.getAttribute("data-toggle") == "dismiss") {
			const targetElemId = toggleElem.getAttribute("data-target");
			if (!document.getElementById(targetElemId)) { /* If the target is not on
				the page, also throw an error. */
				console.error("Toggle element target missing from page", toggleElem);
				continue;
			}
			const targetElem = document.getElementById(targetElemId);
			toggleElem.addEventListener("click", function (ev) {
				hideElement(targetElem);
				ev.preventDefault();
			});
		} else if (toggleElem.getAttribute("data-toggle") == "open" || toggleElem.getAttribute("data-toggle") == "") {
			const targetElemId = toggleElem.getAttribute("data-target");
			if (!document.getElementById(targetElemId)) { /* If the target is not on
				the page, also throw an error. */
				console.error("Toggle element target missing from page", toggleElem);
				continue;
			}
			const targetElem = document.getElementById(targetElemId);
			toggleElem.addEventListener("click", function (ev) {
				unhideElement(targetElem);
				ev.preventDefault();
			});
		} else {
			console.warn(`toggle type on page not supported: ${toggleElem.getAttribute("data-toggle")}`, toggleElem);
		}
	}
}

function main() {
	enableToggling();
}
main();

// On the final page, I had to manually run `document.forms[1].submit()` after opening the box with the loading bar.
