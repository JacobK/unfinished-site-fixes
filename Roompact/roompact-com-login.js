/*
roompact-com-login
https://roompact.com/login
*/


async function userIdLookup(userEmail) {
	const response = await fetch(`https://roompact.com/login/userIdLookup?username=${encodeURIComponent(userEmail)}`, {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://roompact.com/login",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

document.querySelector(".home-login-continue-btn").addEventListener("click", function() {
	userIdLookup(document.getElementById("login-input").value).then((responseJson) => {
		document.location.assign(responseJson.login_url);
	});
});
