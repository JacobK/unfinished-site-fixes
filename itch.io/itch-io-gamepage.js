/*
itch-io-gamepage
https://*.itch.io/*
*/

// SPDX-FileCopyrightText: 2023-2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

async function getUploadInfo(uploadId) {
	const response = await fetch(`${document.URL}/file/${uploadId}?source=view_game&as_props=1&after_download_lightbox=true`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://ceykie.itch.io/anhedonia",
		"body": `csrf_token=${formUrlEncode(document.querySelector("meta[name='csrf_token']").getAttribute("value"))}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

for (dlButton of document.querySelectorAll(".download_btn")) {
	dlButton.addEventListener("click", async function(ev) {
		const uploadInfo = await getUploadInfo(ev.target.getAttribute("data-upload_id"));
		document.location.href = uploadInfo.url;
		console.info(uploadInfo.lightbox);
	});
}

// Show extended game info
for (infoElem of document.querySelectorAll(".view_game_page .info_panel_wrapper")) {
	infoElem.style.display = "revert";
}
