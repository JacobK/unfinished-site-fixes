/*
itch-io-jam
https://itch.io/jam/*

https://itch.io/jam/<*>/entries
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later


// for /jam/*
document.querySelector("idk").addEventListener("click", async function(ev) {
	const reponse = await fetch(ev.target.getAttribute("data-href"), {
		"method": "POST"
	});
	const responseJson = await response.json();
	if (responseJson.success == true) {
		document.location.assign(responseJson.redirect_to);
	}
});



if (document.URL.match(/https:\/\/itch\.io\/jam\/[^\/]*/)

// for/jam/<*>/entries
async function getEntries() {
	const response = await fetch("https://itch.io/jam/382779/entries.json", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://itch.io/jam/sgda-fall-2023-game-jam/entries",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
