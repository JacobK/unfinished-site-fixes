# unfinished-site-fixes

This is a repository for fixes I started writing for Haketilo but didn't finish enough to publish as a Hydrilla package. Many of these scripts don't do anything useful yet, but some of them work if you type some commands in the developer console after running them. For fixes that are functional without opening the dev console, see https://codeberg.org/JacobK/site-fixes

These scripts do not come with index.json files or license headers in them. They are all (with the exception of files or sections copied from other sources, noted towards the top of the file or at the relevant section) licensed as GPLv3-or-later (as I selected from the dropdown menu, though I don't see anything suggesting "-or-later" after publishing), with some additional permissions noted below. When I move them to the finished repository, I will add the proper header to all files, but for now, the following license notice applies to all files in this repository, unless otherwise noted (for example when I copy source code from others):
```
/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright (C) 2022 Jacob K
 * Copyright (C) 2023 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
```

The above license also applies to files in this repository that start with:
```
// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later
```
