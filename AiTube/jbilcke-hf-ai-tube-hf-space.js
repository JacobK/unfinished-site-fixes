/*
jbilcke-hf-ai-tube-hf-space
https://jbilcke-hf-ai-tube.hf.space/
*/

// TODO: Ask for a license here: https://huggingface.co/spaces/jbilcke-hf/ai-tube/discussions

async function getVideos() {
	const response = await fetch(document.location, {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:121.0) Gecko/20100101 Firefox/121.0",
		    "Accept": "text/x-component",
		    "Accept-Language": "en-US,en;q=0.5",
	        "Next-Action": "c3268bb0d8b045e2cd538bb01b027537d4264bf5", // from: https://jbilcke-hf-ai-tube.hf.space/_next/static/chunks/244-75d8eb03414372c4.js
	//        "Next-Router-State-Tree": "%5B%22%22%2C%7B%22children%22%3A%5B%22__PAGE__%22%2C%7B%7D%5D%7D%2Cnull%2Cnull%2Ctrue%5D",
		    "Content-Type": "text/plain;charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
	//    "referrer": "https://jbilcke-hf-ai-tube.hf.space/",
		"body": "[{\"sortBy\":\"date\",\"mandatoryTags\":[],\"maxVideos\":25}]",
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	const videosJson = JSON.parse(responseText.match("\n(.*)\n$")[1].match(/1:(\[\{.*)/)[1]);
	return videosJson;
}

function renderVideos(videosArr) {
	document.write(`<div id="videosDiv"></div>`);
	const videosDiv = document.getElementById("videosDiv");
	for (video of videosArr) {
		const videoElem = document.createElement("a");
		videoElem.href = `https://jbilcke-hf-ai-tube.hf.space/watch?v=${video.id}`;
		const videoThumbnail = document.createElement("img");
		videoThumbnail.src = video.thumbnailUrl;
		videoThumbnail.style.width = "20em";
		videoElem.appendChild(videoThumbnail);
		const videoTitle = document.createElement("h4");
		videoTitle.innerText = video.label;
		videoElem.appendChild(videoTitle);
		const channelTitle = document.createElement("h5");
		channelTitle.innerText = video.channel.label;
		videoElem.appendChild(channelTitle);
		videosDiv.appendChild(videoElem);
	}
	videosDiv.style.display = "flex";
	videosDiv.style["flex-wrap"] = "wrap";
}

async function main() {
	const videos = await getVideos();
	renderVideos(videos);
}

main();
