/*
graphql-faq
https://graphql.org/faq/
*/

/* Thanks to <https://stackoverflow.com/a/55578854> for helping me learn the
	syntax to combine multiple querySelector calls into one. */
for (elem of document.querySelectorAll(".inner-faq-content p,.inner-faq-content ul")) { /* This
		selects all elements of type "p" (i.e. paragraph) that are within an
		element with class "inner-faq-content", as well as elements of type "ul"
		(i.e. unordered list) that are within an element with class
		"inner-faq-content". */
	elem.style.maxHeight = "revert";
	elem.style.opacity = "revert";
}

// TODO: fix "For example, GraphQL enables you to:"
