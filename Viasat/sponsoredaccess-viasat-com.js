/*
sponsoredaccess-viasat-com
https://sponsoredaccess.viasat.com/americanairlines
*/

function getDeviceIdFromUrl() {
	return new URLSearchParams(document.URL.split('?')[1].split('/')[0]).get("deviceId");
}

async function getDeviceInfo(deviceId) {
	await fetch("https://sponsoredaccess.viasat.com/engage-advertising-service/api/v1/device", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:112.0) Gecko/20100101 Firefox/112.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "venueCode": "aal",
		    "deviceId": deviceId,
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://sponsoredaccess.viasat.com/americanairlines/",
		"method": "GET",
		"mode": "cors"
	});
}

async function getOfferOptions(deviceId) {
	const response = await fetch("https://sponsoredaccess.viasat.com/engage-advertising-service/api/v1/offers", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:112.0) Gecko/20100101 Firefox/112.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "deviceId": deviceId,
		    "venueCode": "aal",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://sponsoredaccess.viasat.com/americanairlines/",
		"body": "{\"placements\":[{\"divName\":\"items\",\"count\":5,\"siteName\":\"aal_connect\",\"eventIds\":[40,900],\"adTypes\":[18]}]}",
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// offerId and deviceId both look like UUIDs
// deviceId can be found in LocalStorage state json, if you've run the site-served scripts
async function activateWifi(offerId, deviceId) {
	const response = await fetch(`https://sponsoredaccess.viasat.com/engage-advertising-service/api/v1/offers/${offerId}/activate`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:112.0) Gecko/20100101 Firefox/112.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "deviceId": deviceId,
		    "venueCode": "aal", // guessing this stands for American Airlines
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://sponsoredaccess.viasat.com/americanairlines/",
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	if (responseJson.status != "SUCCESS") {
		console.warn("Unexpected response");
		console.log(responseJson);
	}
	return responseJson;
}

async function main() {
	const deviceId = getDeviceIdFromUrl();
	const offersJson = await getOfferOptions(deviceId);
	document.write(JSON.stringify(offersJson));
}

main();
