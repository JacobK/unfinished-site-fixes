// When clicking "All Filters", show more (not all for some reason) filters and hide the button clicked
document.getElementsByClassName("AppFilterBar_mobileText__u3_dd")[0].addEventListener("click", function(){
	document.getElementsByClassName("AppFilterBar_filterInnerWrapper__MfqE1")[0].className = "";
	document.getElementsByClassName("AppFilterBar_wrapper__igAN5")[0].remove();
});

// get __NEXT_DATA__ JSON
const nextData = JSON.parse(document.getElementById("__NEXT_DATA__").text);

//using data from nextData.props.pageProps.availableFilters
// for each category of filters, set up all of the options to link to their filters
[...document.getElementsByClassName("AppFilterBar_filters__fNqXa")].forEach(function(filterCategory){
	if (filterCategory.getAttribute("data-testid") == "filter-platforms") {
		[...filterCategory.children].forEach(function(filterOption){
			const optionText = filterOption.textContent;
			nextData.props.pageProps.availableFilters.platformsFilters.forEach(function(option){
				if (option.text == optionText) {
					filterOption.addEventListener("click", function(){
						console.log(option.urlName);
					});
				}
			});
		});
	} else if (filterCategory.getAttribute("data-testid") == "filter-features") {
		[...filterCategory.children].forEach(function(filterOption){
			const optionText = filterOption.textContent;
			nextData.props.pageProps.availableFilters.featureFilters.forEach(function(option){
				if (option.text == optionText) {
					filterOption.addEventListener("click", function(){
						console.log(option.urlName);
					});
				}
			});
		});
	} else if (filterCategory.getAttribute("data-testid") == "filter-license") {
		[...filterCategory.children].forEach(function(filterOption){
			const optionText = filterOption.textContent;
			nextData.props.pageProps.availableFilters.licenseFilters.forEach(function(option){
				if (option.text == optionText) {
					filterOption.addEventListener("click", function(){
						console.log(option.urlName);
					});
				}
			});
		});
	}
});

//JSON.parse(document.getElementById("__NEXT_DATA__").text).props.pageProps.categories.items

// Also ?lw=1 shows alts with a legal warning, and there's also a way to sort by rank, etc.
