/*
	identifier: alternativeto-homepage
	long name: Homepage for AlternativeTo
	description: Fix search on alternativeto.net
	URL pattern: https://alternativeto.net
*/

document.getElementById("search-input").nextSibling.disabled = false;
document.getElementById("search-input").nextSibling.style.cursor = "";
