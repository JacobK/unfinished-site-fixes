// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/*
textsynth-com-playground
https://textsynth.com/playground.html
*/
/* This program doesn't work but I don't understand why. It's something to do
	with CORS it seems. */

function getApiKey() {
	for (script of document.scripts) {
		const matchArr = script.textContent.match(/var textsynth_api_key = "([^"]*)"/);
		if (matchArr) {
			return matchArr[1];
		}
	}
	return null;
}

const apiKey = getApiKey();

/* prompt is a string. temperature, topK, topP, and maxTokens are numbers. */
async function completions(engine, prompt, temperature, topK, topP, maxTokens, stopAfter) {
	const response = await fetch(`https://api.textsynth.com/v1/engines/${engine}/completions`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Authorization": `Bearer ${apiKey}`,
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://textsynth.com/",
		"body": JSON.stringify({
			"prompt": prompt,
			"temperature": temperature,
			"top_K": topK,
			"top_p": topP,
			"max_tokens": maxTokens,
			"stream": false, /* Assuming you get the same amount of text with or
				 without streaming, without streaming seems simpler to implement.
				 If you change this to true, you will need to use .text()
				 instead of .json() on the response, because .json() is for a
				 single JSON string, and the stream will send several in one
				 response. */
			"stop": stopAfter
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

function hideAllButtons() {
	for (button of document.querySelectorAll("button")) {
		button.style = "display: none;";
	}
}

function showAllButtons() {
	for (button of document.querySelectorAll("button")) {
		button.style = "display: inline;";
	}
}

/* Get machine-generated text using the human-generated text as the prompt, and
	then write the result to the screen. */
async function genFromHumanText() {
	hideAllButtons();
	const prompt = document.getElementById("input_text").value;
	document.getElementById("gtext").innerHTML = `<b>${prompt}</b>`;
	const completionsJson = await completions(document.getElementById("model").value, prompt, document.getElementById("temperature").value, document.getElementById("topk").value, document.getElementById("topp").value, document.getElementById("max_tokens").value, document.getElementById("stop").value);
	document.getElementById("gtext").textContent += completionsJson.text;
	showAllButtons();
	return document.getElementById("gtext").textContent;
}

/* Get machine-generated text using the onscreen machine-generated text as a
	prompt, and then write the result to the screen. */
async function genFromMachineText() {
	hideAllButtons();
	const prompt = document.getElementById("gtext").textContent;
	const completionsJson = await completions(document.getElementById("model").value, prompt, document.getElementById("temperature").value, document.getElementById("topk").value, document.getElementById("topp").value, document.getElementById("max_tokens").value, document.getElementById("stop").value);
	document.getElementById("gtext").textContent += completionsJson.text;
	showAllButtons();
	return document.getElementById("gtext").textContent;
}

document.getElementById("submit_button").addEventListener("click", genFromHumanText);
document.getElementById("more_button").addEventListener("click", genFromMachineText);
