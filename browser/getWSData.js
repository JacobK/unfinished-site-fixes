/* Special thanks to < https://chat.lmsys.org/?arena > and gpt-4-turbo for
	letting me know about the Browser Toolbox, as well as helping fix some
	bugs. */

/* This will get all of the WS Data corresponding to the 101 request that is
	currently being inspected. */

bigString = "";
for (row of document.querySelector("html").querySelector("browser.devtools-toolbox-bottom-iframe").contentDocument.querySelector("iframe#toolbox-panel-iframe-netmonitor").contentDocument.querySelector(".message-list-body").querySelectorAll("tr")) {
  //console.log(row);
	if (row.querySelector(".message-list-payload")) {
		bigString += row.querySelector(".message-list-payload").textContent;
		bigString += "\n\n";
	}
}
if (veryBigString) {
	veryBigString += bigString;
	veryBigString += "\n\n\n";
}
console.log(bigString);
