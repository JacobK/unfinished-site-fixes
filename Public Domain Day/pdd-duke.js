/*
ppd-duke
https://web.law.duke.edu/cspd/publicdomainday/2023/
*/

// just remove the pictures for now so they don't get in the way
[...document.getElementsByClassName("pics")].forEach(function(picDiv){
	picDiv.remove();
});
