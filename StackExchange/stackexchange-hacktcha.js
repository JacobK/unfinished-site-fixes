/* SPDX-License-Identifier: CC0-1.0
 *
 * docs-google-com-fix-forms-hacktcha, but trivially modified by Jacob K.
 *
 * Copyright (C) 2022 Wojtek Kosior
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

"use strict";

// next couple functions may need to be different for each site
function getRecaptchaDiv() {
	return document.querySelector("div#no-captcha-here");
}

function getSiteKey() {
	for (let script of document.scripts) {
		const matchArr = script.textContent.match(/'sitekey': '([^']*)',/);
		if (matchArr) {
			return matchArr[1];
		}
	}
}

(async () => {
    const recaptcha_div = getRecaptchaDiv();

    if (recaptcha_div === null)
	return;

    recaptcha_div.style.display = "block";

    const site_key = getSiteKey();

    const token_field = document.createElement("textarea");
    token_field.name = "g-recaptcha-response";
    token_field.style.display = "none";
    recaptcha_div.before(token_field);

    let authenticated = false;

    /* Function called from google_forms.js. */
    window.hacktcha_completed = function() {
	return authenticated;
    }

    const submit_buttons = [...document.querySelectorAll('[jsname=M2UYVd]')];

    for (const button of submit_buttons) {
	button._color_orig = getComputedStyle(button)["background-color"];
	button.style.backgroundColor = "lightgray";
    }

    for await (const token of HCHA.run(recaptcha_div, site_key)) {
	if (token === null) {
	    authenticated = false;

	    for (const button of submit_buttons)
		button.style.backgroundColor = "lightgray";
	} else {
	    authenticated = true;

	    for (const button of submit_buttons)
		button.style.backgroundColor = button._color_orig;

	    token_field.value = token;
	}
    }
})();
