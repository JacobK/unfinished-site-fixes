// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: CC0-1.0

/*
stackexchange-ask
https://*.stackexchange.com/questions/ask
*/

document.getElementById("post-form").querySelector("button.js-begin-review-button").addEventListener("click", function() {
  document.getElementById("post-form").submit();
})
