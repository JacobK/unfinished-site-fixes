/*
speedrun-com
https://www.speedrun.com/***
*/

// This script sets innerHTML and outerHTML, as well as sending the time.

function getGameName() {
	let gameName = document.URL.split('/')[3]; // get game or series name from URL
	if (gameName == "games" || gameName == "series") { // /games is for all games
		gameName = "";
	}
	return gameName
}

/* begin functions for list pages, which includes /games, /series, and series pages,
	like /doom. */

/* official app can read from a URL like
	"https://www.speedrun.com/games#platform=32X&orderby=mostplayers&unofficial=off"
	to parse sharable links. */

function getPlatformName() {
	if (document.getElementById("platform")) {
		return document.getElementById("platform").value; // will be "" for "All Platforms"
	} else {
		return "";
	}
}

function getUnofficialString() {
	if (document.getElementById("checkbox-unofficial") && document.getElementById("checkbox-unofficial").checked) {
		return "on";
	} else {
		return "off";
	}
}

function getOrderBy() {
	return document.getElementById("orderby").value;
}

function getSeriesVal() {
	if (document.URL.split('/')[3] == "series") {
		return "1";
	} else {
		return "";
	}
}

function getAjaxGamesUrl(start) {
	return `https://www.speedrun.com/ajax_games.php?game=${getGameName()}&platform=${getPlatformName()}&unofficial=${getUnofficialString()}on&orderby=${getOrderBy()}&title=&series=${getSeriesVal()}&start=${start}`;
}

async function renderList() {
	const resp = await fetch(getAjaxGamesUrl(0));
	const respHtml = await resp.text();
	document.getElementById("list").innerHTML = respHtml;
	document.querySelector("a.button-loadmore").addEventListener("click", extendList);
}

async function extendList() {
	const resp = await fetch(getAjaxGamesUrl(document.querySelector("a.button-loadmore").getAttribute("data-start")));
	const respHtml = await resp.text();
	document.querySelector("a.button-loadmore").parentElement.outerHTML = respHtml;
	document.querySelector("a.button-loadmore").addEventListener("click", extendList);
}

function addInitialListeners() {
	if (document.getElementById("platform")) {
		document.getElementById("platform").addEventListener("change", renderList);
	}
	if (document.getElementById("orderby")) {
		document.getElementById("orderby").addEventListener("change", renderList);
	}
	if (document.getElementById("checkbox-unofficial")) {
		document.getElementById("checkbox-unofficial").addEventListener("change", renderList);
	}
}

// end functions for list pages

// begin functions for game pages

function atoj(base64) {
	return JSON.parse(atob(base64));
}

function getLeaderboardWidgetData() {
	const widget = document.querySelector("div[component-name='GameLeaderboardWidget']");
	return atoj(widget.getAttribute("component-data"));
}

function getGameId() {
	return getLeaderboardWidgetData().gameId;
	//return document.querySelector("img.cover-tall-48.bs-border").src.split('/')[4];
	/* Not sure how the official script gets this; there's a "link" response
		header in the page request, but I don't know how to see that with
		JavaScript without fetching the page again. */
}

function getVary() {
	return getLeaderboardWidgetData().vary;
	//return Math.floor(new Date().getTime() / 1000);
}

function jtoa(json) {
	return btoa(JSON.stringify(json));
}

async function getGameData() {
	const json = {
		"gameId": getGameId(),
		"vary": getVary()
	};
	const response = await fetch(`https://www.speedrun.com/api/v2/GetGameData?_r=${jtoa(json)}`);
	const responseJson = await response.json();
	return responseJson;
}

async function getGameSummary() {
	const json = {
		"gameUrl": getGameName(),
		"vary": getVary()
	};
	const response = await fetch(`https://www.speedrun.com/api/v2/GetGameSummary?_r=${jtoa(json)}`);
	const responseJson = await response.json();
	return responseJson;
}

async function getLatestGameLeaderboard() {
	const json = {
		"gameUrl": getGameName(),
		"vary": getVary(),
		"limit": 7
	};
	const response = await fetch(`https://www.speedrun.com/api/v2/GetLatestLeaderboard?_r=${jtoa(json)}`);
	const responseJson = await response.json();
	return responseJson;
}

function getValueObj(variableId, valueIdsArr) {
	return {
		"variableId": variableId,
		"valueIds": valueIdsArr
	}
}

async function getGameLeaderboard(categoryId, valuesArr) {
	const json = {
		"params": {
		"gameId": getGameId(),
		"categoryId": categoryId,
		"values": valuesArr,
		"timer": 0,
		"regionIds": [],
		"platformIds": [],
		"emulator": 1,
		"video": 0,
		"obsolete": 0
		},
		"page": 1,
		"vary": getVary()
	};
	const response = await fetch(`https://www.speedrun.com/api/v2/GetGameLeaderboard?_r=${jtoa(json)}`);
	const responseJson = await response.json();
	return responseJson;
}

function prettyPrintTime(timeInSeconds) {
	const hours = Math.floor(timeInSeconds / (60*60));
	timeInSeconds = timeInSeconds % (60*60);
	const minutes = Math.floor(timeInSeconds / 60);
	timeInSeconds = timeInSeconds % 60;
	return `${hours} hours, ${minutes} minutes, ${timeInSeconds} seconds`;
}

function prettyPrintDate(dateInSecondsSinceEpoch) {
	return new Date(dateInSecondsSinceEpoch*1000).toString();
}

async function renderGameLeaderboard() {
	const widget = document.querySelector("div[component-name='GameLeaderboardWidget']");
	const gameData = await getGameData();
	
	const categoriesDiv = document.createElement("div");
	for (category of gameData.categories) {
		const catButton = document.createElement("button");
		catButton.innerText = category.name;
		if (category.isMisc) {
			catButton.style["background-color"] = "gray";
		}
		catButton.setAttribute("data-categoryid", category.id);
		// TODO: add event listener to the button
		categoriesDiv.appendChild(catButton);
	}
	widget.appendChild(categoriesDiv);
	
	const variablesDiv = document.createElement("div");
	for (variable of gameData.variables) {
		if (variable.categoryId == gameData.categories[0].id) { // TODO: change the second operand to refer to an element on the page
			const selectElem = document.createElement("select");
			selectElem.setAttribute("data-variableid", variable.id);
			for (value of gameData.values) {
				if (value.variableId == variable.id) {
					const optElem = document.createElement("option");
					optElem.innerText = value.name;
					optElem.value = value.id;
					selectElem.appendChild(optElem);
				}
			}
			// TODO: add event listener to select elem
			variablesDiv.appendChild(selectElem);
		}
	}
	widget.appendChild(variablesDiv);
	
	// TODO: add button to show rules
	// TODO: add button to show filters
	
	let variablesArr = [];
	for (varElem of variablesDiv.querySelectorAll("select")) {
		variablesArr.push(getValueObj(varElem.getAttribute("data-variableid"), [varElem.value]))
	}
	const leaderboardData = await getGameLeaderboard(gameData.categories[0].id, variablesArr); // TODO: change the first argument to refer to an element on the page
	
	const leaderboardDiv = document.createElement("div");
	const leaderboardTable = document.createElement("table");
	const leaderboardTableHead = document.createElement("thead");
	const leaderboardTableHeadRow = document.createElement("tr");
	for (headerString of ["Place", "Player", "Time", "Date"]) {
		const thElem = document.createElement("th");
		thElem.innerText = headerString;
		leaderboardTableHeadRow.appendChild(thElem);
	}
	leaderboardTableHead.appendChild(leaderboardTableHeadRow);
	leaderboardTable.appendChild(leaderboardTableHead);
	const leaderboardTableBody = document.createElement("tbody");
	for (run of leaderboardData.leaderboard.runs) {
		const rowElem = document.createElement("tr");
		const placeTd = document.createElement("td");
		placeTd.innerText = run.place;
		rowElem.appendChild(placeTd);
		const playerTd = document.createElement("td");
		playerTd.innerText = run.playerIds[0];
		rowElem.appendChild(playerTd);
		const timeTd = document.createElement("td");
		timeTd.innerText = prettyPrintTime(run.time);
		rowElem.appendChild(timeTd);
		const dateTd = document.createElement("td");
		dateTd.innerText = prettyPrintDate(run.date);
		rowElem.appendChild(dateTd);
		leaderboardTableBody.appendChild(rowElem);
	}
	leaderboardTable.appendChild(leaderboardTableBody);
	leaderboardDiv.appendChild(leaderboardTable);
	widget.appendChild(leaderboardDiv);
}

async function renderGamePageComponents() {
	// can use `document.querySelectorAll("div[component-name]")` to find more components
	renderGameLeaderboard();
}

// end functions for game pages

addInitialListeners(); // for list pages
