/*
docs-google-com-forms-viewanalytics
e.g.
https://docs.google.com/forms/d/e/1FAIpQLScsj0Yibs4rJjHWjIRVb3wwHsgVS64qYOAoUWqRwrXEfgVeag/viewanalytics?usp=form_confirm
*///https://docs.google.com/forms/d/e/*/viewanalytics

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

const data = JSON.parse(document.scripts[3].textContent.match(/ANALYTICS_LOAD_DATA_ = (.*);/)[1]);
const questions = data[0][1][1];
