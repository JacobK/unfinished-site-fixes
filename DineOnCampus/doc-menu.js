/*
	identifier: doc-menu
	long name: Menu for DineOnCampus
	description: show menus hosted by DineOnCampus
	URL pattern: idk
	example URLs:
		https://dineoncampus.com/utarlington/whats-on-the-menu
		https://www.dineoncampus.com/tamu/whats-on-the-menu
		
*/

// currently targeting https://dineoncampus.com/*/whats-on-the-menu only
/*	some other examples I found (Some of these pages may work without JavaScript; I haven't checked.):
	https://dineoncampus.com/uh/feedback
	https://dineoncampus.com/tamu/hours-of-operation
	https://dineoncampus.com/tamu/food-trucks
	https://dineoncampus.com/unccharlotte/hours-of-operation
	https://dineoncampus.com/utdallasdining/students-in-residence
	https://dineoncampus.com/utdallasdining/general-information
	https://dineoncampus.com/unccharlotte/feedback
	https://dineoncampus.com/utarlington/inclement-dining-hours-notice--february-17th
	https://dineoncampus.com/uh/vegetarian-and-vegan-options
	https://dineoncampus.com/tamu/our-safety-measures
	https://dineoncampus.com/pitt/for-residents
	https://dineoncampus.com/tamu/
	https://www.dineoncampus.com/northwestern/meal-exchange-pilot-program
	https://www.dineoncampus.com/Documents/uwf/Catering/UWF%20Commuter.pdf
	http://www.dineoncampus.com/tamug/show.cfm?cmd=menus2
*/


// TODO: Actually show a menu lol (currently you can look at requests (for breakfast at least))

const platform = 0; // unknown meaning
const for_menus = true; // unknown meaning
const with_address = false; // unknown meaning
const with_buildings = true; // unknown meaning

const site = document.URL.split("/")[3];
const slug = document.URL.split("/")[4]; // "whats-on-the-menu", for example (assuming it is that, currently)

let lastMenu = null; // stores the most recent menu response, so it can be interacted with on the command line after requesting a menu

// get the menu for a given location on a given date (will show the default period (e.g. breakfast (but the id will be arbitrary)) as well as a list of other periods you can call)
async function getMenuAtLocation(location, date) { // date formatted like "2022-5-5"
	const menu_fetch = await fetch(`https://api.dineoncampus.com/v1/location/${location}/periods?platform=${platform}&date=${date}`, {
		"credentials": "omit",
		"headers": {
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"method": "GET",
		"mode": "cors"
	});
	return await menu_fetch.json();
}

// get the menu for a given location during a given period of a given date
async function getMenuAtLocationForPeriod(location, period, date) {
	const menu_fetch = await fetch(`https://api.dineoncampus.com/v1/location/${location}/periods/${period}?platform=${platform}&date=${date}`, {
		"credentials": "omit",
		"headers": {
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"method": "GET",
		"mode": "cors"
	});
	return await menu_fetch.json();
}

async function main() {

// get info about the site, specifically the site id
const info_fetch = await fetch(`https://api.dineoncampus.com/v1/sites/${site}/info`, {
    "credentials": "omit",
    "headers": {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site"
    },
    "method": "GET",
    "mode": "cors"
});
const info_response = await info_fetch.json();
const site_id = info_response.site.id;
const local_date = info_response.site.local_date;

// get some superfluous info
const info_by_slug_fetch = await fetch(`https://api.dineoncampus.com/v1/sections/info_by_slug?site_id=${site_id}&slug=${slug}`, {
    "credentials": "omit",
    "headers": {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site"
    },
    "method": "GET",
    "mode": "cors"
});
const info_by_slug_response = await info_by_slug_fetch.json();

function listMenuItems(lastMenu) {
	console.log("Period: ", lastMenu.menu.periods.name);
	lastMenu.menu.periods.categories.forEach(function(element){
		console.log("Category: ", element.name);
		element.items.forEach(function(element){
			console.log(element.name, " ingredients: ", element.ingredients);
		});
	});
}

// get the list of locations
const all_locations_fetch = await fetch(`https://api.dineoncampus.com/v1/locations/all_locations?platform=${platform}&site_id=${site_id}&for_menus=${for_menus}&with_address=${with_address}&with_buildings=${with_buildings}`, {
    "credentials": "omit",
    "headers": {
        "Accept": "application/json, text/plain, */*",
		"Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site"
    },
    "method": "GET",
    "mode": "cors"
});
const all_locations_response = await all_locations_fetch.json();
// show all the buildings and locations
all_locations_response.buildings.forEach(function(building){
	console.log(building.name);
	let buildingElement = document.createElement("h2");
	buildingElement.textContent = building.name;
	document.getElementById("app").appendChild(buildingElement);
	building.locations.forEach(function(location){
		console.log(location.name);
		let locationElement = document.createElement("h6");
		locationElement.textContent = location.name;
		// when clicking on a location, get that location's menu (but don't show it (yet to be programmed))
		locationElement.addEventListener("click", async function(ev){
			lastMenu = await getMenuAtLocation(location.id, local_date);
			for (period of (lastMenu.periods)) {
				const newButton = document.createElement("button");
				newButton.innerText = period.name;
				const thisPeriod = period;
				newButton.addEventListener("click", async function() {
					const nextMenu = await getMenuAtLocationForPeriod(location.id, thisPeriod.id, local_date);
					listMenuItems(nextMenu);
				});
				//ev.target.appendChild(newButton); // <-- will cause buttons inside of buttons
				ev.target.insertAdjacentElement('afterend', newButton);
			}
			// log the name of all food items in the defualt period for that location (TODO: show this in an onscreen popup or something)
			//listMenuItems(lastMenu);
		});
		buildingElement.appendChild(locationElement);
	});
});

// get the list of upcoming events (superfluous)
const upcoming_events_fetch = await fetch(`https://api.dineoncampus.com/v1/events/upcoming_events?site_id=${site_id}`, {
    "credentials": "omit",
    "headers": {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site"
    },
    "method": "GET",
    "mode": "cors"
});
const upcoming_events_response = await upcoming_events_fetch.json();

}

main();
