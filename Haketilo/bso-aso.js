/*
	identifier: bso-aso
	longname: Randomizer for Bso/Aso
	description: Prints a random URL pattern from the Block scripts on/Allow scripts on page.
	URL pattern: moz-extension://ac14f79a-6191-4469-bdcd-98d50d8ab9ae/html/settings.html
*/

// getRandomInt is from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
// licensed under CC0 or MIT (unclear, see https://developer.mozilla.org/en-US/docs/MDN/About#Copyrights_and_licenses)
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

let listString = "";
[...document.getElementsByClassName("text_entry_noneditable")].forEach(function(item){
	listString = listString + item.children[0].textContent + "\n";
});
console.info("Here is a list of every URL pattern:", listString);

let listStringBlocked = "";
[...document.querySelector("#blocking_list_container").getElementsByClassName("text_entry_noneditable")].forEach(function(item){
	listStringBlocked = listString + item.children[0].textContent + "\n";
});
console.info("Here is a list of every blocked URL pattern:", listStringBlocked);

let listStringAllowed = "";
[...document.querySelector("#allowing_list_container").getElementsByClassName("text_entry_noneditable")].forEach(function(item){
	listStringAllowed = listString + item.children[0].textContent + "\n";
});
console.info("Here is a list of every allowed URL pattern:", listStringAllowed);


console.info(`There are ${document.querySelector("#blocking_list_container").getElementsByClassName("text_entry_noneditable").length + document.querySelector("#allowing_list_container").getElementsByClassName("text_entry_noneditable").length} URL patterns saved. ${document.querySelector("#blocking_list_container").getElementsByClassName("text_entry_noneditable").length} of those are blocked, and ${document.querySelector("#allowing_list_container").getElementsByClassName("text_entry_noneditable").length} of those are allowed.`);

console.info("Here's a random URL pattern to try and fix!", document.getElementsByClassName("text_entry_noneditable")[getRandomInt(document.getElementsByClassName("text_entry_noneditable").length)].children[0].textContent);
console.info("Here's a random allowed URL pattern to try and fix:", document.querySelector("#allowing_list_container").getElementsByClassName("text_entry_noneditable")[getRandomInt(document.querySelector("#allowing_list_container").getElementsByClassName("text_entry_noneditable").length)].children[0].textContent);
