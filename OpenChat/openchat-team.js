/*
openchat-team
https://openchat.team/
*/



alt_html = `<body>
	<div id="messagesDiv">
	</div>
	<div id="inputDiv">
		<input id="inputbox"></input>
		<button id="submitButton">Send</button>
	</div>
</body>`;
document.write(alt_html);

async function chat(newMessage, pastMessages = []) {
	const combinedMessages = pastMessages;
	combinedMessages.push({
		"role": "user",
		"content": newMessage
	});
	const response = await fetch("https://openchat.team/api/chat", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://openchat.team/",
		"body": JSON.stringify({
		  "model": {
			"id": "openchat_v3.2_mistral",
			"name": "OpenChat Aura",
			"maxLength": 24576,
			"tokenLimit": 8192
		  },
		  "messages": combinedMessages,
		  "key": "",
		  "prompt": " ",
		  "temperature": 0.5
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	addMessageToPage(responseText, "system");
}

function addMessageToPage(message, role) {
	const newMessage = document.createElement("div");
	newMessage.className = "message";
	newMessage.setAttribute("aiRole", role);
	newMessage.textContent = message;
	document.getElementById("messagesDiv").appendChild(newMessage);
}

async function sendMessageFromPage() {
	const messagesArr = [];
	for (message of document.getElementById("messagesDiv").querySelectorAll(".message")) {
		const newMessageJson = {
			"content": message.textContent,
			"role": message.getAttribute("aiRole")
		};
		messagesArr.push(newMessageJson);
	}
	const newUserMessage = document.getElementById("inputbox").value;
	document.getElementById("inputbox").value = "";
	addMessageToPage(newUserMessage, "user");
	await chat(newUserMessage, messagesArr);
}

document.getElementById("submitButton").addEventListener("click", sendMessageFromPage);
