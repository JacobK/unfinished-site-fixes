/*
wifi-boingohotspot-net
https://wifi.boingohotspot.net/*
e.g.
https://wifi.boingohotspot.net/msy.ta/
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This script worked for me in the New Orleans airport

function getMtModel() {
	for (script of document.scripts){
		const match = script.textContent.match(/var mtModel = ({[^;]*})/);
		if (match) {
			return JSON.parse(match[1]);
		}
	}
}

async function bipRequest(loginModuleId) {
	const response = await fetch("https://wifi.boingohotspot.net/api/wifi/credentials/bip", {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json;charset=utf-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin",
		    "Priority": "u=0"
		},
		"body": JSON.stringify({
		  "bipModuleId": loginModuleId,
		  "bip-module-id": loginModuleId,
		  "pageTitle": document.title,
		  "isC9AdMissing": true // not sure what this means
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function loginRequest(username, password, link, gsid) {
	const newForm = document.createElement("form");
	// TODO: figure out if "buttonClicked" should ever be something other than 4
	for (inputPair of [["username", username], ["user", username], ["password", password], ["cmd", "authenticate"], ["buttonClicked", 4], ["gsid", gsid]]) {
		const newInput = document.createElement("input");
		newInput.type = "text";
		newInput.name = inputPair[0];
		newInput.value = inputPair[1];
		newForm.appendChild(newInput);
	}
	newForm.action = link;
	newForm.method = "POST";
	document.body.appendChild(newForm);
	newForm.submit();
}

for (elem of document.querySelectorAll("a.one-click-small-button.one-click-module")) {
	const moduleId = elem.getAttribute("login-module-id");
	elem.addEventListener("click", function() {
		bipRequest(moduleId).then(function(json){
			loginRequest(json.username, json.password, json.link_login_only, json.gsid);
		});
	})
}

