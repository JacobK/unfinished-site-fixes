/*
evb-gg-n
https://evb.gg/n
e.g. https://evb.gg/n#cbbbbb3ltcn
*/

// TODO: find examples where things like notificationType and expiredTimestamp (in the json) would matter.

// get json about a notification, based on an id passed as a string
async function getInfo(id) {
	const response = await fetch(`https://evb.gg/v1/api/sms/notification/info/${id}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:107.0) Gecko/20100101 Firefox/107.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
//		"referrer": "https://evb.gg/n",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

if (document.URL.split("#")[1]) {
	getInfo(document.URL.split("#")[1]).then(function(json) {
		document.getElementById("messageTitle").innerText = json.title;
		document.getElementById("messageTitle").classList.remove("hidden");
		document.getElementById("messageContent").innerText = json.body;
		if (Date.now() > json.expiredTimestamp) {
			alert(`This notification expired on ${new Date(json.expiredTimestamp)}`);
		}
	});
} else {
	alert("This URL is incomplete. The part after the '#' is important!");
}
