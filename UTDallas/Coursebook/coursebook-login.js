/*
coursebook-login
https://coursebook.utdallas.edu/login/coursebook
https://wat.utdallas.edu/login/eval
https://wat.utdallas.edu/login
*/

// Set visible input fields to have a name so they get submitted.
document.getElementById("netid").name = "netid";
document.getElementById("password").name = "password";

// Remove hidden input fields, which already had names.
document.querySelectorAll("[name=netid]")[1].remove();
document.querySelectorAll("[name=password]")[1].remove();

// Make the button submit without also sending image coordinates.
document.getElementById("login-button").type = "submit";

// Hide "Submit Query" text ontop of button.
document.getElementById("login-button").value = "";
