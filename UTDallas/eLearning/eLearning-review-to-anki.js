/*
elearning-review-to-anki
https://elearning.utdallas.edu/webapps/assessment/review/review.jsp
*/

// This is a utility script, not a fix

/* This only works if only a single correct answer is shown in all cases! Otherwise
	the output will be incorrect, so double check it! Also, make sure there are
	no semicolons on the page. */

bigString = "";
for (detailsDiv of document.querySelectorAll("div.details")) {
	bigString += detailsDiv.querySelector("div.vtbegenerated.inlineVtbegenerated").innerHTML;
	bigString += '\t';
	bigString += detailsDiv.querySelector(".answerTextSpan").innerText;
	bigString += '\n';
}
console.info(bigString);

/* Also, this script does not get images. */

// TODO: Add images support (idk how)
// TODO: Add support for questions with many correct answers
// TODO: Add support for reviews that show incorrect answers also
