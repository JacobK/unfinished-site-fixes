/*
	identifier: utd-eval
	long name: Fix for UTD Evaluations
	description: allow submitting UTD end-of-semester evaluations
	URL pattern: https://eval.utdallas.edu/evaluation/*
*/

// TODO: write a separate script to actually get to this page

// WARNING: UTDallas will be able to tell that you are using this script.
// WARNING: You need to save each text field individually before submitting!

let debugResponse = null; // for debugging, of course


// send data about a button or text field
function sendData(element) {
	let action = element.getAttribute("data-action");	
	let dataData = "";
	let checked = "false";
	let val = "";
	if (action == "answer") {
		checked = "true";
		dataData = element.getAttribute("data-data");
		val = "on";
	} else if (action == "savetext") {
		checked = "false";
		// TODO: figure out proper encoding (spaces should be "+", not %20", but the latter seems to work okay)
		dataData = encodeURIComponent(element.value); // This seems to be what's actually saved.
		val = encodeURIComponent(element.value);
	} else if (action == "submitevel") {
		// TODO: *syncronously* save all of the text fields before actually submitting
	} // other possible actions are "unsubmiteval" and "clear" (The "clear" option seems to be hidden even in the proprietary app, so I've not implemented it.)
	
	fetch("https://eval.utdallas.edu/ues5/clips/clip-ues.zog", {
		"credentials": "include",
		"headers": {
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": `id=${element.getAttribute("data-id")}&action=${action}&data=${dataData}&dbaction=&target=&val=${val}&checked=${checked}`,
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		const responseJson = await response.json(); // If you submit garbage data, the response will be just "hello" (not json). Or, if you don't encode properly, you may get a response like "Y29uc29sZS5sb2coJ3VwZGF0aW5nIHVlcyByZXNwb25zZScpOw==". I don't know what that one means. You may also get that message if the text didn't actually change.
		debugResponse = responseJson;
		if (responseJson.success) { // This can be "response saved" or "response updated"
			if (responseJson.sethtml) {
				Object.keys(responseJson.sethtml).forEach(key => console.log(responseJson.sethtml[key]));
				// TODO: update html (probably not by setting innerHTML, that seems dangerous)
			}
			console.log(responseJson.success);
			// TODO: show this success in a popup
		}
		if (responseJson.redirect) {
			window.location.href = responseJson.redirect;
		}
	});
}

// add a hook to every parent of a pbutton // TODO: fix duplicating requests
[...document.getElementsByClassName("pbutton")].forEach(function(element){
	let clickable = element.parentElement;
	if (element.getAttribute("data-action") == "submiteval" || element.getAttribute("data-action") == "unsubmiteval") {
		clickable = element;
	}
	clickable.addEventListener("click", function(parent) {
		console.log(parent); // debugging
		sendData(element);
	});
});



// add a button below each text box, and then add a hook to that button to save the text
[...document.getElementsByClassName("uescomment")].forEach(function(element){
	let saveButton = document.createElement("h3");
	saveButton.innerText = "Click to save text";
	element.insertAdjacentElement("afterend", saveButton);
	saveButton.addEventListener("click", function(){
		sendData(element);
	});
});

// show warning about the requirement to manually save (TODO: remove this once submitting saves all fields first)
if (document.getElementsByClassName("ues-save-block")[0]) {
	let saveWarning = document.createElement("h2");
	saveWarning.innerText = "WARNING: You must save each text field manually before submitting!";
	document.getElementsByClassName("ues-save-block")[0].insertAdjacentElement("beforebegin", saveWarning);
}
