/*
	identifier: utd-eval-home.js
	URL pattern: https://eval.utdallas.edu/
*/
console.warn("WARNING: possible backdoor due to direct innerHTML set");

fetch("https://eval.utdallas.edu/ues5/clips/clip-ues.zog?action=openevalspanel", {
    "credentials": "include",
    "headers": {
        "Accept": "text/html, */*; q=0.01",
        "Accept-Language": "en-US,en;q=0.5",
        "X-Requested-With": "XMLHttpRequest",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin"
    },
    "method": "GET",
    "mode": "cors"
}).then(async function(response) {
	responseText = await response.text();
	document.getElementById("open-evals-panel").innerHTML = responseText;
});

/* This is different from how the official script does it, as the official
	scripts uses <svg> instead of <img> */
for (script of document.scripts) {
	const match = script.textContent.match(/\$\('#ueschart'\)\.load\('([^']*)'\);/);
	if (match)
		document.getElementById('ueschart').innerHTML = `<img src='${match[1]}'>`;
}

