/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright (C) 2022-2024 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
duo-auth
https://api-a4b09f6a.duosecurity.com/frame/web/v1/auth
https://api-a4b09f6a.duosecurity.com/frame/prompt
https://api-a4b09f6a.duosecurity.com/frame/frameless/v4/auth
*/
/* possible privacy considerations:
This script sends device information to the server:
	screen size (`window.screen.width` and `window.screen.height`)
	color depth (`window.screen.colorDepth`)
*/

// TODO: Don't break when the user presses enter instead of clicking the log in button

const inIframe = window.location != window.parent.location;

// method to populate the plugin form and submit it, takes the plugin form as its only input
function handlePluginForm(formElement) {
	formElement.querySelector("input[name='screen_resolution_width']").value = window.screen.width;
	formElement.querySelector("input[name='screen_resolution_height']").value = window.screen.height;
	formElement.querySelector("input[name='color_depth']").value = window.screen.colorDepth;
	formElement.querySelector("input[name='is_cef_browser']").value = false; // TODO: understand why they want this
	formElement.querySelector("input[name='is_ipad_os']").value = false; // TODO: understand why they want this
	formElement.querySelector("input[name='is_user_verifying_platform_authenticator_available']").value = false; // TODO: understand why
	formElement.querySelector("input[name='react_support']").value = true; // TODO: understand why
	formElement.submit();
}

// method to submit the endpoint health form, takes such form as its only input
function handleEndpointHealthForm(formElement) {
	formElement.submit();
}

// method to set the login form to visible and attach the event listeners needed to submit it, takes the form as its only input
function handleLoginForm(formElement) {
	formElement.classList.remove("hidden");
//	formElement.querySelector("fieldset.device-selector").classList.remove("hidden"); // TODO: only show this if there is more than 1 options
	// TODO: support having more than 1 device
	// `formElement.querySelector("fieldset.device-selector").querySelector("select").value` gets the currently selected device
	formElement.querySelector("fieldset[data-device-index]").classList.remove("hidden");
	// There may be multiple fieldsets if the user has multiple devices registered (not confirmed with testing)
	for (deviceFieldset of formElement.querySelectorAll("fieldset[data-device-index]")) {
		deviceFieldset.querySelector("button#passcode").addEventListener("click", passcodeButtonClick);
	}
}

// event method to handle a click on the passcode button
function passcodeButtonClick(event) {
	event.preventDefault(); // Don't submit yet
	if (event.target.innerText == "Enter a Passcode") {
		event.target.parentElement.querySelector("span").classList.add("hidden");
		event.target.parentElement.querySelector("input.passcode-input").classList.remove("hidden");
		event.target.innerText = "Log In";
		showSMSSendButton(event.target);
		event.preventDefault(); // Don't submit yet
	} else if (event.target.parentElement.querySelector("input.passcode-input").value) { // if there is a value in the passcode input
		submitAuth(event.target);
		event.preventDefault();
	} else {
		event.preventDefault();
	}
}

function showSMSSendButton(buttonElement) {
	const formElement = buttonElement.parentElement.parentElement.parentElement.parentElement;
	const rowElement = buttonElement.parentElement;
	const xsrfToken = formElement.querySelector("input[name='_xsrf']").value;
	const sid = formElement.querySelector("input[name='sid']").value;
	const device = formElement.querySelector("input.passcode-input").getAttribute("data-index");
	const factor = rowElement.querySelector("input[name='factor']").value;
	const sendButton = document.createElement("button");
	sendButton.style.backgroundColor = "blue";
	sendButton.innerText = "Send new SMS";
	sendButton.onclick = function(ev) {
		ev.target.remove();
		sendNewCodes(xsrfToken, sid, device, "sms"); // TODO: make this work on the frameless page too
	};
	document.querySelector("div.messages-list").appendChild(sendButton);
	document.querySelector("div#messages-view").classList.remove("hidden");
}

// method to submit the oidc form with a given txid
function oidcFormSubmit(txid) {
	const oidcForm = document.querySelector("form.oidc-exit-form");
	oidcForm.querySelector("input[name='txid']").value = txid;
	oidcForm.submit();
}


// method to submit a passcode (which requires a series of requests), with the button clicked as the input
// TODO: refactor this function so it's not such a mess
async function submitAuth(buttonElement) {
	const formElement = buttonElement.parentElement.parentElement.parentElement.parentElement;
	const rowElement = buttonElement.parentElement;
	const xsrfToken = formElement.querySelector("input[name='_xsrf']").value;
	const sid = formElement.querySelector("input[name='sid']").value;
	const device = formElement.querySelector("input.passcode-input").getAttribute("data-index");
	const factor = rowElement.querySelector("input[name='factor']").value;
	const passcode = rowElement.querySelector("input.passcode-input[name='passcode']").value;
	const framePromptJson = await framePrompt(xsrfToken, sid, device, factor, passcode);
	if (framePromptJson.stat == "OK") {
		const txid = framePromptJson.response.txid;
		const frameStatusJson = await frameStatus(xsrfToken, sid, txid);
		if (frameStatusJson.stat == "OK" && frameStatusJson.response.result == "SUCCESS") {
			const resultUrl = frameStatusJson.response.result_url;
			// resultUrl may be undefined if frameStatusJson.response.post_auth_action exists e.g. as "oidc_exit"
			if (frameStatusJson.response.post_auth_action == "oidc_exit") {
				oidcFormSubmit(txid);
				return;
			}
			const frameStatusHashJson = await frameStatusHash(xsrfToken, resultUrl, sid);
			if (frameStatusHashJson.stat == "OK") {
				console.log(`submitting back to ${frameStatusHashJson.response.parent}`);
				const parentUrl = frameStatusHashJson.response.parent;
				const responseCookie = frameStatusHashJson.response.cookie;
				if (inIframe) {
					window.parent.postMessage(`${responseCookie}`, `${parentUrl.split("/")[0]}/${parentUrl.split("/")[1]}/${parentUrl.split("/")[2]}`); // send signature to the parent so it can submit the form
				} else {
					console.warn("You are not in an iframe! Doing nothing. Here's the last JSON response though:", frameStatusHashJson);
				}
			} else {
				console.error(`error in ${resultUrl} endpoint`, frameStatusHashJson);
			}
		} else {	// TODO: explicitly handle invalid passcode errors
			console.error("error in /frame/status endpoint", frameStatusJson);
		}
	} else {
		console.error("error in /frame/prompt endpoint", framePromptJson);
	}
}

// method to submit a passcode, with the values used in the fetch request as the input
async function framePrompt(xsrfToken, sid, device, factor, passcode) {
	const response = await fetch("https://api-a4b09f6a.duosecurity.com/frame/prompt", {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "text/plain, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
			"X-Xsrftoken": xsrfToken,
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://api-a4b09f6a.duosecurity.com/frame/prompt?sid=${encodeURIComponent(sid)}`,
		// body contruction way from here: https://stackoverflow.com/a/53189376
		"body":  new URLSearchParams({
			"sid": sid,
			"device": device,
			"factor": factor,
			"passcode": passcode,
			"out_of_date": "False", // TODO: figure out what this means
			"days_out_of_date": 0, // TODO: figure out what this means
			"days_to_block": "None" // TODO: figure out what this means
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function frameStatus(xsrfToken, sid, txid) {
	const response = await fetch("https://api-a4b09f6a.duosecurity.com/frame/status", {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "text/plain, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
			"X-Xsrftoken": xsrfToken,
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://api-a4b09f6a.duosecurity.com/frame/prompt?sid=${encodeURIComponent(sid)}`,
		"body": new URLSearchParams({
			"sid": sid,
			"txid": txid
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function frameStatusHash(xsrfToken, resultUrl, sid) {
	const response = await fetch(resultUrl, {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "text/plain, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
			"X-Xsrftoken": xsrfToken,
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://api-a4b09f6a.duosecurity.com/frame/prompt?sid=${encodeURIComponent(sid)}`,
		"body": new URLSearchParams({
			"sid": sid,
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// send an SMS containing extra codes
async function sendNewCodes(xsrfToken, sid, device, factor) {
	const response = await fetch("https://api-a4b09f6a.duosecurity.com/frame/prompt", {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:107.0) Gecko/20100101 Firefox/107.0",
			"Accept": "text/plain, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
			"X-Xsrftoken": xsrfToken,
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://api-a4b09f6a.duosecurity.com/frame/prompt?sid=${sid}`,
		"body": new URLSearchParams({
			"sid": sid,
			"device": device,
			"factor": factor
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// This script is used on 2 (maybe 3?) different web pages with the same exact URL. The first page contains a single form (with id "plugin_form") linking to itself; populating and submitting the form will take you to the second page. The second page contians a 2 forms: 1 with id "login-form" that is not used at this time, and 1 with id "endpoint-health-form", which is sent as soon as the page is loaded. The 3rd page contains only a form with id "login-form", which is the actual login form. The 3rd page is actually a different URL, but I didn't realize that until after I wrote them as 1 script (So, I don't think I really needed to explicitly set the referrers.).
// TODO: consider splitting this script into 2 different scripts for the 2 different URLs

const pluginForm = document.querySelector("form#plugin_form");
const endpointHealthForm = document.querySelector("form#endpoint-health-form");
const loginForm = document.querySelector("form#login-form");
if (pluginForm) { // TODO: add detection here
	handlePluginForm(pluginForm);
} else if (endpointHealthForm) {
	handleEndpointHealthForm(endpointHealthForm);
} else if (loginForm) {
	handleLoginForm(loginForm);
}
