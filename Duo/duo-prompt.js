/*
duo-prompt
https://api-a4b09f6a.duosecurity.com/frame/v4/auth/prompt
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

// Some things that are hardcoded: "SMS+Passcode", and "OIDC_EXIT"

// Some of this code at the beginning is common to multiple Duo pages, so it should be moved to a duo-common script when publishing

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

function getBaseData() { // TODO: fix with JSON5 prob.
	return JSON.parse(document.getElementById("base-data").textContent);
}

// This code is weird because we'll be erasing the page so we need to store the xsrf in a variable; I should probably change this to a simple variable or a full class
let xsrf = null;
function getXsrf() {
	if (xsrf) {
		return xsrf;
	} else {
		xsrf = document.getElementById("base-data").textContent.match(/"xsrf_token": "([^"]*)"/)[1];
		return xsrf;
	}
}
getXsrf();



function getSid() {
	return new URL(document.location).searchParams.get("sid");
}

function getBrowserFeatures() {
	return {
	  "touch_supported": false,
	  "platform_authenticator_status": "unavailable",
	  "webauthn_supported": true
	}
}

let dataJson = null;
async function getDataJson() {
	const response = await fetch(`https://api-a4b09f6a.duosecurity.com/frame/v4/auth/prompt/data?post_auth_action=OIDC_EXIT&sid=${getSid()}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:129.0) Gecko/20100101 Firefox/129.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Xsrftoken": getXsrf(),
		    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin",
		    "Priority": "u=4"
		},
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	dataJson = responseJson.response;
	return responseJson;
}

// device is e.g. "phone1", factor is e.g. "sms", passcode is an integer
async function prompt(device, factor, passcode) {
	if (factor == "Passcode") {
		device = null;
	}
	let extraString = "";
	if (factor == "sms") {
		extraString = "inSmsErrorRetryFlow=undefined&";
	} if (factor == "Passcode") {
		extraString = `passcode=${passcode}&`;
	}
	const response = await fetch("https://api-a4b09f6a.duosecurity.com/frame/v4/prompt", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:129.0) Gecko/20100101 Firefox/129.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Xsrftoken": getXsrf(),
		    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin",
		    "Priority": "u=4"
		},
		"body": `${extraString}device=${device}&factor=${factor}&postAuthDestination=OIDC_EXIT&browser_features=${formUrlEncode(JSON.stringify(getBrowserFeatures()))}&sid=${getSid()}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function status(txid) {
	const response = await fetch("https://api-a4b09f6a.duosecurity.com/frame/v4/status", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:129.0) Gecko/20100101 Firefox/129.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Xsrftoken": getXsrf(),
		    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin",
		    "Priority": "u=4"
		},
		"body": `txid=${txid}&sid=${getSid()}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = response.json();
	return responseJson;
}

async function submitAsForm(action, method, dataArrs) {
	const formElem = document.createElement("form");
	formElem.action = action;
	formElem.method = method;
	for (pair of dataArrs) {
		newInput = document.createElement("input");
		newInput.name = pair[0];
		newInput.value = pair[1];
		formElem.appendChild(newInput);
	}
	document.body.appendChild(formElem);
	formElem.submit();
}

async function exit(txid, deviceKey) {
	submitAsForm("https://api-a4b09f6a.duosecurity.com/frame/v4/oidc/exit", "POST", [
		["sid", getSid()],
		["txid", txid],
		["factor", "SMS Passcode"],
		["device_key", document.getElementById("device-select").querySelector(`[value='${document.getElementById("device-select").value}']`).getAttribute("data-key")],
		["_xsrf", getXsrf()],
		["dampen_choice", "true"]
	]);
}

async function submit() {
	const device = document.getElementById("device-select").value;
	const factor = document.getElementById("factor-select").value;
	const passcode = document.getElementById("passcode-input").value;
	const promptJson = await prompt(device, factor, passcode);
	const statusJson = await status(promptJson.response.txid);
	if (statusJson.response.result == "SUCCESS" && statusJson.response.post_auth_action == "oidc_exit") {
		exit(promptJson.response.txid);
	} else {
		getDataJson();
	}
}

async function buildPage() {
	// document.write doesn't work because of "A call to document.write() from an asynchronously-loaded external script was ignored." warning
	document.querySelector('body').innerHTML = `
		<body>
			device: <select id="device-select" name="device">
			</select><br>
			factor: <select id="factor-select" name="factor">
			</select><br>
			passcode (if applicable): <input id="passcode-input" name="passcode" /><br>
			submit: <button id="submit-button">submit</button>
		</body>`;
	const dataJson = await getDataJson();
	const deviceSelect = document.getElementById("device-select");
	const factorSelect = document.getElementById("factor-select");
	const submitButton = document.getElementById("submit-button");
	for (phone of dataJson.response.phones) {
		const newOpt = document.createElement("option");
		newOpt.value = phone.index;
		newOpt.label = phone.name;
		newOpt.setAttribute("data-key", phone.key);
		deviceSelect.appendChild(newOpt);
	}
	for (factor of ["sms", "Passcode"]) {
		const newOpt = document.createElement("option");
		newOpt.value = factor;
		newOpt.label = factor;
		factorSelect.appendChild(newOpt);
	}
	submitButton.addEventListener("click", submit);
}

async function main() {
	await buildPage();
}
main();
