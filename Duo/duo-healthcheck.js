/*
duo-healthcheck
https://api-a4b09f6a.duosecurity.com/frame/v4/preauth/healthcheck
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

// Warning: this could lead to an infinite loop involving requesting duo-auth and duo-healthcheck pages

function getBaseData() { // TODO: fix with JSON5 prob.
	return JSON.parse(document.getElementById("base-data").textContent);
}

function getXsrf() {
	return document.getElementById("base-data").textContent.match(/"xsrf_token": "[^"]*"/)[1];
}

function getSid() {
	return new URL(document.location).searchParams.get("sid");
}

async function data() {
	const response = await fetch(`https://api-a4b09f6a.duosecurity.com/frame/v4/preauth/healthcheck/data?sid=${getSid()}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:129.0) Gecko/20100101 Firefox/129.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Xsrftoken": getXsrf(),
		    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin",
		    "Priority": "u=4"
		},
		"method": "GET",
		"mode": "cors"
	});
}

async function returnFunc() {
	document.location.href = `https://api-a4b09f6a.duosecurity.com/frame/v4/return?sid=${getSid()}`;
}

async function main() {
	await data();
	await returnFunc();
}

main();
