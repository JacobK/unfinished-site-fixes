/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright (C) 2022,2023 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
duo-sso
https://idp.utdallas.edu/idp/profile/SAML2/Redirect/SSO
*/
/* possible security considerations:
This script loads an iframe with a server-provided URL.
*/

// Sometimes, all we need to do is redirect, if the user is already logged in
if (document.body.getAttribute("blocked-onload") && document.body.getAttribute("blocked-onload").search(/document.forms\[0\].submit\(\)/) != -1) { // uses search instead of exact match so LibreJS doesn't get in the way
	document.forms[0].submit();
} // TODO: make it so the below code does not run when the above condition is true

const iFrameElement = document.querySelector("iframe");
iFrameElement.src = `https://${iFrameElement.getAttribute("data-host")}/frame/web/v1/auth?tx=${iFrameElement.getAttribute("data-sig-request").split(":")[0]}&parent=${encodeURIComponent(document.location.href)}&v=2.6`;
// Part of the URL is in `["https://",s,"/frame/web/v1/auth?tx=",f,"&parent=",encodeURIComponent(document.location.href),"&v=2.6"]` in https://idp.utdallas.edu/idp/js/Duo-Web-v2.min.js . Perhaps it would be better to get the "/frame/web/v1/auth?tx=" part from the script, but I will hard-code it for now. Also, I saw that encodeURIComponent was used in the original script.

// The page contains a form with id "duo_form", but I'm not sure what the purpose of it is. Scratch that, it's what's submitted after the iframe is done.
const formElement = document.querySelector("form#duo_form");

window.addEventListener("message", (event) => {
	if (event.origin !== "https://api-a4b09f6a.duosecurity.com") {
		console.error("Bad origin:", event.origin);
		console.error("message missed:", event.data);
		return;
	}
	
	if (event.data.split("|")[0] == "AUTH") {
		setSigAuth(event.data);
	} else {
		console.error("unknown message:", event.data);
	}
	
}, false);

// method to set the sig_response value
function setSigAuth(sigAuth) {
	const newInput = document.createElement("input");
	newInput.type = "hidden";
	newInput.name = "sig_response";
	formElement.appendChild(newInput);
	newInput.value = `${sigAuth}:${iFrameElement.getAttribute("data-sig-request").split(":")[1]}`;
	formElement.submit();
}
