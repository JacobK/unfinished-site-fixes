/*
group-com-background-check
https://services.group.com/intervarsity/form/
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

dateFields = document.querySelectorAll("input.mydatepicker");
for (inputElem of dateFields) {
	inputElem.type = "date";
	inputElem.removeAttribute("max");
	inputElem.removeAttribute("readonly"); // TODO: How should I know if the field is actually supposed to be readonly?
}

