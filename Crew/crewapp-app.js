/*
crewapp-app
https://crewapp.com/app
*/

// SPDX-FileCopyrightText: 2022-2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Note: the official app changes the URL a lot, but seemingly without refresh.

const timeZoneName = "America/Chicago";

function setCookie(cookieName, cookieValue) {
	document.cookie = `${cookieName}=${cookieValue}`;
}

/* Replace current page contents. */
const replacement_html = `\
<!DOCTYPE html>
<html>
<body>
phone number: <input type="text" id="countryCodeInput" placeholder="country code" style="width:6em"> <input type="text" id="phoneNumberInput"> <button id="sendCodeButton">send code</button><br>
<input type="text" id="loginSessionIdInput" style="display:none;"> code: <input type="text" id="codeInput"><button id="loginButton">log in</button><br>
<button id="checkLogin">check login</button>
<button id="showMessages">show recent messages</button>
<div id="messageBox">

</div>
</body>
</html>
`;

// a few lines of code from app-box-com-fix (C) Wojtek Kosior 2022
/*
 * We could instead use document.write(), but browser's debugging tools would
 * not see the new page contents.
 */
const parser = new DOMParser();
const alt_doc = parser.parseFromString(replacement_html, "text/html");
document.documentElement.replaceWith(alt_doc.documentElement);


document.getElementById("sendCodeButton").addEventListener("click", async function() {
	// TODO: check formatting of values first
	const verificationResponseJson = await login(document.getElementById("countryCodeInput").value, document.getElementById("phoneNumberInput").value);
	document.getElementById("loginSessionIdInput").value = verificationResponseJson.loginSessionId;
});

document.getElementById("loginButton").addEventListener("click", async function() {
	const loginResponseJson = await verificationPin(document.getElementById("countryCodeInput").value, document.getElementById("phoneNumberInput").value, document.getElementById("loginSessionIdInput").value, document.getElementById("codeInput").value);
	setCookie("cutk",loginResponseJson.credentials.accessToken);
	setCookie("cuid", loginResponseJson.credentials.id);
});

function printMessage(message, messageBoxElem) {
	const messageString = `[${new Date(message.createdAt)}] <${message.from.profile.fullName}> ${message.text}`;
	const newParagraph = document.createElement("p");
	newParagraph.innerText = messageString;
	console.info(messageString);
	messageBoxElem.appendChild(newParagraph);
}

document.getElementById("showMessages").addEventListener("click", async function() {
	const messageBoxElem = document.getElementById("messageBox");
	messageBoxElem.innerHtml = ""; // clear any existing visible messages
	const organizationsJson = await organizations(getCookie("cuid"));
	if (organizationsJson.length > 1) {
		alert("You are in more than one organization, but only showing one organization is supported.");
	}
	const everyoneMessages = await messages(organizationsJson[0].id, organizationsJson[0].everyoneConversationId.id);
	console.info("Showing everyone messages...");
	for (message of everyoneMessages) {
		printMessage(message, messageBoxElem);
	}
	const announcementMessages = await messages(organizationsJson[0].id, organizationsJson[0].announcementsConversationId.id);
	console.info("Showing announcement messages...");
	for (message of announcementMessages) {
		printMessage(message, messageBoxElem);
	}
});

// userId is lowercase hexadecimal
async function users(userId) {
	const currentTimestamp = Date.now();
	const response = await fetch(`https://crewapp.com/api/users/${userId}?_=${currentTimestamp}`, {
		"credentials": "include",
		"headers": {
//		    ":authority": "crewapp.com", // error: `Uncaught (in promise) TypeError: Window.fetch: :authority is an invalid header name.`
//		    ":method": "GET", // error: `Uncaught (in promise) TypeError: Window.fetch: :method is an invalid header name.`
//		    ":path": `/api/users/${userId}?_=${currentTimestamp}`, // error: `Uncaught (in promise) TypeError: Window.fetch: :path is an invalid header name.`
//		    ":scheme": "https", // error: `Uncaught (in promise) TypeError: Window.fetch: :scheme is an invalid header name.`
		    "accept": "*/*",
		    "accept-language": "en-US,en;q=0.9",
		    "content-type": "application/json",
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "crew-call-id": genCallId(),
		    "crew-device-id": getDeviceId(),
//		    "sec-ch-ua": "\"Microsoft Edge\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
		    "sec-ch-ua-mobile": "?0",
//		    "sec-ch-ua-platform": "\"Windows\"",
		    "sec-fetch-dest": "empty",
		    "sec-fetch-mode": "cors",
		    "sec-fetch-site": "same-origin",
//		    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.50",
		    "x-requested-with": "XMLHttpRequest"
		},
		"referrer": "https://crewapp.com/app",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

document.getElementById("checkLogin").addEventListener("click", async function() {
	if (!getCookie("cuid")) {
		alert("You are not logged in.");
		return false;
	}
	const usersJson = await users(getCookie("cuid"));
	if (usersJson.id == getCookie("cuid")) {
		alert(`You are logged in as ${usersJson.profile.fullName}.`);
		return true;
	}
});


// From https://stackoverflow.com/a/15724300 , by kirlich (https://stackoverflow.com/users/7635/kirlich) and Arseniy-II (https://stackoverflow.com/users/8163773/arseniy-ii), licensed CC BY-SA 4.0
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function getDeviceId() {
	return getCookie("did");
}

function genCallId() {
	return crypto.randomUUID();
}

function getUserId() {
	return getCookie("cuid");
}

// This function gets a list of countries and country codes for phone numbers.
async function getCountries() {
	const response = await fetch(`https://crewapp.com/api//config/countries/allowed?_=${Date.now()}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:107.0) Gecko/20100101 Firefox/107.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": crypto.randomUUID(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/login",
		"method": "GET",
		"mode": "cors"
	});
	responseJson = await response.json();
	return responseJson;
}
/*
expected reply is an array of JSON objects contianing a dialingCode (i.e.
	country code), country (i.e. fancy name), and iso2Code (i.e. short code)
*/


/*
countryCode is a string containing the + number in the phone number (e.g. "1" for +1)
phoneNumber is a string containing the phone number without the + value (like "##########")
This function sends a code over SMS to the phone number specified.
The official client sends this request as soon as the phone number is typed,
	before the user clicks the button to send it. We should not do that.
*/
async function login(countryCode, phoneNumber) {
	const response = await fetch("https://crewapp.com/api/login", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:107.0) Gecko/20100101 Firefox/107.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": crypto.randomUUID(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/login",
		"body": JSON.stringify({
			"countryCode": countryCode,
			"phone": phoneNumber,
			"deviceId": getDeviceId()
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
// expected reply is JSON with a single value, loginSessionId, containing a UUID

/*
first 2 arguments same as login()
loginSessionId is the UUID received from login()
This function sends a phone call to the phone number specified. The official
	client automatically sent this request 5 minutes after typing in the phone
	number, and it seems like one could have chosen to send it on command.
	Not sending this request 5 minutes after may, in combination with other
	information, reveal that the user is using an alternate client.
*/
async function sendPhoneCall(countryCode, phoneNumber, loginSessionId) {
	await fetch(`https://crewapp.com/api/login/${loginSessionId}/notification`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:107.0) Gecko/20100101 Firefox/107.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": crypto.randomUUID(),
		    "crew-api-version": "3",
		    "crew-app-state": "BACKGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/login/pin",
		"body": JSON.stringify({
			"countryCode": countryCode,
			"phone": phoneNumber,
			"deviceId": getDeviceId(),
			"accessCodeNotificationType": "VOICE"
		}),
		"method": "POST",
		"mode": "cors"
	});
}
// expected reply is empty JSON (i.e. {})


/*
first 3 arguments same as sendPhoneCall()
accessCode is the access code from the SMS text, in the form "######"
This method proves to the server that you can read the text messages from the
	phone number you gave.
*/
async function verificationPin(countryCode, phoneNumber, loginSessionId, accessCode) {
	const response = await fetch(`https://crewapp.com/api/login/${loginSessionId}/verification`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:107.0) Gecko/20100101 Firefox/107.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": crypto.randomUUID(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/login/pin",
		"body": JSON.stringify({
			"countryCode": countryCode,
			"phone": phoneNumber,
			"deviceId": getDeviceId(),
			"accessCode": accessCode,
			"adContentId": null
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/*
Should return a bunch of JSON data, importantly responseJson.user.id which is
	used in lots of later methods as userId, and responseJson.credentials.id
	which is assigneeId.
possible error response: {"code":"invalidLoginSession","message":"The passed login session is not valid","type":400}
	may happen if you take too long to enter the code, as I did when developing this script
*/


// not sure if this is important
async function devices(userId) {
	await fetch(`https://crewapp.com/api/users/${userId}/devices`, {
		"credentials": "include",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": crypto.randomUUID(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/login/pin",
		"body": JSON.stringify({
			"deviceId": getDeviceId(),
			"timeZoneName": "UTC",
			"deviceType": "WEB",
			"app": "CREW",
			"agentClass": 1,
			"userAgent": "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0"
			/* Mote that userAgent does not match the user agent in the header.
				I suspect it is what the user agent would be without
				anti-fingerpringing enabled. */
		}),
		"method": "POST",
		"mode": "cors"
	});
	// This fetch does not return a body; it may just be unnecessary telemetry.
}


async function getStream() {
	const response = await fetch("https://crewapp.com/api/entity-events/stream?format=JSON_ARRAY", {
		"credentials": "include",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "crew-call-id": crypto.randomUUID(),
		    "crew-device-id": getDeviceId(),
		    "crew-agent-class": "web",
		    "crew-app-state": "FOREGROUND",
		    "crew-api-version": "3",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/newuser",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* response is an array of a bunch of entity objects, importantly an object
	representing the organization the user is in, which contains an ID
	necessary for later. In my case, this ID would be at
	responseJson[34].entity.id */


// fullName is a string containing a full name, like "Firstname Lastname".
async function sendName(fullName, uerId) {
	const response = await fetch(`https://crewapp.com/api/users/${userId}`, {
		"credentials": "include",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": genCallId(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/newuser",
		"body": JSON.stringify({
			"profile": {
				"fullName": fullName
			},
			"profileCreated": true
		}),
		"method": "PATCH",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
// returns: a bunch of stuff

// title is a job title e.g. "tutor".
async function sendJobTitle(title, userIdm, orgId) {
	const response = await fetch(`https://crewapp.com/api/organizations/${orgId}/memberships/users/${userId}/metadata`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-call-id": genCallId(),
		    "crew-device-id": getDeviceId(),
		    "crew-agent-class": "web",
		    "crew-app-state": "FOREGROUND",
		    "crew-api-version": "3",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app/newuser",
		"body": JSON.stringify({
			"profile": {
				"title": title
			}
		}),
		"method": "PATCH",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}


async function organizations(userId) {
	const response = await fetch(`https://crewapp.com/api/users/${userId}/organizations`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "crew-call-id": genCallId(),
		    "crew-device-id": getDeviceId(),
		    "crew-agent-class": "web",
		    "crew-app-state": "FOREGROUND",
		    "crew-api-version": "3",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
// returns an array of organizations

async function memberships(orgId) {
	const response = await fetch(`https://crewapp.com/api/organizations/${orgId}/memberships`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "crew-call-id": genCallId(),
		    "crew-device-id": getDeviceId(),
		    "crew-agent-class": "web",
		    "crew-app-state": "FOREGROUND",
		    "crew-api-version": "3",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function teamResources(orgId) {
	const response = await fetch(`https://crewapp.com/api/organizations/${orgId}/team-resources`, {
		"credentials": "include",
		"headers": {
		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "crew-call-id": genCallId(),
		    "crew-device-id": getDeviceId(),
		    "crew-agent-class": "web",
		    "crew-app-state": "FOREGROUND",
		    "crew-api-version": "3",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": "https://crewapp.com/app",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
// returns an empty array for me


// add a new shift
async function addShift(bodyJson, orgId, userId, startTime, endTime) {
	const response = await fetch("https://crewapp.com/api/calendar-items", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/plain, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-call-id": genCallId(),
		    "crew-device-id": getDeviceId(),
		    "crew-agent-class": "web",
		    "crew-app-state": "FOREGROUND",
		    "crew-api-version": "3",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://crewapp.com/app/o/${orgId}/calendar`,
		"body": JSON.stringify(bodyJson),
/*
		"body": JSON.stringify({
			"organizationId": orgId,
			"dtStart": {
				"type": "DATE_TIME",
				"value": "2023-01-03T22:00:00.000Z"
			},
			"dtEnd": {
				"type": "DATE_TIME",
				"value": "2023-01-04T01:00:00.000Z"
			},
			"timeZoneName": "America/Chicago",
			"status": "ACTIVE",
			"type": "MULTI_ASSIGNEE_SHIFT",
			"muteAssignmentNotifications": false,
			"members": [
				{
					"type": "USER",
					"userId": userId
				}
			],
			"name": ""
		}),
*/
		"method": "POST",
		"mode": "cors"
	});

	const responseJson = await response.json();
	return responseJson;
	/*
	example fetch body for adding a shift from 4-7 on Tuesday that repeats every
	Tuesday, Thursday, and Saturday:
			"body": "{\"organizationId\":\"redacted\",\"dtStart\":{\"type\":\"DATE_TIME\",\"value\":\"2023-01-17T22:00:00.000Z\"},\"dtEnd\":{\"type\":\"DATE_TIME\",\"value\":\"2023-01-18T01:00:00.000Z\"},\"timeZoneName\":\"America/Chicago\",\"status\":\"ACTIVE\",\"type\":\"MULTI_ASSIGNEE_SHIFT\",\"muteAssignmentNotifications\":false,\"members\":[{\"type\":\"USER\",\"userId\":\"redacted2\"}],\"name\":\"Tuesday, Thursday, Saturday\",\"recurrenceSchedule\":{\"frequency\":\"WEEKLY\",\"daysOfWeek\":[\"TUESDAY\",\"THURSDAY\",\"SATURDAY\"]}}",
	*/
}
// returns JSON with an id that can be used to delete the shift later

async function deleteShift(shiftId, orgId) {
	const response = await fetch(`https://crewapp.com/api/calendar-items/${shiftId}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": genCallId(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://crewapp.com/app/o/${orgId}/schedule/requests/${shiftId}`,
		"method": "DELETE",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

/* Times are numbers containing miliseconds since epoch. The times are midnight
	local time
	on the first day of time off to midnight local time of the last day of time
	off (e.g. `new Date("2024-03-30").getTime()`). That
	means the times should be the same if only a single day off is requested.
	Strangely, the time off will default to 6am - 6pm when editing, and if the
	edit is saved it will send the more precise times.
	notes is a string containing notes about the time off request. Make sure you
	have the orgId right. Also, assigneeId is usually just going to be the
	user's own ID.
*/
async function requestTimeOff(orgId, assigneeId, startTime, endTime, notes) {
	const response = await fetch("https://crewapp.com/api/calendar-items", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-agent-class": "web",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": genCallId(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://crewapp.com/app/o/${orgId}/calendar`,
		"referrerPolicy": "strict-origin-when-cross-origin",
		"body": JSON.stringify({
			"organizationId": orgId,
			"assigneeId": assigneeId,
			"start": startTime,
			"end": endTime,
			"timeZoneName": timeZoneName,
			"status": "OPEN", // not sure if this is supposed to be "ASSIGNED" or "OPEN"
			"type": "TIME_OFF",
			"notes": notes,
			"muteAssignmentNotifications": false
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// 
async function requestMyTimeOff(startDateString, endDateString, notes) {
	const orgs = await organizations(getUserId());
	if (orgs.length != 1) {
		alert("err: you are in more than one or less than one org");
		return;
	}
	const orgId = orgs[0].id;
	return await requestTimeOff(orgId, getUserId(), new Date(startDateString).getTime(), new Date(endDateString).getTime(), notes);
}

/* orgId is the organization ID. The calendar shows a week at a time; startTime
	is the time, in miliseconds since epoch, of midnight on a monday, e.g.
	1673848800000 for Mon 16 Jan 2023 12:00:00.000 AM CST, and endTime is the
	time, in miliseconds since epoch, of the milisecond before midnight next
	monday, e.g. 1674453599999 for Sun 22 Jan 2023 11:59:59.999 PM CST.
	Together, those example times make up the week of Jan 16 - Jan 22.
	currentTime is the current time down to the milisecond, e.g. 1673987202876
	for Tue 17 Jan 2023 02:26:42.876 PM CST. Currently, this function does not
	derive, round, or add randomness to the current time. It sends the exact
	time given as input.   */
async function getCalendar(orgId, startTime, endTime, currentTime) {
	const response = await fetch(`https://crewapp.com/api/organizations/${orgId}/all-calendar-data?startsAtOrAfter=${startTime}&startsAtOrBefore=${endTime}&_=${currentTime}`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "crew-device-id": getDeviceId(),
		    "crew-call-id": genCallId(),
		    "crew-api-version": "3",
		    "crew-app-state": "FOREGROUND",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"referrer": `https://crewapp.com/app/o/${orgId}/calendar`,
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
// 3 arrays are returned.

/* Get messages from a conversation. orgId is the organization ID,
	conversationID is the ID of the conversation, and atOrBefore is an optional
	argument that is miliseconds since epoch that you want the latest message to
	be at or before. */
async function messages(orgId, conversationId, atOrBefore) {
	if (atOrBefore != null) {
		timeString = `atOrBefore=${atOrBefore}&`;
	} else {
		timeString = "";
	}
	const response = await fetch(`https://crewapp.com/api/conversations/${conversationId}/messages?${timeString}limit=25`, { // You can omit the "messages" part of the URL to instead get metadata about the conversation.
	  "headers": {
		"accept": "application/json, text/plain, */*",
		"accept-language": "en-US,en;q=0.9",
		"crew-agent-class": "web",
		"crew-api-version": "3",
		"crew-app-state": "FOREGROUND",
		"crew-call-id": genCallId(),
		"crew-device-id": getDeviceId(),
		"sec-ch-ua": "\"Microsoft Edge\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
		"sec-ch-ua-mobile": "?0",
		"sec-ch-ua-platform": "\"Windows\"",
		"sec-fetch-dest": "empty",
		"sec-fetch-mode": "cors",
		"sec-fetch-site": "same-origin"
	  },
	  "referrer": `https://crewapp.com/app/o/${orgId}/messages/${conversationId}`,
	  "referrerPolicy": "strict-origin-when-cross-origin",
	  "body": null,
	  "method": "GET",
	  "mode": "cors",
	  "credentials": "include"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function acceptInvite(orgId, calendarId, inviteId) {
	fetch(`https://crewapp.com/api/calendar-items/${calendarID}/members/${inviteId}`, {
	  "headers": {
		"accept": "*/*",
		"accept-language": "en-US,en;q=0.9",
		"content-type": "application/json",
		"crew-api-version": "3",
		"crew-app-state": "FOREGROUND",
		"crew-call-id": genCallId(),
		"crew-device-id": getDeviceId(),
		"sec-ch-ua": "\"Microsoft Edge\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
		"sec-ch-ua-mobile": "?0",
		"sec-ch-ua-platform": "\"Windows\"",
		"sec-fetch-dest": "empty",
		"sec-fetch-mode": "cors",
		"sec-fetch-site": "same-origin",
		"x-requested-with": "XMLHttpRequest"
	  },
	  "referrer": `https://crewapp.com/app/o/${orgId}/schedule/requests/${calendarId}`,
	  "referrerPolicy": "strict-origin-when-cross-origin",
	  "body": "{\"status\":\"ACCEPTED\"}",
	  "method": "PATCH",
	  "mode": "cors",
	  "credentials": "include"
	});
}

function main() {
	
}
main();

// 2023-04-16: I suspect the auth error the other day was due to cookies









