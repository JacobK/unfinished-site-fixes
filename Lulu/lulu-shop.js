/*
lulu-shop
e.g. https://www.lulu.com/shop/greg-london/impatient-perl/paperback/product-1wvyk8yj.html?page=1&pageSize=4
*/

// Woah, see <https://graphql.org/>

const appJson = JSON.parse(document.scripts[0].textContent);
const productId = appJson.sku || appJson.mpn;

/* productId is from the appJson, but cartId may be null if you don't have one 
	yet. This function is used in lulu-shop. */
async function addToCart(productId, cartId) {
	const response = await fetch("https://api.lulu.com/graphql/", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "*/*",
		    "Accept-Language": "en",
		    "content-type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://www.lulu.com/",
		"body": JSON.stringify({
			"operationName": "addToCart",
			"variables": {
				"productId": productId,
				"quantity": 1,
				"cartId": cartId
			},
			"query": "mutation addToCart($productId: ID!, $quantity: Int!, $cartId: ID) {\n  addToCart(productId: $productId, quantity: $quantity, cartId: $cartId) {\n    id\n    items {\n      product {\n        id\n        name\n        subName\n        type\n        thumbnailUrl\n        attributes {\n          name\n          value\n          __typename\n        }\n        canonicalUrlSlug\n        adultsOnly\n        category {\n          name\n          __typename\n        }\n        printOrSellOnly\n        __typename\n      }\n      unitPrice {\n        ...MoneyFields\n        __typename\n      }\n      totalPrice {\n        ...MoneyFields\n        __typename\n      }\n      totalDiscountedPrice {\n        ...MoneyFields\n        __typename\n      }\n      discounts {\n        code\n        type\n        __typename\n      }\n      quantity\n      selfPurchase\n      __typename\n    }\n    appliedVolumeDiscounts {\n      discountPercentage\n      totalDiscountAmount {\n        ...MoneyFields\n        __typename\n      }\n      __typename\n    }\n    appliedVolumeDiscountsTotal {\n      ...MoneyFields\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment MoneyFields on Money {\n  amount\n  currency\n  __typename\n}\n"
}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

/* cartId is from the result of addToCart(). This function is used in lulu-shop
	and lulu-cart. */
async function viewCart(cartId) {
	const response = await fetch("https://api.lulu.com/graphql/", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "*/*",
		    "Accept-Language": "en",
		    "content-type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://www.lulu.com/",
		"body": JSON.stringify({
			"operationName": "cart",
			"variables": {
				"id": cartId,
				"visit": true
			},
			"query": "query cart($id: ID!, $visit: Boolean) {\n  cart(id: $id, visit: $visit) {\n    id\n    ...Cart\n    __typename\n  }\n}\n\nfragment Cart on CartDetails {\n  items {\n    product {\n      id\n      name\n      subName\n      type\n      thumbnailUrl\n      attributes {\n        name\n        value\n        __typename\n      }\n      canonicalUrlSlug\n      adultsOnly\n      category {\n        name\n        __typename\n      }\n      printOrSellOnly\n      __typename\n    }\n    unitPrice {\n      ...MoneyFields\n      __typename\n    }\n    totalPrice {\n      ...MoneyFields\n      __typename\n    }\n    totalDiscountedPrice {\n      ...MoneyFields\n      __typename\n    }\n    discounts {\n      code\n      type\n      value\n      __typename\n    }\n    quantity\n    selfPurchase\n    __typename\n  }\n  subtotal {\n    ...MoneyFields\n    __typename\n  }\n  firstVisit\n  lastVisit\n  appliedVolumeDiscounts {\n    discountPercentage\n    totalDiscountAmount {\n      ...MoneyFields\n      __typename\n    }\n    __typename\n  }\n  appliedVolumeDiscountsTotal {\n    ...MoneyFields\n    __typename\n  }\n  __typename\n}\n\nfragment MoneyFields on Money {\n  amount\n  currency\n  __typename\n}\n"
	}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// This is used on lulu-cart.
async function proceedToNewCheckout(cartId) {
	const response = await fetch("https://api.lulu.com/graphql/", {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "*/*",
		    "Accept-Language": "en",
		    "content-type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://www.lulu.com/",
		"body": JSON.stringify({
			"operationName": "proceedToNewCheckout",
			"variables": {
				"cartId": cartId,
				"currency": "USD",
				"language": "EN"
		},
		"query": "mutation proceedToNewCheckout($cartId: ID!, $currency: CurrencyEnum!, $language: LanguageEnum!) {\n  checkout {\n    proceedToCheckout(cartId: $cartId, currency: $currency, language: $language) {\n      checkout {\n        publicId\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
// responseJson.data.checkout.proceedToCheckout.checkout.publicId is the UUID(?) used in the url for lulu-checkout
