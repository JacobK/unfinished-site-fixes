/*
yourhotspot-net-login
https://yourhotspot.net/3125/login/**
e.g. https://yourhotspot.net/3125/login/3230/<mac>/<ip>
*/

/* Unfortunately, the page that redirects to this one uses JavaScript and that
	page takes on the URL of whatever site you go to. I'm not sure how Haketilo
	could support this, but I think it's worth trying. Maybe we could have a fix
	for neverssl.com that works for lots of different Wi-Fi sites. */

/* Script for page that redirects to this one:
document.location.assign(document.scripts[0].textContent.match(/location.replace\('([^']*)'\);/)[1]);
*/

/* Also, this page asked for my full name, email, date of birth, and phone
	number when I visited without JavaScript enabled. I tried to submit, but it
	just took me back to the page I was already on, and internet was not
	working. When I ran the nonfree scripts, it did not ask for that information
	and I was connected quickly. Once I set my MAC address to something else and
	tried my own script, without entering any information in the form, I managed
	to get to the "You are connected" message, but it seems that I am not
	actually connected. I noticed I can visit IP addresses directly, and this
	works even if I
	re-randomize my MAC address. */

// redirect to https://yourhotspot.net/3125/welcome/<mac>/<ip>

document.location.assign(`https://yourhotspot.net/${document.URL.split('/')[3]}/welcome/${document.URL.split('/')[6]}/${document.URL.split('/')[7]}`);
