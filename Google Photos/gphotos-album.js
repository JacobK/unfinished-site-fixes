/*
gphotos-album
https://photos.google.com/share/*
*/

// very hacky way to get a download link for the album
let downloadURL = null;
for (script of document.scripts) {
  const match = script.textContent.match(`"${document.title.split(" - Google Photos")[0]}"[^"]*"([^"]*)"`);
	if (match)
		downloadURL = match[1];
}
if (downloadURL)
	console.info(downloadURL);
else
	console.info("couldn't get URL");
