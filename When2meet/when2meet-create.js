/*
when2meet-create

*/

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

async function getDocFromHtml(pageHtml) {
	const parser = new DOMParser();
	const alt_doc = parser.parseFromString(pageHtml, "text/html");
	return alt_doc;
}

// timeZone is e.g. "US/Central".
async function createEvent(eventName, timeZone = "US/Central", earliestHour = 0, latestHour = 0) {
	const response = await fetch("https://www.when2meet.com/SaveNewEvent.php", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded",
		    "Upgrade-Insecure-Requests": "1",
		    "Sec-Fetch-Dest": "document",
		    "Sec-Fetch-Mode": "navigate",
		    "Sec-Fetch-Site": "same-origin",
		    "Sec-Fetch-User": "?1"
		},
//		"referrer": "https://www.when2meet.com/",
		"body": `NewEventName=${formUrlEncode(eventName)}&DateTypes=DaysOfTheWeek&PossibleDates=${formUrlEncode("0|1|2|3|4|5|6")}&NoEarlierThan=${earliestHour}&NoLaterThan=${latestHour}&TimeZone=${formUrlEncode(timeZone)}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	return responseText;
}

async function createAndNavigate() {
	const createHtml = await createEvent("test event", "US/Central", 0, 0);
	const tempDocument = await getDocFromHtml(createHtml);
	const matchArr = tempDocument.body.getAttribute("onload").match("window.location='([^']*)'");
	document.location.assign(matchArr[1]);
}
