/*
when2meet-join

*/

const eventId = document.URL.split("?")[1].split("-")[0];

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

async function login(name, password = "") {
	const response = await fetch("https://www.when2meet.com/ProcessLogin.php", {
		"credentials": "include",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "text/javascript, text/html, application/xml, text/xml, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Requested-With": "XMLHttpRequest",
		    "X-Prototype-Version": "1.7.3",
		    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		// referrer will be set by browser I think
		"body": `id=${eventId}&name=${formUrlEncode(name)}&password=${formUrlEncode(password)}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	return responseText;
}
// responseText is the user id

// userId is from login()
async function saveTimes(userId, availabilityGrid) {
	const response = await fetch("https://www.when2meet.com/SaveTimes.php", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/116.0",
		    "Accept": "text/javascript, text/html, application/xml, text/xml, */*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "X-Requested-With": "XMLHttpRequest",
		    "X-Prototype-Version": "1.7.3",
		    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		// referrer will be set by browser I think
		"body": `person=${userId}&event=${eventId}&slots=&availability=${availabilityGrid}&ChangeToAvailable=true`,
		"method": "POST",
		"mode": "cors"
	});
}
