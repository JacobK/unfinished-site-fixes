/*
login-awe-cluster-att-wifi-com
https://login-awe-cluster.attwifi.com/guest/enterprise.php
*/
/* This should actually be fully functional, though I haven't tested it yet.
	Though, it doesn't require pressing a button, so the user doesn't get a
	chance to read the terms of service. That would be easy to fix though. */

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

async function connect(bVal, nasId, url, switchIp, vcName, apMac, apName, ipAddr, essid, macAddr, cmd) {
	await fetch("https://login-awe-cluster.attwifi.com/guest/enterprise.php?_browser=1", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded",
		    "Upgrade-Insecure-Requests": "1",
		    "Sec-Fetch-Dest": "document",
		    "Sec-Fetch-Mode": "navigate",
		    "Sec-Fetch-Site": "same-origin",
		    "Sec-Fetch-User": "?1"
		},
		// Referrer is filled in by the browser.
		"body": `b=${formUrlEncode(bVal)}&nas-id=${formUrlEncode(nasId)}&url=${formUrlEncode(url)}&switchip=${formUrlEncode(switchIp)}&vcname=${formUrlEncode(vcName)}&apmac=${formUrlEncode(apMac)}&apname=${formUrlEncode(apName)}&ip=${formUrlEncode(ipAddr)}&essid=${formUrlEncode(essid)}&mac=${formUrlEncode(macAddr)}&cmd=${formUrlEncode(cmd)}&no_login=&static_u=&no_u=&no_p=&user=Anonymous_Ent&password=`,
		"method": "POST",
		"mode": "cors"
	});
}

/* Note: this original request corresponding to this function sets off the
	network boundary shield in JShelter. It seems like NoScript may block even
	this script in some circumstances, but I have not done significant testing. */
function login() {
	//document.querySelector('form[name="weblogin_form"]').submit(); // Uh, I think a website update broke this selector?
	document.querySelector('form').submit();
}


async function main() {
	const queryObj = new URLSearchParams(document.location.search);
	if (queryObj.get("b")) {
		await connect(queryObj.get("b"), queryObj.get("nas-id"), queryObj.get("url"), queryObj.get("switchip"), queryObj.get("vcname"), queryObj.get("apmac"), queryObj.get("apname"), queryObj.get("ip"), queryObj.get("essid"), queryObj.get("mac"), queryObj.get("cmd"));
	}
	login();
}
main();
