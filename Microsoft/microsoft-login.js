/*
	identifier: microsoft-login
	long name: Login for Microsoft
	description: allow login to a Microsoft account without proprietary software
	URL patterns:
		https://login.microsoftonline.com/commons/oauth2/authorize
		https://login.microsoftonline.com/8d281d1d-9c4d-4bf7-b16e-032d15de9f6c/oauth2/authorize
		https://login.microsoftonline.com/common/oauth2/authorize
*///	URL pattern: https://login.microsoftonline.com/*/oauth2/authorize

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

/*	example URL (with lots of redactions): https://login.microsoftonline.com/8d281d1d-9c4d-4bf7-b16e-032d15de9f6c/oauth2/authorize?client_id=${redacted2}&response_mode=form_post&protectedtoken=true&response_type=code%20id_token&resource=${redacted3}&scope=openid&nonce=${redacted4}&redirect_uri=https%3A%2F%2Fcometmail.sharepoint.com%2F_forms%2Fdefault.aspx&state=OD0w&login_hint=${redacted5}&claims=%7B%22id_token%22%3A%7B%22xms_cc%22%3A%7B%22values%22%3A%5B%22CP1%22%5D%7D%7D%7D&wsucxt=1&cobrandid=${redacted6}&client-request-id=${redacted7}

There is no redacted1 because I had initially redacted
	"8d281d1d-9c4d-4bf7-b16e-032d15de9f6c" but then I found it online at
	<https://atlas.utdallas.edu/TDClient/30/Portal/KB/ArticleDet?ID=301>.

Potentially interesting information about the "claims" parameter:
	<https://learn.microsoft.com/en-us/azure/active-directory/develop/claims-challenge>
*/

// WARNING: Microsoft will be able to tell that you are using this script!

const notUnderstood = {
	"i19": "10800" // not sure how this is generated, has been "4726464", "10800", and "11891"
}

let config = null;
for (const script of document.scripts) {
	const match = /Config=({([^;]|[^}];)+})/.exec(script.textContent); // looks for "Config=" in the script files and grabs the json after that.
	if (!match)
	continue;
	config = JSON.parse(match[1]);
}

// The following modified exceprt from Box fix is Copyright 2022 Wojtek Kosior <koszko@koszko.org>, licensed identically to this script (LicenseRef-GPL-3.0-or-later-WITH-js-exceptions).
/* begin modified excerpt from Box fix */
/* Replace current page contents. */
const replacement_html = `\
<!DOCTYPE html>
<html>
  <head>
    <style>
      button, .button {
          border-radius: 10px;
          padding: 20px;
          color: #333;
          background-color:
          lightgreen;
          text-decoration: none;
          display: inline-block;
      }
      button:hover, .button:hover {
          box-shadow: -4px 8px 8px #888;
      }

      .hide {
          display: none;
      }

      #download_button .unofficial, #download_button .red_note {
          display: none;
      }
      #download_button.unofficial .unofficial {
          display: inline;
      }
      #download_button.unofficial .red_note {
          display: block;
      }
      .red_note {
          font-size: 75%;
          color: #c55;
          font-style: italic;
          text-align: center;
      }
    </style>
  </head>
  <body>
	<form>
		<input id="username" type="text" placeholder="email" value="" />
		<input id="password" type="password" placeholder="password" value="" />
		<button id="loginButton" >login</button>
	</form>
  </body>
</html>
`;

/*
 * We could instead use document.write(), but browser's debugging tools would
 * not see the new page contents.
 */
const parser = new DOMParser();
const alt_doc = parser.parseFromString(replacement_html, "text/html");
document.documentElement.replaceWith(alt_doc.documentElement);

const nodes = {};
document.querySelectorAll('[id]').forEach(node => nodes[node.id] = node);
/* end modified excerpt from Box fix */


// TODO: get these variables from text input
 // The password should be hidden on-screen (e.g. show "***********" instead of "Micro$oft19")

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

// This is after submitting the invisible form

const instrumentationKey = config.clientEvents.appInsightsConfig.instrumentationKey;
async function login(ev) {
	const username = document.getElementById("username").value;
	const password = document.getElementById("password").value;
	document.getElementById("loginButton").remove();
	
	fetch("https://login.microsoftonline.com/common/login", {
		"credentials": "include",
		"headers": {
		    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded",
		    "Upgrade-Insecure-Requests": "1",
		    "Sec-Fetch-Dest": "document",
		    "Sec-Fetch-Mode": "navigate",
		    "Sec-Fetch-Site": "same-origin",
		    "Sec-Fetch-User": "?1"
		},
		// referrer will be set by browser I think
		"body": "i13=0&login="+encodeURIComponent(username)+"&loginfmt="+encodeURIComponent(username)+"&type=11&LoginOptions=3&lrt=&lrtPartition=&hisRegion=&hisScaleUnit=&passwd="+formUrlEncode(password)+"&ps=2&psRNGCDefaultType=&psRNGCEntropy=&psRNGCSLK=&canary="+encodeURIComponent(config.canary)+"&ctx="+encodeURIComponent(config.sCtx)+"&hpgrequestid="+encodeURIComponent(config.sessionId)+"&flowToken="+encodeURIComponent(config.sFT)+"&PPSX=&NewUser=1&FoundMSAs=&fspost=0&i21=0&CookieDisclosure=0&IsFidoSupported=1&isSignupPost=0&i19="+notUnderstood.i19,
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		const responseText = await response.text();
		// next 3 lines based on code from the fix for Box, Copyright 2022 Wojtek Kosior <koszko@koszko.org>
		const parser = new DOMParser();
		const alt_doc = parser.parseFromString(responseText, "text/html");
		document.documentElement.replaceWith(alt_doc.documentElement);
		
		document.forms[0].submit(); // submit the form in the HTML from the request
	});
	ev.preventDefault();
}

document.getElementById("loginButton").addEventListener("click", login);
