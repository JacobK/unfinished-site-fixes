/*
sharepoint-slides
https://cometmail-my.sharepoint.com/:p:/g/personal/**
https://intervarsity365-my.sharepoint.com/personal/**
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* siteId, webId, and listId look like lowercase UUIDs, and listItemId was a 4
	digit number in my case. */
async function getInfo(siteId, webId, listId, listItemId) {
	const response = await fetch(`https://${document.domain}/_api/v2.0/sites/cometmail-my.sharepoint.com,${siteId},${webId}/lists/${listId}/items/${listItemId}/driveItem?expand=openWith`, {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
		    "Accept": "application/json; odata.metadata=none",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		// referrer will be added by browser
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

// gets a bunch of variables from the page's scripts, most of which are not actually used though
let scriptVars = {};
for (script of document.scripts) {
	let tempString = script.textContent;
	while (match = /var (\S*) = ([^;]*);/.exec(tempString)) {
		scriptVars[match[1]] = match[2];
		tempString = tempString.replace("var ", "varn't ");
	}
}
// gets a certain variable again due to semicolons in the data
// (TODO: Fix the general code and remove this variable-specific hack.)
for (script of document.scripts) {
	let tempString = script.textContent;
	if (match = /var (_spPageContextInfo)=([^;]*);/.exec(tempString)) {
		scriptVars[match[1]] = match[2];
	}
}
// gets a certain variable again due to semicolons in the data
// (TODO: Fix the general code and remove this variable-specific hack.)
for (script of document.scripts) {
	let tempString = script.textContent;
	if (match = /var (_wopiContextJson) =([^;]*);/.exec(tempString)) {
		scriptVars[match[1]] = match[2];
	}
}
const pageContextInfo = JSON.parse(scriptVars._spPageContextInfo);
const siteId = pageContextInfo.siteId.match(/{(.*)}/)[1];
const webId = pageContextInfo.webId.match(/{(.*)}/)[1];
const wopiContextJson = JSON.parse(scriptVars._wopiContextJson);
const listId = wopiContextJson.ListId;
const listItemId = wopiContextJson.ListItemId;

await getInfo(siteId, webId, listId, listItemId);

