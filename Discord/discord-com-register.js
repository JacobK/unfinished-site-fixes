/*
discord-com-register
https://discord.com/register
*/

const assetUrl = "https://newassets.hcaptcha.com/captcha/v1/3b797c3/static"; // Can be found as `assetUrl:"https://newassets.hcaptcha.com/captcha/v1/b1c18b1/static"` in https://js.hcaptcha.com/1/api.js but maybe not if the URL query string contains "custom=True".
//const recaptchaCompat = "true"; // also in api.js


/*
These IDs were harvested by running the nonfree hCaptcha client, and the
code after the IDs gets a list of chars contained in the ids.
*/
const exampleIds = [
  "02m92yr33lfl",
  "03pygb1i3iv9",
  "0nceqtjwt65",
  "0lbdo83rok5p",
  "06ojhj1ubrf",
  "0rj4m25qpxdl",
  "0e0ldsa1f4ju",
  "0txa5nixytxj",
  "09cllnm4q4ai",
  "0tlmlrtoxy8",
  "0i74euiyes7l",
  "0tdce6d808yr",
  "0ttbnmojq8j",
  "06r9dq0ilupo",
  "0or2cqiij9gb",
  "0pr3gihljewq",
  "0y5bgasdl7w",
  "0gqh2a2za3xi" // from Discord, much later than the example before
]
function getAllChars(strArr) {
	let allChars = "";
	for (str of strArr) { // for every example id
		for (let i = 0; i < str.length; i++) { // iterate through every char in the id
			let inAllChars = false;
			// make sure the char isn't already in allChars
			for (let j = 0; j < allChars.length; j++) {
				if (allChars.at(j) == str.at(i)) {
					inAllChars = true;
					break; // exit the inner loop
				}
			}
			if (!inAllChars) {
				allChars += str.at(i);
			}
		}
	}
	return allChars;
}
const allChars = getAllChars(exampleIds);

// function from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}




function generateId() {
	const length = getRandomIntInclusive(11, 12);
	let idString = "0";
	for (let i = 1; i < length; i++) {
		idString += allChars.at(getRandomIntInclusive(0, allChars.length - 1));
	}
	return idString;
}


async function register(email, displayName, username, password, birthMonth, birthDay, birthYear, fingerprint, captchaKey) {
	const headers = {
//		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0",
		"Accept": "*/*",
		"Accept-Language": "en-US,en;q=0.5",
		"Content-Type": "application/json",
		"X-Super-Properties": btoa(JSON.stringify({
			"os": navigator.oscpu.split(" ")[0],
			"browser": navigator.userAgent.split(" ")[6].split("/")[0],
			"device": "",
			"system_locale": navigator.language,
			"browser_user_agent": navigator.userAgent,
			"browser_version": navigator.userAgent.split(" ")[6].split("/")[1],
			"os_version": "",
			"referrer": "",
			"referring_domain": "",
			"referrer_current": "",
			"referring_domain_current": "",
			"release_channel": "stable",
			"client_build_number": 258377,
			"client_event_source": null
		})),
	    "X-Fingerprint": fingerprint,
	    "X-Discord-Locale": "en-US",
	    "X-Discord-Timezone": "UTC",
	    "X-Debug-Options": "bugReporterEnabled",
	    "Sec-Fetch-Dest": "empty",
	    "Sec-Fetch-Mode": "cors",
	    "Sec-Fetch-Site": "same-origin"
	};
	if (captchaKey) {
		headers["X-Captcha-Key"] = captchaKey;
	}
	
	
	const response = await fetch("https://discord.com/api/v9/auth/register", {
		"credentials": "include",
		"headers": headers,
		"referrer": "https://discord.com/register",
		"body": JSON.stringify({
			"fingerprint": fingerprint,
			"email": email,
			"username": username,
			"global_name": displayName,
			"password": password,
			"invite": null,
			"consent": true, /* Interestingly, the US version of the page does
				not require checking a box saying one agrees to the terms of
				service and privacy policy, while some other versions of the
				site appear to require that */
			"date_of_birth": `${birthYear}-${birthMonth}-${birthDay}`,
			"gift_code_sku_id": null
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

class DisClient {
	captchaRequired;
	captchaSiteKey;
	captchaService;
	token;
	fingerprint;
	
	async experiments() {
		const response = await fetch("https://discordapp.com/api/v9/experiments", {
			headers: {
				"X-Context-Properties": btoa(JSON.stringify({"location": "Login"})), // based on info from harmony source code
			}
		});
		const responseJson = await response.json();
		this.fingerprint = responseJson.fingerprint;
		return responseJson;
	}
	
	async init() {
		await this.experiments();
		return this;
	}
	
	
	
	async beginRegistration(email, displayName, username, password, birthMonth, birthDay, birthYear) {
		const registerResponse = await register(email, displayName, username, password, birthMonth, birthDay, birthYear, this.fingerprint);
		if (registerResponse.errors) {
			console.error("error");
		} else if (registerResponse.token) {
			console.info("Login successful!!");
			this.token = registerResponse.token; // TODO: store locally as well
			return registerResponse.token;
		} else {
			if (registerResponse.captcha_key && registerResponse.captcha_key[0] == "captcha-required") {
				this.captchaRequired = true;
				this.captchaSiteKey = registerResponse.captcha_sitekey;
				this.captchaService = registerResponse.captcha_service;
			}
		}
		return this;
	}
	
	async getSiteKey() {
		if (this.captchaRequired == true) {
			return this.captchaSiteKey;
		} else {
			console.error("Captcha not currently required - maybe try registering first.");
			return null;
		}
	}
}

class HcaptchaInstance {
	siteKey;
	siteOrigin = document.URL.split("/").slice(0,3).join("/"); // can probably be reused on any site!
	apiJsUrl = "https://js.hcaptcha.com/1/api.js?render=explicit&onload=hcaptchaOnLoad"; // TODO: get automatically
	apiJsParams = new URLSearchParams(this.apiJsUrl.split("?")[1]);
	id = generateId();
	globalIframe;
	localIframes = [];
	
	// initialize the assumed-to-be-one-per-page iframe
	initGlobalIframe() {
		const frame = document.createElement("iframe");
		// Actual src was s/#/?_v=ztr1vsjq3n#/non-regex of this
		let globalIframeSrc = `${assetUrl}/hcaptcha.html#frame=challenge&id=${this.id}&host=${document.domain}&sentry=true&reportapi=`;
		if (this.apiJsParams.get("reportapi")) {
			globalIframeSrc += `${encodeURIComponent(this.apiJsParams.get("reportapi"))}`;
		}
		globalIframeSrc += `&recaptchacompat=true&custom=`;
		if (this.apiJsParams.get("custom")) {
			globalIframeSrc += `${this.apiJsParams.get("custom")}`;
		}
		globalIframeSrc += `&hl=en&tplinks=on&sitekey=${this.siteKey}&theme=light&origin=${encodeURIComponent(this.siteOrigin)}`;
		frame.src = globalIframeSrc;
		document.body.appendChild(frame);
		this.globalIframe = frame;
		return frame;
	}

	// initialize a probably one-per-form iframe
	initLocalIframe() {
		const frame = document.createElement("iframe");
		let globalIframeSrc = `${assetUrl}/hcaptcha.html#frame=checkbox&id=${this.id}&host=${document.domain}&sentry=true&reportapi=`;
		if (this.apiJsParams.get("reportapi")) {
			globalIframeSrc += `${encodeURIComponent(this.apiJsParams.get("reportapi"))}`;
		}
		globalIframeSrc += `&recaptchacompat=true&custom=`;
		if (this.apiJsParams.get("custom")) {
			globalIframeSrc += `${this.apiJsParams.get("custom")}`;
		}
		globalIframeSrc += `&hl=en&tplinks=on&sitekey=${this.siteKey}&theme=light&origin=${encodeURIComponent(this.siteOrigin)}`;
		frame.src = globalIframeSrc;
		document.body.appendChild(frame);
		this.localIframes.push(frame);
		return frame;
	}
	
	
}

window.addEventListener("message", (event) => {
	if (event.origin !== "https://newassets.hcaptcha.com") {
		console.error("Bad origin:", event.origin);
		console.error("message missed:", event);
		return;
	}
	
	if (false) {
		// do something
	} else {
		console.error("unknown message:", event);
	}
	
}, false);

async function test() {
	const cord = new DisClient();
	const cap = new HcaptchaInstance();
	await cord.init();
	await cord.beginRegistration("subtotal@donator.approval", "aghast", "utopiafoothold", "subtotal donator approval aghast utopia foothold", "11", "20", "1924");
	cap.siteKey = cord.getSiteKey();
}

