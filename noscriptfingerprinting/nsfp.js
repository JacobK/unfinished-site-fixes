// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

block = "";
for (liElement of document.getElementsByClassName("signals__list")[0].querySelectorAll("li")) {
  let row = "`";
	row += liElement.querySelector(".signals__title").querySelector("span").innerText;
	row += "` is `";
  row += liElement.getElementsByClassName("signals__detail-value")[1].innerText;
	row += "`\n";
	block += row;
}
console.info(block);
