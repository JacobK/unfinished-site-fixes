/*
stripe-checkout
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* Use where you would run <https://checkout.stripe.com/checkout.js>, in
	addition to the page's other scripts */

const checkoutFramePage = "oivkx0oP8BgueCG8QFpDfA"; // in checkout.js somewhere

function genUUID() {
	return crypto.randomUUID();
}

function initCheckoutFrame() {
	const checkoutFrame = document.createElement("iframe");
	// attributes copied from original rendered page
	checkoutFrame.classList.add("stripe_checkout_app");
	checkoutFrame.setAttribute("frameborder", "0");
	checkoutFrame.setAttribute("allowtransparency", "true");
	checkoutFrame.setAttribute("style", "z-index: 2147483647; display: none; background: rgba(0, 0, 0, 0.005); border: 0px transparent; overflow: hidden auto; visibility: visible; margin: 0px; padding: 0px; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%;");
	checkoutFrame.setAttribute("name", "stripe_checkout_app");
	checkoutFrame.src = `https://checkout.stripe.com/v3/${checkoutFramePage}.html?distinct_id=${StripeCheckout.distinctId}`;
	document.body.appendChild(checkoutFrame);
	StripeCheckout.checkoutFrame = checkoutFrame;
}

const StripeCheckout = {};
StripeCheckout.configure = function(json) {
	const handler = {};
	StripeCheckout.configuration = json;
	handler.open = function() {
		StripeCheckout.checkoutFrame.style.display = "block";
		StripeCheckout.renderConfiguration = StripeCheckout.preloadConfiguration;
		StripeCheckout.renderConfiguration.referrer = document.location.href;
		StripeCheckout.renderConfiguration.url = document.location.href;
		StripeCheckout.renderConfiguration.timeLoaded = new Date().setMilliseconds(0)/1000;
		StripeCheckout.renderConfiguration.createSource = false;
		StripeCheckout.renderConfiguration.supportsTokenCallback = false;
		StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"render","args":["","iframe", StripeCheckout.renderConfiguration],"id":3}), "https://checkout.stripe.com");
		StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"open","args":[{"timeLoaded": StripeCheckout.renderConfiguration.timeLoaded}],"id":4}), "https://checkout.stripe.com");
	};
	handler.close = function() {
		StripeCheckout.checkoutFrame.style.display = "none";
	}
	return handler;
};
StripeCheckout.distinctId = genUUID();

window.addEventListener("message", (event) => {
	console.log(event);
	if (event.origin !== "https://checkout.stripe.com") {
		return;
	}
	if (JSON.parse(event.data).method == "frameReady") {
		StripeCheckout.preloadConfiguration = {};
		for (value of ["key", "image", "locale", "amount", "name", "description", "zipCode", "panelLabel", "allowRememberMe", "email"]) {
			StripeCheckout.preloadConfiguration[value] = StripeCheckout.configuration[value];
		}
		StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"preload","args":[StripeCheckout.preloadConfiguration],"id":1}), "https://checkout.stripe.com");
		StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"frameCallback","args":[1,false],"id":2}), "https://checkout.stripe.com");
	} else if (JSON.parse(event.data).method == "frameCallback") {
		if (JSON.parse(event.data).args[0] == 4) {
			StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"frameCallback","args":JSON.parse(event.data).args,"id":5}));
		}
	} else if (JSON.parse(event.data).method == "setToken") {
		StripeCheckout.tokenJson = JSON.parse(event.data);
		StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"frameCallback","args":[6,null],"id":6}));
		StripeCheckout.configuration.token(JSON.parse(event.data).args[0].token);
	} else if (JSON.parse(event.data).method == "closed") {
		StripeCheckout.checkoutFrame.contentWindow.postMessage(JSON.stringify({"method":"frameCallback","args":[7,null],"id":7}));
	}
});
initCheckoutFrame();
