/*
checkout-stripe-com
https://checkout.stripe.com/v3/*
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

function getPaymentUserAgent() {
	return "Haketilo Userscript Development";
}

function getTimeOnPage() {
	return 22179; // TODO: fix
}

// device_id=DNT was in the traffic from the nonfree scripts
/* cardNumber is a credit card number with spaces every 4 numbers, cardExpYear
	has the whole year, e.g. "2028" */
async function tokens(email, cardNumber, cardCvc, cardExpMonth, cardExpYear, zipCode) {
	const response = await fetch("https://api.stripe.com/v1/tokens", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:129.0) Gecko/20100101 Firefox/129.0",
		    "Accept": "application/json",
		    "Accept-Language": "en-US",
		    "Content-Type": "application/x-www-form-urlencoded",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site",
		    "Priority": "u=0"
		},
		"referrer": "https://checkout.stripe.com/",
		"body": `email=${formUrlEncode(email)}&validation_type=card&payment_user_agent=${getPaymentUserAgent()}&user_agent=${formUrlEncode(navigator.userAgent)}&device_id=DNT&referrer=${StripeObject.renderConfiguration.referrer}&time_checkout_opened=${new Date().setMilliseconds(0)/1000}&time_checkout_loaded=${StripeObject.timeLoaded}&card[number]=${formUrlEncode(cardNumber)}&card[cvc]=${cardCvc}&card[exp_month]=${cardExpMonth}&card[exp_year]=${cardExpYear}&card[name]=${formUrlEncode(email)}&card[address_zip]=${zipCode}&time_on_page=${getTimeOnPage()}&guid=NA&muid=NA&sid=NA&key=${StripeObject.renderConfiguration.key}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

const StripeObject = {};

async function submitCardInfo() {
	const tokensJson = await tokens(StripeObject.renderConfiguration.email, document.getElementById("cardInput").value, document.getElementById("cvcInput").value, document.getElementById("expMonthIput").value, document.getElementById("expYearInput").value, document.getElementById("zipInput").value);
	parent.postMessage(JSON.stringify({"method":"setToken","args":[{"token":tokensJson,"args":{}}],"id":6}), StripeObject.parentOrigin);
}

window.addEventListener("message", (event) => {
	console.log(event);
	if (JSON.parse(event.data).method == "preload") {
		StripeObject.preloadConfiguration = JSON.parse(event.data).args[0];
		StripeObject.parentOrigin = event.origin;
	} else if (JSON.parse(event.data).method == "frameCallback") {
		if (JSON.parse(event.data).args[0] == 1) {
			parent.postMessage(JSON.stringify({"method":"frameCallback","args":JSON.parse(event.data).args,"id":2}), StripeObject.parentOrigin);
		} else if (JSON.parse(event.data).args[0] == 6) {
			parent.postMessage(JSON.stringify({"method":"closed","args":[],"id":7}));
		}
	} else if (JSON.parse(event.data).method == "render") {
		StripeObject.renderConfiguration = JSON.parse(event.data).args[2];
		document.write(`
card number (with spaces between each set of 4 numbers): <input id=cardInput /><br>
cvc: <input id=cvcInput /><br>
expiration month, numberical, with no preceeding 0: <input id=expMonthIput /><br>
expiration year, 4 digits: <input id=expYearInput /><br>
zip code: <input id=zipInput /><br>
<button id=submitButton>submit</submit>
		`);
		document.getElementById("submitButton").addEventListener("click", submitCardInfo);
	} else if (JSON.parse(event.data).method == "open") {
		StripeObject.timeInfo = JSON.parse(event.data).args[0];
		parent.postMessage(JSON.stringify({"method":"frameCallback","args":[3,true],"id":3}), StripeObject.parentOrigin);
		parent.postMessage(JSON.stringify({"method":"opened","args":[],"id":4}), StripeObject.parentOrigin);
		parent.postMessage(JSON.stringify({"method":"frameCallback","args":[4,null],"id":5}), StripeObject.parentOrigin);
	}
});

StripeObject.parentOrigin = "*";
parent.postMessage(JSON.stringify({"method":"frameReady","args":[],"id":1}), StripeObject.parentOrigin);
StripeObject.timeLoaded = new Date().setMilliseconds(0)/1000;
