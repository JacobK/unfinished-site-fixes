/*
m-stripe-network-inner
https://m.stripe.network/inner.html
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* Input is a fingerprint to give to Stripe, output is JSON describing the
	fingerprint. */
function printToJson(print) {
	return JSON.parse(decodeURIComponent(atob(print)));
}

/* Input is a JSON object, output is a fingerprint to give to Stripe. */
function jsonToPrint(json) {
	return btoa(encodeURIComponent(JSON.stringify(json)))
}

async function getIds() {
	await fetch("https://m.stripe.com/6", {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "*/*",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "text/plain;charset=UTF-8",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "cross-site"
		},
		"referrer": "https://m.stripe.network/",
		"method": "POST",
		"mode": "cors"
	});
}
/* Sending an empty body still gets a 200 OK response, with different
	muid and sid, but the same guid. Sending a fingerprint of {}
	(i.e. "JTdCJTdE") gets a 400 Bad Request response. Sending a fingerprint
	in which b.b is a different length seems to return a 400 Bad Request, but
	replacing every character except for the '/' characters with 'A' still
	gets a 200. Omitting b.b entirely doesn't work. Sending nothing might be
	okay, but then the muid and sid will be different than what the URL
	contains, which seems bad. Though, it's quite possible the muid and sid
	were randomly generated anyway. */
