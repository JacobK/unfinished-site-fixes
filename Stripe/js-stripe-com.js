/*
js-stripe-com
https://js.stripe.com/v3/*
*/

// SPDX-FileCopyrightText: 2023-2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// function from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

function genStripeJsVal() {
	const chars = "123456789abcdef";
	let str = "";
	for (let i = 0; i < 9; i++) {
		str += chars.at(getRandomIntInclusive(0, chars.length - 1));
	}
	return str;
}

function getPaymentUserAgent() {
	return "Haketilo Userscript Development";
}

const stripeJsVal = genStripeJsVal(); /* Instead of randomly generated, this should
	be sent from learn-zybooks-com-signin. */

/* cardNumber is a credit card number, cardCvc is that card's CVC number,
	expMonth is the expiration month in the form "MM" (e.g. "08" for August),
	expYear is the card's expiration year in the form "YY" (e.g. "23" for
	2023), and zipCode is the card's 5-digit zip code, I guess. guid, muid, and
	sid are from m-stripe-network-inner. stripeJsVal is
	complicated. timeSincePageLoad is the time in miliseconds since the page
	was loaded, I think. publicKey is from learn-zybooks-com. guid, muid, and
	sid were all "NA" when I ran the proprietary scripts on
	<https://snowdrift.coop/payment-info>, which uses js.stripe.com/v2/ instead
	of v3. I was also using some software such as uBlock Origin which may have
	blocked those values.*/
async function tokens(cardNumber, cardCvc, expMonth, expYear, zipCode, guid, muid, sid, stripeJsVal, timeSincePageLoad, publicKey) {
	const response = await fetch("https://api.stripe.com/v1/tokens", {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/x-www-form-urlencoded",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://js.stripe.com/",
		// I had payment_user_agent as ${encodeURIComponent(`stripe.js/${stripeJsVal};+stripe-js-v3/${stripeJsVal}`)} for ZyBooks
		"body": `card[number]=${cardNumber}&card[cvc]=${cardCvc}&card[exp_month]=${expMonth}&card[exp_year]=${expYear}&card[address_zip]=${zipCode}&guid=${guid}&muid=${muid}&sid=${sid}&payment_user_agent=${formUrlEncode(getPaymentUserAgent())}&time_on_page=${timeSincePageLoad}&key=${publicKey}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* The returned JSON includes several values, importantly, "id", which is a
	string that looks like "tok_????????????????????????" */
