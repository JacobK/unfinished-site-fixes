/*
learn-zybooks-com-reset
https://learn.zybooks.com/reset/*
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

const resetCode = document.URL.split('/')[4];

/* resetCode is the code in the * part of the URL, and passwordStr is a string
	containing the new password. The official app does a client-side check
	that 2 boxes in which the user types the password are equal. */
async function resetPassword(resetCode, passwordStr) {
	const response = await fetch("https://zyserver.zybooks.com/v1/forgot/reset", {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"body": JSON.stringify({
			"reset_code": resetCode,
			"password": passwordStr
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Response should be simply {"success":true} if the request is valid.
	Invalid requests have not been tested. */
