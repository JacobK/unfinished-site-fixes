/*
learn-zybooks-com-signin
https://learn.zybooks.com/***
e.g.
https://learn.zybooks.com/signin
https://learn.zybooks.com/zybook/BOOK_CODE_HERE/chapter/3/section/1
	I think the official app only uses the URL display for cosmetic reasons
so I think signing in and viewing a book secttion makes sense to be in one
package.
*/

// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

const publicKey = JSON.parse(decodeURIComponent(document.querySelector("meta[name='zybooks-web/config/environment']").content)).stripe.publishableKey;

/* emailAddress is a string containing the email address, and passwordStr is a
	string containing the password. */
async function signin(emailAddress, passwordStr) {
	const response = await fetch("https://zyserver.zybooks.com/v1/signin", {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"body": JSON.stringify({
			"email": emailAddress,
			"password": passwordStr
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Return value should be JSON that may have an "error" object containing a
	numerical error "code" (e.g. 401) and a string "message" (e.g. "Invalid
	email or password."). In addition to the "error" object, a "success"
	boolean value exists. On a successful input, "success" will be true, and
	there will also be a "session" (containing some tokens) and "user" object. */

/* emailAddress is a string containing the user's email address. */
async function forgotPassword(emailAddress) {
	const response = await fetch("https://zyserver.zybooks.com/v1/forgot", {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"body": JSON.stringify({
			"email": emailAddress
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Return value should simply be {"success":true} for email address with an
	account. Email addresses without an account and invalid email addresses
	have not been tested. */

/* authToken is a authentication token from  the session object from signin.
	UserId is a user ID from the user object from signin. */
async function stream(authToken, userId) {
	const response = await fetch(`https://zyevents.zybooks.com/v1/stream?auth_token=${authToken}&channel=user:${userId}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "text/event-stream",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site",
			"Pragma": "no-cache",
			"Cache-Control": "no-cache"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
}
/* This method doesn't return anything, so maybe it's not important. Considering how
	often the official software sends this request, maybe it has something to
	do with refreshing the session? Bus if so, how come the refresh token
	isn't used? */

/* userId is a user ID from the user object from signin. authToken is an
	authentication token from the session object from signin. */
async function bookRequests(userId, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/user/${userId}/zybook_requests?auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Returns {"success":true,"zybook_requests":[]} for me. */

/* This has the same inputs as bookRequests. For some reason, the official
	software sends this request twice. Once immediately after bookRequests and
	once immediately after user. */
async function items(userId, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/user/${userId}/items?items=${encodeURIComponent(`["zybooks"]`)}&auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Response includes "success":true as well as an "items" object containing an
	array of zybooks objects. These are zyBooks the student has bought before. */

/* same inputs as bookRequests */
async function user(userId, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/user/${userId}?auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Returns "success":true and the user object (not sure if same object as
	earlier). */

/* bookCode is a code given to the student user by a teacher. When searching,
	this should be lowercase, but if you already know the exact book code from
	a search, it should have some capitalization. */
async function book(bookCode, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/zybooks?zybooks=${encodeURIComponent(`["${bookCode}"]`)}&auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* If you enter the code exactly, you should get JSON including "success":true
	as well as "lms_name" (null for me), "zybooks_with_links" (0 for me), and
	an array called "zybooks" that contains the zybook object the user entered
	the code of. The "zybook_code" within that object does contain
	capitalization, and such capitalized code is used in a later request with
	the same structure (using this same function). */


async function bookSearch(bookCode, authToken) {
	const responseJson = await book(bookCode.toLowerCase(), authToken);
}

/* bookCode is the code of a specific book (with capitalization), and authToken
	is the authentication token. */
async function roster(bookCode, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/zybook/${bookCode}/roster?zybook_roles=${encodeURIComponent(`["Instructor","TA","Student","Temporary","Dropped"]`)}&auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Returns "success" boolean and a roster object with multiple arrays of people
	objects. */

/* inputs same as roster() */
async function ordering(bookCode, authToken) {
	const response = await fetch(`https://zyserver2.zybooks.com/v1/zybook/${bookCode}/ordering?include=${encodeURIComponent(`["content_ordering"]`)}&auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* Returns "success" boolean and 2 objects, "default_ordering" and "ordering",
	which describe how the chapters were originally orderd, and how the
	instructor has ordered them. In my case, both orderings were the same, so
	I am not sure which one should be displayed. I assume it's "ordering". */

/* userId is the user's ID, bookCode is a book code, authToken is the user's
	authentication token. */
async function find(userId, bookCode, authToken) {
	const response = await fetch("https://zyserver.zybooks.com/v1/deferral_request/find", {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"body": JSON.stringify({
			"user_id": userId,
			"zybook_code": bookCode,
			"status": "pending",
			"auth_token": authToken
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* I'm not sure of the purpose of this request. It returned
	{"deferral_request":null,"success":true} */

/* TODO: Insert 2 Stripe frames for payment. The example URLs have been
	redacted because they contained lots of identifiers I don't understand. We
	should only insert the frames if we need them, and it might be good to
	make sure that arbitrary JavaScript execution is not enabled on the frame
	URL. */

/* In order to get a part of the "payment_user_agent" needed for Stripe
	(called stripeJsval in js-stripe-com), a
	request to https://js.stripe.com/v3/ with
	match(/return r}}\);var r="([^"]*)"/) is needed.  TODO: Is the script the
	same other than that value? */

/* bookCode is the code of the book the user want to read. chapterNumber and
	sectionNumber correspond to the part of the book the user wants to read. 
	authToken is the user's authentication token. */
async function getBookSection(bookCode, chapterNumber, sectionNumber, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/zybook/${bookCode}/chapter/${chapterNumber}/section/${sectionNumber}?auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* In addition to "success" boolean, the return will include a section object
	that contains a content_resources array with each part of the book. */

function getBookSectionHtmlFromSectionJson(section) {
	let htmlStr = `<!DOCTYPE html>
	<head>
		<style type="text/css">
.term {
  color: #000;
  font-style: italic;
  font-weight: bold;
  text-decoration: none;
}

p {
  font-family: roboto, sans-serif;
}
		</style>
	</head>
	<body>`;
	for (resource of section.content_resources) {
		if (resource.type == "html") {
			if (!resource.payload.html) {
				throw new Error(`resource.payload.html is undefined. Here's resource: ${JSON.stringify(resource)}`);
			}
			for (htmlObj of resource.payload.html) {
				htmlStr += htmlObj.text;
			}
		} else if (resource.type == "container") {
			htmlStr += `<h4>${resource.payload.name}</h4>`;
			for (htmlObj of resource.payload.html) {
				if (htmlObj.attributes[0].type == "html" && !htmlObj.attributes[1]) {
					htmlStr += htmlObj.text;
				} else if (htmlObj.attributes[0].type == "image" && !htmlObj.attributes[1]) {
					const attr = htmlObj.attributes[0];
					htmlStr += `<img src="https://zytools.zybooks.com/zyAuthor/${attr.subject}/8/IMAGES/${attr.filename}" alt_text="${attr.alt_text}" title="${attr.title}" width=${attr.width} height=${attr.height}>`;
				} else {
					// TODO: Show a warning saying that part of the textbook is missing.
					htmlStr += `<p> ${JSON.stringify(htmlObj)} </p>`;
					console.error("A part of the textbook is displayed improperly:", resource);
				}
			}
		} else if (resource.type == "custom") {
			if (resource.payload.tool == "zyAnimator") {
				htmlStr += `<h4>${resource.caption}</h4>`;
				htmlStr += `<p>${resource.payload.alt_text}</p>`;
				console.warn("A part of the textbook is displayed as alt text:", resource);
			} else {
				// TODO: Show a warning saying that part of the textbook is missing.
				htmlStr += `<p> ${JSON.stringify(resource.payload)} </p>`;
				console.error("A part of the textbook is displayed improperly:", resource);
			}
		} else {
			// TODO: Show a warning saying that part of the textbook is missing.
			htmlStr += `<p> ${JSON.stringify(resource.payload)} </p>`;
			console.error("A part of the textbook is displayed improperly:", resource);
		}
	}
	htmlStr += `</body>`;
	return htmlStr;
}

async function getBookSectionHtml(bookCode, chapterNumber, sectionNumber, authToken) {
	const json = await getBookSection(bookCode, chapterNumber, sectionNumber, authToken);
	if (json.success == true) {
		return getBookSectionHtmlFromSectionJson(json.section);
	} else {
		throw new Error(`Bad response from getBookSection(): ${JSON.stringify(json)}`);
	}
}

/* same input as getBookSection */
async function instructorNotes(bookCode, chapterNumber, sectionNumber, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/zybook/${bookCode}/chapter/${chapterNumber}/section/${sectionNumber}/instructor_notes?auth_token=${authToken}`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* TODO: We should check if this is an empty array, and if not, throw an error!
	We don't want students to miss important information that may be here. */

/* Get an order object so you can pay for a book. Needs activity on the Stripe
	side first. */
async function orderBook(bookCode, stripeToken, authToken) {
	const response = await fetch(`https://zyserver.zybooks.com/v1/zybook/${bookCode}/order`, {
		"credentials": "omit",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"body": JSON.stringify({
			"access_key": null,
			"stripe_token": stripeToken,
			"auth_token": authToken,
		}),

		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function payForBook(bookCode, userId, stripeToken, orderId, authToken) {
	await fetch(`https://zyserver.zybooks.com/v1/zybook/${bookCode}/subscription/${userId}`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
		"referrer": "https://learn.zybooks.com/",
		"body": JSON.stringify({
			"zybook_role": "Student",
			"stripe_token": stripeToken,
			"order_id": orderId,
			"auth_token": authToken
		}),
		"method": "POST",
		"mode": "cors"
	});
}

/*
Simple instructions for buying a book (This will charge REAL MONEY to your credit card, so maybe you want to read the cost it says when you run orderBook()!):
Go to https://learn.zybooks.com/signin
Run this script.
~`const userResponse = await signin(USER_EMAIL_ADDRESS_GOES_HERE, PASSWORD_GOES_HERE);`
```
const authToken = userResponse.session.auth_token;
const userId = userResponse.user.user_id;
publicKey
```
Copy the resulting value to your clipboard, and in another tab go to https://js.stripe.com/v3/null
On that tab, run js-stripe-com.js.
~`const tokensResp1 = await tokens("CREDIT_CARD_NUMBER_GOES_HERE", "THOSE_3_NUMBERS_ON_THE_BACK_OF_YOUR_CREDIT_CARD_GO_HERE", "EXP_MONTH_IN_FORM_MM", "EXP_YEAR_IN_FORM_YY", "YOUR_5_DIGIT_ZIP_CODE_HERE", "NA", "NA", "NA", stripeJsVal, 200530, "PASTED_publicKey_VALUE_GOES_HERE");`
`tokensResp1.id`
Copy output and go back to first tab.
~`const orderResp1 = await orderBook("BOOK_CODE_GOES_HERE_MAKE_SURE_ITS_EXACTLY_CORRECT_SO_YOU_DONT_BUY_THE_WRONG_BOOK", "PASTED_tokensResp1.id_VALUE_GOES_HERE", authToken);`
Go back to second tab.
~`const tokensResp2 = await tokens("CREDIT_CARD_NUMBER_GOES_HERE", "THOSE_3_NUMBERS_ON_THE_BACK_OF_YOUR_CREDIT_CARD_GO_HERE", "EXP_MONTH_IN_FORM_MM", "EXP_YEAR_IN_FORM_YY", "YOUR_5_DIGIT_ZIP_CODE_HERE", "NA", "NA", "NA", stripeJsVal, 301837, "PASTED_publicKey_VALUE_GOES_HERE");`
`tokensResp2.id`
Copy output and go back to first tab.
~`const orderResp2 = await orderBook("BOOK_CODE_GOES_HERE_MAKE_SURE_ITS_EXACTLY_CORRECT_SO_YOU_DONT_BUY_THE_WRONG_BOOK", "PASTED_tokensResp2.id_VALUE_GOES_HERE", authToken);
~`let payForBookResp = payForBook("BOOK_CODE_GOES_HERE_MAKE_SURE_ITS_EXACTLY_CORRECT_SO_YOU_DONT_BUY_THE_WRONG_BOOK", userId, orderResp2.order.id, authToken);`
*/

async function main() {
	
}
