/*
connecteam
https://app.connecteam.com/***
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

async function getConversationsListJson() {
	const path = "/api/Chat/Conversations/";
	const reponse = await fetch(path, {
		"credentials": "include",
		"headers": {
		    ":authority": "app.connecteam.com",
		    ":method": "GET",
		    ":path": path,
		    ":scheme": "https",
		    "accept": "application/json, text/plain, */*",
		    "accept-language": "en-US,en;q=0.9",
		    "cache-control": "no-cache",
		    "content-type": "application/json",
		    "pragma": "no-cache",
		    "priority": "u=1, i",
		    "sec-ch-ua": "\"Not)A;Brand\";v=\"99\", \"Microsoft Edge\";v=\"127\", \"Chromium\";v=\"127\"",
		    "sec-ch-ua-mobile": "?0",
		    "sec-ch-ua-platform": "\"Windows\"",
		    "sec-fetch-dest": "empty",
		    "sec-fetch-mode": "cors",
		    "sec-fetch-site": "same-origin",
//		    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0"
		},
		"referrer": "https://app.connecteam.com/index.html",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

async function getConversationJson() {
	const path = `/api/Chat/Conversation/?conversationId=${redacted1}_all_users`;
	const response = await fetch(path, {
		"credentials": "include",
		"headers": {
		    ":authority": "app.connecteam.com",
		    ":method": "GET",
		    ":path": path,
		    ":scheme": "https",
		    "accept": "application/json, text/plain, */*",
		    "accept-language": "en-US,en;q=0.9",
		    "cache-control": "no-cache",
		    "content-type": "application/json",
		    "pragma": "no-cache",
		    "priority": "u=1, i",
		    "sec-ch-ua": "\"Not)A;Brand\";v=\"99\", \"Microsoft Edge\";v=\"127\", \"Chromium\";v=\"127\"",
		    "sec-ch-ua-mobile": "?0",
		    "sec-ch-ua-platform": "\"Windows\"",
		    "sec-fetch-dest": "empty",
		    "sec-fetch-mode": "cors",
		    "sec-fetch-site": "same-origin",
		    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0"
		},
//		"referrer": "https://app.connecteam.com/index.html",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson();
}

async function getMessagesJson() {
	const path = `/api/Chat/Messages/?conversationId=${redacted1}_all_users&amountOfMessagesBeforeTimestamp=50&startTimestamp=1723498239351`
	const reponse = await fetch(path, {
		"credentials": "include",
		"headers": {
		    ":authority": "app.connecteam.com",
		    ":method": "GET",
		    ":path": path,
		    ":scheme": "https",
		    "accept": "application/json, text/plain, */*",
		    "accept-language": "en-US,en;q=0.9",
		    "cache-control": "no-cache",
		    "content-type": "application/json",
		    "pragma": "no-cache",
		    "priority": "u=1, i",
		    "sec-ch-ua": "\"Not)A;Brand\";v=\"99\", \"Microsoft Edge\";v=\"127\", \"Chromium\";v=\"127\"",
		    "sec-ch-ua-mobile": "?0",
		    "sec-ch-ua-platform": "\"Windows\"",
		    "sec-fetch-dest": "empty",
		    "sec-fetch-mode": "cors",
		    "sec-fetch-site": "same-origin",
//		    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0"
		},
		"referrer": "https://app.connecteam.com/index.html",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
