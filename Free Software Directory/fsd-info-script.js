// SPDX-FileCopyrightText: 2022 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// run this at https://directory.fsf.org/wiki?title=Special:RecentChanges&hideminor=1&limit=500
	// remove "&hideminor=1" if you want to include minor edits as well

const maxChars = 40; // set to null to get all chars

async function getPage(link) {
	const fetchResponse = await fetch(link);
	const pageHTML = await fetchResponse.text();
	const parser = new DOMParser();
	const alt_doc = parser.parseFromString(pageHTML, "text/html");
	return alt_doc;
}

function getShortDescriptionFromEditPage(doc) {
	return doc.querySelector("input[name='Entry[Short description]']").value;
}

function getDescriptionFromEditPage(doc, numChars) {
	if (!numChars) {
		return doc.querySelector("textarea[name='Entry[Full description]']").value;
	} else {
		return doc.querySelector("textarea[name='Entry[Full description]']").value.substring(0, numChars);
	}
}

// dead code
function getShortDescriptionFromPage(alt_doc) {
	if (alt_doc.getElementById("Overview")) {
		return alt_doc.getElementById("Overview").querySelector(".left-float").querySelectorAll("p")[0].innerText.substring(0,40);
	} else {
		console.log("no description found for", alt_doc.title);
		return "(no description found)";
	}
}

// dead code
function getFirst40CharsOfDescriptionFromPage(alt_doc) { // TODO: make the number of chars to return configurable (and change the function name to refect that)
	if (alt_doc.getElementById("Overview")) {
		return alt_doc.getElementById("Overview").querySelector(".left-float").querySelectorAll("p")[2].innerText.substring(0,40);
	} else {
		console.log("no description found for", alt_doc.title);
		return "(no description found)";
	}
}

// get the "Edit with form" link from a document
function getEditWithFormLink(alt_doc) {
	if (alt_doc.getElementById("ca-formedit")) {
		return alt_doc.getElementById("ca-formedit").querySelector("a").href;
	} else {
		return null;
	}
}

(async function() {
block = "";
for (const title of document.getElementsByClassName("mw-changeslist-title")) {
  let row = "";
	if (title.parentElement.parentElement.parentElement.querySelector(".newpage")) {
		row += "(New!) ";
	} else {
		//continue; // if you want to only show new pages
		row += "(updated) ";
	}
	row += `[${title.title}](${title.href})`;
	const alt_doc = await getPage(title.href);
	const editWithFormLink = getEditWithFormLink(alt_doc);
	if (editWithFormLink) {
		const alt_doc_edit = await getPage(editWithFormLink);
		const shortDescPart = getShortDescriptionFromEditPage(alt_doc_edit);
		const longDescPart = getDescriptionFromEditPage(alt_doc_edit, maxChars);
		row += `, ${shortDescPart}	,	${longDescPart}`;
	}
	// add summary to row
	if (title.parentElement.parentElement.querySelector(".comment")) {
		const summaryTextWithParentheses = title.parentElement.parentElement.querySelector(".comment").innerText;
		row += ` ${summaryTextWithParentheses}`;
	}
	console.log("(debug) row generated: ", row);
	block += row;
	block += "\n";
}
// TODO: final result should look something like `[Ccache](https://directory.fsf.org/wiki/Ccache), a compiler cache.`
console.info(block);
})();
