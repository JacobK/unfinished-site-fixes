/*
tetrio-lazyload
https://tetr.io/about/acknowledgements/
*/

async function getPackages() {
	const response = await fetch("https://tetr.io/api/server/packages", {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0",
		    "Accept": "application/json",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Alt-Used": "tetr.io",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin",
		    "Pragma": "no-cache",
		    "Cache-Control": "no-cache"
		},
//		"referrer": "https://tetr.io/about/acknowledgements/",
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
