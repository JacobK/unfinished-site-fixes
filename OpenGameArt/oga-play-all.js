// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

function playAll() {
	const audioElems = [...document.querySelectorAll("audio")];
	playNextRecursive(audioElems);
}

function playNextRecursive(audioElems) {
	const audioElement = audioElems.shift();
	console.log("Playing audio element:", audioElement);
	audioElement.play();
	if (audioElems.length > 0) {
		audioElement.onended = function() {
			playNextRecursive(audioElems)
		}
	}
}
// TODO: add floating button to click to play all

// TODO: show which song is playing

// TODO: update on-page play buttons automatically (probably by adding an API to the original script I wrote)
