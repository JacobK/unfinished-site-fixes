/*
weather-gov-location
https://www.weather.gov/*
e.g.
https://www.weather.gov/fwd/
*/

for (cTabs of [...document.getElementsByClassName("c-tabs")]) {
  const navArr = [...cTabs.getElementsByClassName("c-tabs-nav")[0].getElementsByClassName("c-tabs-nav__link")];
  const tabArr = [...cTabs.getElementsByClassName("c-tab")];
  for (let i = 0; i < navArr.length; i++) {
  	const ind = i;
  	navArr[ind].addEventListener("click", function() {
  		cTabs.querySelector(".c-tabs-nav__link.is-active").classList.remove("is-active");
  		cTabs.querySelector(".c-tab.is-active").classList.remove("is-active");
  		tabArr[ind].classList.add("is-active");
  		navArr[ind].classList.add("is-active");
  	});
  }
}
