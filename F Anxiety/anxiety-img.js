/*
	identifier: anxiety-img
	long name: Images at Fuck Anxiety
	description: show the images on fuckanxiety.net
	URL pattern: https://fuckanxiety.net/
*/

// TODO: support pages other than the home page

// show top navigation bar (and possibly other things)
document.querySelectorAll(".x .c1-2f").forEach(function(element){
	element.style.visibility = "visible";
});

// hide one element that seems to be just repeating the navigation bar
document.getElementById("bs-4").style.display = "none";

// allow showing the videos dropdown
document.querySelectorAll(`a[data-aid="NAV_DROPDOWN"]`).forEach(function(element){ // should only be 1 element
	element.addEventListener("click", function() {
		if (element.parentElement.parentElement.getElementsByTagName("ul")[0].style.display == "block") {
			element.parentElement.parentElement.getElementsByTagName("ul")[0].style.display = "none";
		} else {
			element.parentElement.parentElement.getElementsByTagName("ul")[0].style.display = "block";
		}
	});
});

// allow showing the login dropdown
document.querySelectorAll(`a[data-aid="MEMBERSHIP_ICON_DESKTOP_RENDERED"]`).forEach(function(element){
	if (element.parentElement.parentElement.classList.contains("membership-icon-logged-out")) {
		element.addEventListener("click", function(){
			if (element.parentElement.parentElement.parentElement.children[2].getElementsByClassName("membership-sign-out")[0].style.display == "block") {
				element.parentElement.parentElement.parentElement.children[2].getElementsByClassName("membership-sign-out")[0].style.display = "none";
			} else {
				element.parentElement.parentElement.parentElement.children[2].getElementsByClassName("membership-sign-out")[0].style.display = "block";
			}
		});
	} else if (element.parentElement.parentElement.classList.contains("membership-icon-logged-in")) {
		element.addEventListener("click", function(){
			if (element.parentElement.parentElement.parentElement.children[2].getElementsByClassName("membership-sign-in")[0].style.display == "block") {
				element.parentElement.parentElement.parentElement.children[2].getElementsByClassName("membership-sign-in")[0].style.display = "none";
			} else {
				element.parentElement.parentElement.parentElement.children[2].getElementsByClassName("membership-sign-in")[0].style.display = "block";
			}
		});
	}
});

/* show lazy load images, always using the final img element and not any of the
	source elements */ // TODO: figure out how or why to use source elements
[...document.getElementsByTagName("img")].forEach(function(element){
  if (element.getAttribute("data-lazyimg") == "true") { // if lazy image
    element.src = element.getAttribute("data-srclazy"); // set the img source
		element.removeAttribute("srcset"); /* remove "srcset" which takes
			priority over "src" apparently */
		if (element.parentElement.tagName == "PICTURE") {
			[...element.parentElement.getElementsByTagName("source")].forEach(function(parentPicture){
				parentPicture.remove();
			});
		}
  }
});

// TODO: allow clicking "Show More" to see more images

// TODO: allow using the chat box
