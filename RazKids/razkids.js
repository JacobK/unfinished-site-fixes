/*
razkids
e.g.
https://accounts.learninga-z.com/ng/member/login?siteAbbr=rk
*/

const urlParams = new URLSearchParams(window.location.search); // from https://stackoverflow.com/a/901144

async function login(username, password, rememberMe = false) {
	const response = await fetch("https://accounts.learninga-z.com/ng/api/accounts/member/login", {
		"headers": {
			"content-type": "application/json"
		},
		"method": "POST",
		"body": JSON.stringify({
			"username": username,
			"password": password,
			"siteAbbr": urlParams.get("siteAbbr"),
			"rememberMe": rememberMe,
			"redirectUri": urlParams.get("redirectUri"),
			"webPack": null
		})
	});
	const responseJson = await response.json();
	return responseJson;
}

async function students() {
	const response = await fetch("https://www.kidsa-z.com/main/students", {
		"credentials": "include",
		"headers": {
		    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
		    "Accept-Language": "en-US,en;q=0.9",
		    "Sec-Fetch-Dest": "document",
		    "Sec-Fetch-Mode": "navigate",
		    "Sec-Fetch-Site": "cross-site",
		    "Sec-Fetch-User": "?1",
		    "Upgrade-Insecure-Requests": "1",
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
//		    "sec-ch-ua": "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Google Chrome\";v=\"116\"",
//		    "sec-ch-ua-mobile": "?0",
//		    "sec-ch-ua-platform": "\"Windows\""
		},
		"referrer": "https://www.raz-kids.com/",
		"method": "GET",
		"mode": "cors"
	});
	const responseHtml = await response.text();
	return responseHtml;
}
