/*
accounts-learningaz-com-login
https://accounts.learninga-z.com/ng/member/login
*/

const urlParams = new URLSearchParams(window.location.search); // from https://stackoverflow.com/a/901144

async function login(username, password, rememberMe = false) {
	const response = await fetch("https://accounts.learninga-z.com/ng/api/accounts/member/login", {
		"headers": {
			"content-type": "application/json"
		},
		"method": "POST",
		"body": JSON.stringify({
			"username": username,
			"password": password,
			"siteAbbr": urlParams.get("siteAbbr"),
			"rememberMe": rememberMe,
			"redirectUri": urlParams.get("redirectUri"),
			"webPack": null
		})
	});
	const responseJson = await response.json();
	return responseJson;
}

async function loginAndRedirect(username, password, rememberMe = false) {
	if (!username || !password) {
		alert("The username or password field is blank!");
		return;
	}
	const loginJson = await login(username, password, rememberMe);
	if (loginJson.federateUrl) {
		document.location.assign(loginJson.federateUrl);
	} else {
		alert("unknown error, see network log");
	}
}

/* Replace current page contents. */
const replacement_html = `\
<!DOCTYPE html>
<html>
<body>
username: <input id="usernameInput" type="text" placeholder="username"><br>
password: <input id="passwordInput" type="password" placeholder="password"><br>
remember me: <input id="rememberMeInput" type="checkbox"><br>
<button id="loginButton">login</button>
</body>
</html>
`;

// a few lines of code from app-box-com-fix (C) Wojtek Kosior 2022
/*
 * We could instead use document.write(), but browser's debugging tools would
 * not see the new page contents.
 */
const parser = new DOMParser();
const alt_doc = parser.parseFromString(replacement_html, "text/html");
document.documentElement.replaceWith(alt_doc.documentElement);

document.getElementById("loginButton").addEventListener("click", function() {
	loginAndRedirect(document.getElementById("usernameInput").value, document.getElementById("passwordInput").value, document.getElementById("rememberMeInput").checked);
});
