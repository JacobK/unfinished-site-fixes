/*
www-kidsaz-com-students
https://www.kidsa-z.com/main/students
*/

// TODO: Remove dependency on semicolon
function getStudentsArray() {
	for (script of document.scripts) {
		const matchArr = script.textContent.match(/var students = (\[[^;]*);/);
		if (matchArr) {
			return JSON.parse(matchArr[1]);
		}
	}
}
