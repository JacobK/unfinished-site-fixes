/*
	identifier: insynchcs-login
	long name: Login for InSynchcs
	description: login to InSynchcs with free software
	URL patterns:
		https://*.insynchcs.com/Login/***
		https://*.insynchcs.com/
*/

/* URL looks more like https://*.insynchcs.com/Login/Login?username=*
or https://nextstepswwintouch.insynchcs.com/Login
apparently https://nextstepswwintouch.insynchcs.com/Login/Login/Login/ exists as
well
	Unfortunately it seems all *.insynchcs.com do not have the same style login
	form. For example on scribe.insynchcs.com the sign in button has id
	"loginBtn" instead of "btnSignIn". But some of them seem to be the same,
	like https://intouch1.insynchcs.com/ for example. In both cases, the text
	of the button is "SIGN IN", there may be sites that don't follow that
	convention. I suppose the internal layout of the site after one logs in,
	may also be different in some cases. */

const practice = document.domain.split(".")[0];

let lastResponseJson = null; // for debugging

function userLogin(username, password) {
	fetch(`https://${practice}.insynchcs.com/Login/userLogin`, {
		"credentials": "include",
		"headers": {
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": "UserName="+encodeURIComponent(username)+"&Password="+encodeURIComponent(password),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response){
		const responseJson = await response.json();
		lastResponseJson = responseJson;
		if (responseJson.Status == 1) { // 1 is success, 0 is fail
			location.href = responseJson.url; // looks like https://*.insynchcs.com/WarningMessage?WinName=*
		} // TODO: give better feedback when entering a wrong password or something
	});
}


document.getElementById("btnSignIn").addEventListener("click", function(){
	userLogin(document.getElementById("txtUserName").value, document.getElementById("txtPwd").value);
});

// TODO: allow submitting by pressing enter too
