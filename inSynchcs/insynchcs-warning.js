/*
	identifier: insynchcs-warning
	long name: Warning Advance for InSynchcs
	description: advance the warning that appears when logging in to InSynchcs
	URL pattern: https://*.insynchcs.com/WarningMessage
*/

// show the warning
document.getElementById("popup_warning_msg").className = "modal show";
