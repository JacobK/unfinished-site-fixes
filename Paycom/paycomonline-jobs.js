/*
paycomonline-jobs
https://www.paycomonline.net/v4/ats/web.php/jobs
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

function getJobsJson() {
	for (script of document.scripts) {
	  if (script.textContent.match(/var jobs = /)) {
		return JSON.parse(script.textContent.match(/var jobs = (\[.*\])/)[1])
	  }
	}
}

resultsElem = document.getElementById("results");

for (job of getJobsJson()) {
	const jobListing = document.createElement("div");
	const titleElem = document.createElement("h4");
	const jobLink = document.createElement("a");
	jobLink.textContent = job.title;
	jobLink.href = job.url;
	titleElem.appendChild(jobLink);
	jobListing.appendChild(titleElem);
	resultsElem.appendChild(jobListing);
}
