/*
lesswrong-readmore
e.g.
https://www.lesswrong.com/tag/risks-of-astronomical-suffering-s-risks
*/

function getApolloStateJson() {
	for (script of document.scripts) {
	  const matchArr = script.textContent.match(/window.__APOLLO_STATE__ = (.*)/);
		if (matchArr) {
		return JSON.parse(matchArr[1]);
	  }
	}
}

const apolloStateJson = getApolloStateJson();

// document.write(apolloStateJson["Revision:nzvHaqwdXtvWkbonG_description"].html);
