/*
watermark-expand
https://www.watermark.org/***
e.g.
https://www.watermark.org/membership
https://www.watermark.org/uncommon-parenting
*/

for (const expandButton of document.querySelectorAll("a[data-toggle='collapse'], a[data-toggle='dropdown']")) {
	console.log("(debug)", expandButton);
	expandButton.parentElement.addEventListener("click", function(){
		let target = null;
		if (expandButton.href) {
			target = document.querySelector(`#${expandButton.href.split("#")[1]}`);
		} else if (expandButton.getAttribute("data-target")) {
			target = document.querySelector(expandButton.getAttribute("data-target"));
		} else if (expandButton.getAttribute("data-toggle") == "dropdown") {
			target = expandButton.parentElement.querySelector("div.dropdown-menu");
		}
		if (target) {
			if (!target.classList.contains("show")) {
				target.classList.add("show");
			} else {
				target.classList.remove("show");
			}
		}
	})
}
