/*
starrezhousing-com-redirectlogin
https://*.starrezhousing.com/StarRezPortalX/RedirectLogin/*
e.g. https://utdallas.starrezhousing.com/StarRezPortalX/RedirectLogin/StarNet.Core.AuthProviders.Shibboleth
*/

for (script of document.scripts) {
  const matchArr = script.textContent.match(/window.location.href = '([^']*)';/);
	if (matchArr) {
    document.location.assign(matchArr[1]);
  }
}
