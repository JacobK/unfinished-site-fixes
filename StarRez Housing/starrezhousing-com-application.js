/*
starrezhousing-com-application
https://*.starrezhousing.com/StarRezPortalX/**
e.g.
https://utdallas.starrezhousing.com/StarRezPortalX/5F94E093/14/75/Housing_Application-Term_Selector?UrlToken=67DCFA14
https://utdallas.starrezhousing.com/StarRezPortalX/481F7A26/14/77/Housing_Application-Welcome?UrlToken=67DCFA14&TermID=48&ClassificationID=4
https://utdallas.starrezhousing.com/StarRezPortalX/A943C405/14/119/Housing_Application-Sign_Up_Information?UrlToken=67DCFA14&TermID=48&ClassificationID=4
https://utdallas.starrezhousing.com/StarRezPortalX/A7FEECAB/14/114/Housing_Application-Intent_for_Summer?UrlToken=67DCFA14&TermID=48&ClassificationID=4
https://utdallas.starrezhousing.com/StarRezPortalX/877E9DD8/14/121/Housing_Application-Housing_Details?UrlToken=67DCFA14&TermID=48&ClassificationID=4
https://utdallas.starrezhousing.com/StarRezPortalX/7212FF7B/14/448/Housing_Application-Room_Preferences?UrlToken=67DCFA14&TermID=48&ClassificationID=4
https://utdallas.starrezhousing.com/StarRezPortalX/E5C94F5C/14/123/Housing_Application-Room_Self_Selection_?UrlToken=67DCFA14&TermID=48&ClassificationID=4
*/
/* Warning: This script downloads HTML and sets it via innerHTML values. Is
	that dangerous? */
/* Warning: This script is not well-tested. The data the client sends may not
	correspond to the information you wanted to convey, in some cases. Checking
	that a request was the right one may involve checking the HTML. */
/* This script has been tested on all of the example URLs (The room self
	selection page was visited before room self selection was available.), but
	with almost every new page I visited I needed to change the code at least
	slightly, so expecting this script to work for new pages that are not
	simply a click-through page is maybe too hopeful. Since many of the client
	inputs are numerical IDs, submitting completely different data from what
	the user intends to submit may be likely. Proceed with caution.
		I suspect document.URL.split("/")[4] is some sort of checksum on the
	URL, but it's calculated server-side so we probably don't need to worry
	about it too much. Though, figuring out how to generate the hash may
	(unlikely) make exhaustively testing every page more possible. */

/* termId should be a string consisting of a number. This method is used to
	enter the application and to advance to the next page, and json specifies
	the user's input to the previous page. There's always a "PageWidgetData"
	array but there are also a bunch of values needed in a way that doesn't
	seem to follow any pattern, unfortunately. */
async function getNextUrl(verificationToken, json) {
	const response = await fetch(document.URL, {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "*/*",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json; charset=utf-8",
			"__RequestVerificationToken": verificationToken,
			"X-Requested-With": "XMLHttpRequest",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		// Referrer will be set automatically by the browser.
		"body": JSON.stringify(json),
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json(); /* The JSON is a single string,
		not contained in brackets. */
//	const matchArr = responseText.match(/"([^"]*)"/);
//	return matchArr[1];
	if (typeof(responseJson) == "string") {
		return responseJson;
	} else {
		// then it's not a string! Throw an error!
		console.error("Unexpected JSON: ", responseJson);
		throw new Error("Unexpected JSON");
	}
	
}
/* The returnvalue is a URL to navigate to for the next step of the application. */

/* This is the method used to get into the housing application. */
async function navToApp(verificationToken, termId) {
	const url = await getNextUrl(verificationToken, termId);
	document.location.assign(url);
}
/* This function gets the widget data array for the current page, which may
	include some user input. */
function getWidgetData() {
	let pageWidgetData = [];
	for (widgetContainer of document.querySelectorAll("div.widget-container")) {
	  let widget = {};
		const widgetId = widgetContainer.getAttribute("data-portalpagewidgetid");
		widget.Key = widgetId;
		widget.Value = [];
		for (dataIdLi of widgetContainer.querySelectorAll("li[data-id]")) {
			let widgetValue = {};
			widgetValue.Key = `${dataIdLi.getAttribute("data-id")}_${dataIdLi.querySelector("div[data-id]").getAttribute("data-id")}_${dataIdLi.querySelector("div[data-id]").getAttribute("data-masterid")}`; // I made a request without the first underscore ('_') and everything after it, and it seemed to work okay anyway.
			if (dataIdLi.querySelector("select")) {
				widgetValue.Value = dataIdLi.querySelector("select").value;
			} else if (dataIdLi.querySelector("span")) {
				widgetValue.Value = dataIdLi.querySelector("span").innerText;
			}
			widget.Value.push(widgetValue);
		};
		for (dataInput of widgetContainer.querySelectorAll("input")) {
			let widgetValue = {};
			widgetValue.key = dataInput.name;
			widgetValue.Value = dataInput.value;
			widget.Value.push(widgetValue);
		}
		if (widget.Value.length != 0) {
			pageWidgetData.push(widget);
		}
	}
	return pageWidgetData;
}

/* This function gets a URL that I think is used for telemetry. Currently it
	is not called in main. */
function getSentryDsnUrl() {
	let key = null;
	for (script of document.scripts) {
		const matchArr = script.textContent.match(/dsn: "([^"]*)",/);
		if (matchArr) {
			return matchArr[1];
		}
	}
}

/* This function gets a public key that I think is used for telemetry.
	Currently it is not called in main. */
function getSentryPublicKey() {
	const url = getSentryDsnUrl();
	const matchArr = url.match(/https:\/\/([^@]*)@........ingest.sentry.io\/6717479/);
	return matchArr[1];
}

/* This method gets HTML corresponding to what the user has submit previously. */
async function getTableHtml(verificationToken, resizeBreakpoint, pageId) {
	const response = await fetch("https://utdallas.starrezhousing.com/StarRezPortalX/responsiveactivetable/GetDesktopTable", {
		"credentials": "include",
		"headers": {
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "*/*",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json; charset=utf-8",
			"__RequestVerificationToken": verificationToken,
			"X-Requested-With": "XMLHttpRequest",
//			"sentry-trace": `${redacted32charlonglowercasehex}-${redacted16charlonglowercasehex}-0`,
//			"baggage": `sentry-environment=Production,sentry-release=px%4010.9.43.12404,sentry-public_key=${getSentryPublicKey()},sentry-trace_id=${redacted32charlonglowercasehex},sentry-sample_rate=0`,
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		// Referrer will be filled in by the browser.
		"body": JSON.stringify({
			"options": {
				"ResizeBreakpoint": resizeBreakpoint
			},
			"pageID": pageId
		}),
		"method": "POST",
		"mode": "cors"
	});
	const responseHtml = await response.text();
	return responseHtml;
}

/* WARNING: This function sets HTML with untrusted data! If the data contained
	scripts, would those scripts run? */
async function displayTable(tableDiv) {
	const resizeBreakpoint = tableDiv.parentElement.getAttribute("data-resizebreakpoint");
	const pageId = Number.parseInt(document.URL.split("/")[6]);
	const tableHtml = await getTableHtml(verificationToken, resizeBreakpoint, pageId);
	tableDiv.innerHTML = tableHtml;
}

/* This function adds the user input data to the given json to be sent. */
function addTableData(json) {
	const tableDiv = document.getElementsByClassName("ui-responsive-active-table-container")[0];
	for (dataDiv of tableDiv.querySelector("tbody").querySelector("tr").querySelectorAll(`div[data-fieldname]`)) {
		json[dataDiv.getAttribute("data-fieldname")] = dataDiv.querySelector("select").value;
	}
	json["SelectedPreferences"] = [];
	for (tableRow of tableDiv.querySelector("tbody").querySelectorAll("tr")) {
		json["SelectedPreferences"].push(tableRow.querySelector(`div[data-fieldname="PreferenceDropdown"]`).querySelector("select").value);
	}
	return json;
}

/* This method makes it so when a dropdown value is change the change is
	reflected in the visible part of the HTML. */
function makeSelectElemsDynamic() {
	for (let selectElem of document.querySelectorAll("select")) {
		selectElem.addEventListener("change", function() {
			selectElem.parentElement.querySelector("div.ui-dropdown-container-caption").innerText = selectElem.querySelector(`option[value="${selectElem.value}"]`).innerText;
		});
	}
}

// function main() {
const verificationToken = document.querySelector(`input[name="__RequestVerificationToken"]`).value;
//const termId = document.querySelector(`input[name="TermID"]`).value; /* This is not reliable for some reason. Sometimes the value is "0". */
//document.querySelector(`li[data-termid="${termId}"]`).querySelector("button").addEventListener("click", navToApp);

/* This first for loop is only useful on
	https://*.starrezhousing.com/StarRezPortalX/5F94E093/14/75/Housing_Application-Term_Selector */
for (term of document.querySelectorAll(`li[data-termid]`)) {
	term.querySelector("button").addEventListener("click", function() {
		navToApp(verificationToken, {
			"PageWidgetData": getWidgetData(), // Expected to be []
			"TermID": term.getAttribute("data-termid")
		});
	});
}

/* This is meant to activate buttons to advance the application pages. */
if (document.querySelector("div.page-actions").querySelector("button")) {
	document.querySelector("div.page-actions").querySelector("button").addEventListener("click", function() {
		let json = {
			"PageWidgetData": getWidgetData()
		};
		if (document.getElementsByClassName("ui-responsive-active-table-container").length == 1) { // for room selection
			json = addTableData(json);
		}
		// TODO: make sure these next 2 checks don't break on pages other than Housing_Application-Roommate_Group_Join
		if (document.querySelector(`input[name="GroupName"]`)) { // for roommate selection
			json.GroupName = document.querySelector(`input[name="GroupName"]`).value;
		}
		if (document.querySelector(`input[name="Password"]`)) { // for roommate selection
			json.Password = document.querySelector(`input[name="Password"]`).value;
		}
		navToApp(verificationToken, json);
	});
}

if (document.getElementsByClassName("ui-responsive-active-table-container").length == 0) {
	makeSelectElemsDynamic();
} // else makeSelectElemsDynamic() will be called by the below for loop

// This displays the table if there is supposed to be one there.
// TODO: Allow adding new rows to the table by clicking "ADD PREFERENCE".
for (tableDiv of document.getElementsByClassName("ui-responsive-active-table-container")) {
	displayTable(tableDiv).then(makeSelectElemsDynamic);
}
// } // end main()