/*


e.g. https://www.dart.org/guide/transit-and-use/bus-routes/bus-route-detail/bus-route-238-naaman-forest
*/

document.getElementById("collapse").style.display = "revert";

for (linkElem of document.querySelectorAll(".routeInfoButton")) {
	const matchArr = linkElem.getAttribute("blocked-onclick").match(/openDownloadUrl\('([^']*)'/);
	if (matchArr) {
		linkElem.addEventListener("click", function() {
			document.location.assign(matchArr[1]);
		});
	}
}
