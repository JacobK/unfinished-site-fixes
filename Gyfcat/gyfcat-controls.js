/*
	identifier: gyfcat-controls
	long name: Controls for Gyfcat
	description: show controls on Gyfcat videos
	URL pattern: https://gfycat.com/*
*/

// note: currently this script breaks the page if loaded immediately by Haketilo, but it doesn't break the page if loaded after the fact (also you can just right click lol)


// show HTML video controls on the main video
document.getElementsByClassName("video media")[0].controls = true;
