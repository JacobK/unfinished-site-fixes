/*
	identifier: giga-fix
	long name: Search for Gigablast
	description: allows searching with Gigablast using local scripts
	URL pattern: https://www.gigablast.com/search
*/

/* WARNING: This script sets innerHTML variables with data sent by Gigablast's
	servers, which *may* allow the server operator to run arbitrary software on
	your computer! */ /* TODO: double check this, I think Haketilo is supposed
	to block it; I get 2000+ errors about blocked scripts, so I think Haketilo
	is blocking scripts...??? */

/* This script contains the verbatim text of 3 scripts from
	https://www.gigablast.com/search as well as a modified version of the onload
	function that grabs data from the official onload function. The scripts
	might be free software here (but I have not checked):
	https://github.com/gigablast/open-source-search-engine/tree/master/html */

// first script (copied verbatim)
var openmenu=''; var inmenuclick=0;var lasth=null;function x(){document.f.q.focus();}

// second script (copied verbatim)
function show(id){var e = document.getElementById(id);if ( e.style.display == 'none' ){e.style.display = '';}else {e.style.display = 'none';}}

// third script (copied verbatim)
function handler() {
if(this.readyState == 4 ) {
document.getElementById('bodycont').innerHTML=this.responseText;
}}
function ccc ( gn ) {
var e = document.getElementById('fd'+gn);
var f = document.getElementById('sd'+gn);
if ( e.style.display == 'none' ){
e.style.display = '';
f.style.display = 'none';
}
else {
e.style.display = 'none';
f.style.display = '';
}
}

/* get some variables from the official onload script, since they may be
	different each time */
let uxrl = null;
// regex help from here: https://stackoverflow.com/questions/375104/how-can-i-match-a-quote-delimited-string-with-a-regex
// My understanding of the regex is that it's matching `uxrl='` and then grabbing a character that's not a quote (^ = not I think) an infinite (*) amount of times (as long as it's possible), and then it matches `'` outside of the text that's grabbed to be stored in match.
let match = /uxrl='([^']*)'/.exec(document.body.getAttribute("blocked-onload")); // looks for "uxrl=" in the onload text and then grabs the quoted text after that.
if (match) {
	uxrl = match[1];
}
let efs = null;
match = /efs=([^']*)'/.exec(document.body.getAttribute("blocked-onload")); // looks for "efs=" in the onload text and then grabs the text until a quote
if (match) {
	efs = match[1];
}
uxrl = uxrl + 'efs=' + efs;


// onload
var client = new XMLHttpRequest();
client.onreadystatechange = handler;
//var uxrl='/search?c=main&qlangcountry=en-us&q=free+software&fromjs=1&rand=1653325954117&v';
//uxrl=uxrl+'efs=538909665'; // seems to always be the same, but I haven't investigated closely
client.open('GET', uxrl );
client.send();

// TODO: make it so the dropdowns and stuff work
