/*
skribbl-io
https://skribbl.io/
*/

// Depends on Socket.IO: https://cdn.socket.io/4.6.0/socket.io.js

/* See <https://gist.github.com/MrDiamond64/b2081f2cb4ca6d11e848edaeb5ae1814>
	for API documentation, as well as
	<https://github.com/scribble-rs/skribbl.io-docs> */

function getGameId() {
	return document.URL.split("?")[1];
}

// gameId is a string
async function login(gameId) {
	let bodyStr = "";
	if (gameId) {
		bodyStr = `id=${gameId}`;
	} else {
		bodyStr = "lang=0"; // TODO: support other languages
	}
	const response = await fetch("https://skribbl.io:3000/play", {
		"credentials": "omit",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/119.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-type": "application/x-www-form-urlencoded",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site"
		},
	//    "referrer": "https://skribbl.io/",
		"body": bodyStr,
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	return responseText;
}

function getToolInt(toolString) {
	if (toolString == "pencil") {
		return 0;
	} else if (toolString == "eraser") {
		return 1;
	} else if (toolString == "fill") {
		return 2;
	}
}

function getDrawDataJson(toolString) {
	return {"id":19,"data":[[getToolInt(toolString),1,24,343,392,343,392]]}
}

class Avatar {
	constructor(skinColorString, eyesString, mouthString, specialString) {
		this.skinColor = this.getSkinColorInt(skinColorString);
		this.eyes = 9;
		this.mouth = 16;
		this.special = -1;
	}
	
	getSkinColorInt(skinColorString) {
		if (typeof skinColorString == "number") {
			return skinColorString;
		} else if (skinColorString.toLowerCase() == "red") {
			return 0;
		} else {
			return 4;
		}
	}
	
	getSkinColorString(skinColorInt) {
		switch (skinColorInt) {
			case 0:
				return "Red";
			case 1:
				return "Orange";
		}
	}
	
	get arr() {
		return [this.skinColor, this.eyes, this.mouth, this.special];
	}
}

function getLoginJson(gameId = 0, privateGame = true, playerName = "", language = "0") {
	let createId = 0;
	if (gameId == 0 && privateGame == true) {
		createId = 1;
	}
	return {"join":gameId,"create":createId,"name":playerName,"lang":language,"avatar":[22,16,25,-1]};
}

function sendChatMessage(message) {
	socket.emit("chat", message);
}

function startGame() {
	socket.emit("lobbyGameStart");
}

//async function main() {
	const wsDomain = await login(getGameId());
	const socket = io(wsDomain);
	socket.emit("login", getLoginJson());
	
	socket.onAny((eventName, ...args) => { // copied from <https://socket.io/docs/v4/tutorial/api-overview>
	  console.log(eventName);
	  console.log(args);
	});
//}
