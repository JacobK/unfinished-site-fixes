/*
simplex-chat-contact
https://simplex.chat/contact
*/

/* We really ought to just use the page's scripts, since they are free software
	already and they don't seem too complicated. */

document.getElementById("conn_req_uri_text").textContent = `/c ${document.URL}`;
for (elem of document.querySelectorAll(".contact-tab-content")) {
	elem.style.opacity = 1;
	elem.style["max-height"] = "none";
	elem.style.visibility = "visible";
}
