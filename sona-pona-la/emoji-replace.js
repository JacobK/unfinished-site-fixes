/*
emoji-replace
https://sona.pona.la/wiki/nasin_pi_sitelen_jelo
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

// utility script, not replacement

for (elem of document.querySelectorAll("bdi")) {
	if (/\p{Emoji}/u.test(elem.innerText)) { // Thanks to: https://stackoverflow.com/a/64007175
		console.log(elem.innerText);
	}
}

// Goal: make the hover text say the proper name of the emoji instead of "Toki Pona"
