/*
starbucks-wifi
https://sbux-portal.globalreachtech.com/***
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* I think this script works but by the time I got around to testing it the
	store Wi-Fi was closed. */

if (document.URL.split('/')[3].split('?')[0] == "") {
	document.location.href = document.URL.replace('?',"check?"); // hacky but works?
} else if (document.URL.split('/')[3].split('?')[0] == "signup") { // /check redirects to /signup
	if (document.getElementById("apForm")) {
		document.getElementById("apForm").submit();
	}
}
