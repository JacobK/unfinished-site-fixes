/*
clientes-nic-cl
https://clientes.nic.cl/***
*/

const helpMatches = [];

async function main() {
	const helpJsResponse = await fetch("https://clientes.nic.cl/registrar/js/help-min.js");
	const helpJsResponseText = await helpJsResponse.text();
//	const matches = helpJsResponseText.matchAll(/if\(Dom.hasClass\(a,"([^"]*)"\)\){b="([^"]*)"/g);
	const matches = helpJsResponseText.matchAll(/Dom.hasClass\(a,"([^"]*)"\)[^{]*{b="([^"]*)"/g); // still doesn't get topic_31
	let currentMatch;
	while (currentMatch = matches.next()) {
		helpMatches.push(currentMatch); // debug
		if (currentMatch.done == true) {
			break;
		}
		for (helpButtonElem of [...document.getElementsByClassName(currentMatch.value[1])]) {
			const helpText = currentMatch.value[2];
			helpButtonElem.addEventListener("click", function() {
				alert(helpText);
			});
		}
	}
	return helpJsResponseText;
}
a = main();

// TODO: 31
