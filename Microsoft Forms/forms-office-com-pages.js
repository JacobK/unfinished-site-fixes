/*
forms-office-com
https://forms.office.com/pages/responsepage.aspx
https://forms.office.com/Pages/ResponsePage.aspx
*/

// From https://stackoverflow.com/a/15724300 , by kirlich (https://stackoverflow.com/users/7635/kirlich) and Arseniy-II (https://stackoverflow.com/users/8163773/arseniy-ii), licensed CC BY-SA 4.0
function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

// Get the URL used to get the form.
function getFormDataUrl() {
	let url = null;
	for (script of document.scripts) {
		const matchArr = script.textContent.match(/"prefetchFormWithResponsesUrl":"([^"]*)"/);
		if (matchArr) {
			url = matchArr[1];
		}
	}
	url = JSON.parse('"'+url+'"'); /* In the past, all forms didn't need this
		line, but the website changed so now they do. The first time I noticed
		the break was on 2023-04-13. */
	return url;
}

/* The following function is based on code from
	docs-google-com-fix-sheets-display by Wojtek Kosior <koszko@koszko.org>
	and a Stack Overflow answer <https://stackoverflow.com/a/7053514> by
	user113716 <https://stackoverflow.com/users/113716/user113716>.
		This function gets a JSON object from an inline script, and the input
		is the JSON variable you are looking for. */
function getJsonFromScript(variableName) {
	let jsonObj = null;
	const regEx = new RegExp(`${variableName} = ({([^;]|[^}];)+})`); /* Will
		regex search for the variable name followed by " = " and then JSON. */
	for (const script of document.scripts) {
		const matchArr = script.textContent.match(regEx);
		if (!matchArr)
		continue;
		jsonObj = JSON.parse(matchArr[1]);
	}
	return jsonObj;
}

// Get the session ID from an inline script.
function getSessionId() {
	const serverInfoJson = getJsonFromScript("window.OfficeFormServerInfo");
	return serverInfoJson.serverSessionId; // This line was added on 2023-07-04, at the same time that I'm fixing other problems. serverInfoJson.serverAttributes doesn't exist anymore apparently.
	if (serverInfoJson.serverAttributes.SessionId == serverInfoJson.serverSessionId) {
		/* getJsonFromScript("window.OfficeFormServerInfo.serverInfoFromPageHeaders").serverSesionId
			should also be equal but considering the mispelling I decided not
			to check it. */
		return serverInfoJson.serverSessionId;
	} else {
		throw new Error("The session IDs do not match!");
	}
}

// Get the form with questions and answers.
async function getForm(formDataUrl, sessionId, verificationToken) {
	const response = await fetch(formDataUrl, {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "*/*",
			"Accept-Language": "en-US,en;q=0.5",
			"Content-Type": "application/json",
			"X-UserSessionId": sessionId,
			"__RequestVerificationToken": verificationToken,
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		// Referrer should be filled automatically.
		"method": "GET",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}
/* If you get a 401 Unauhorized response with error code 701, that means you
	need to log in first. */

function renderQuestion(question) {
	// TODO: Show when an "other" option is available for a multiple choice question
	// TODO: give better warnings when questions aren't rendered properly
	/* TODO: fix order of questions; I can't seem to figure out how to get the
		order right, but luckily for
		the surveys I need to do it doesn't matter. */
	const questionInfo = JSON.parse(question.questionInfo);
	// TODO: also show descriptiveQuestions
	const questionDiv = document.createElement("div");
	questionDiv.classList.add("question");
	// TODO: also define an attribute called questiontype or something like that
	const titleHeader = document.createElement("h3");
	titleHeader.innerText = question.title;
	questionDiv.appendChild(titleHeader);
	if (question.type == "Question.Choice" && (questionInfo.ChoiceType == 1 || questionInfo.ChoiceType == 2)) { // multiple choice question, radio or checkbox
		for (choice of questionInfo.Choices) {
			const choiceElem = document.createElement("input");
			if (questionInfo.ChoiceType == 1) {
				choiceElem.type = "radio";
			} else if (questionInfo.ChoiceType == 2) {
				choiceElem.type = "checkbox";
			} else {
				console.error(`Multiple choice question choice type ${questionInfo.ChoiceType} not implemented!`);
			}
			choiceElem.name = question.id;
			choiceElem.value = choice.Description;
			questionDiv.appendChild(choiceElem);
			const descSpan = document.createElement("span");
			descSpan.innerText = choice.Description;
			questionDiv.appendChild(descSpan);
		}
	} else if (question.type == "Question.TextField") {
		const textInputElem = document.createElement("input");
		textInputElem.name = question.id;
		questionDiv.appendChild(textInputElem);
	} else {
		console.error(`Question type ${question.type} not implemented!`);
	}
	document.getElementById("questions").appendChild(questionDiv);
}

// TODO: consider different question types more explicitly
function getAnswersArr() {
	let answersArr = [];
	for (questionDiv of document.querySelectorAll("div.question")) {
		let question = {
			"questionId": questionDiv.querySelector("input").name
		};
		let answer = null;
		let choicesArr = []; // for checkbox questions
		for (inputElem of questionDiv.querySelectorAll("input")) {
			if (inputElem.type == "radio") {
				if (inputElem.checked) {
					answer = inputElem.value;
				}
			} else if (inputElem.type == "checkbox") {
				if (inputElem.checked) {
					choicesArr.push(inputElem.value);
				}
			} else {
				answer = inputElem.value;
			}
		}
		if (answer) {
			question.answer1 = answer;
		} else if (choicesArr.length >= 1) {
			question.answer1 = JSON.stringify(choicesArr);
		}
		answersArr.push(question);
	}
	return answersArr;
}

/* Yes, the array of answers is supposed to be stringified within the
	already-stringified JSON. */
async function submit(sessionId, verificationToken, ownerTenantId, ownerId, formId, answersArr) {
	await fetch(`https://forms.office.com/formapi/api/${ownerTenantId}/users/${ownerId}/forms('${formId}')/responses`, {
		"credentials": "include",
		"headers": {
//			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
			"Accept": "application/json",
			"Accept-Language": "en-US,en;q=0.5",
			"content-type": "application/json",
			"x-correlationid": crypto.randomUUID(),
			"x-usersessionid": sessionId,
			"x-ms-form-request-source": "ms-formweb",
			"x-ms-form-request-ring": "business",
			"odata-maxverion": "4.0",
			"odata-version": "4.0",
			"__requestverificationtoken": verificationToken,
			"x-ms-form-muid": getCookie("MUID"), // If this cookie doesn't exist, try GETting "https://c.office.com/c.gif"
			"authorization": "",
			"Sec-Fetch-Dest": "empty",
			"Sec-Fetch-Mode": "cors",
			"Sec-Fetch-Site": "same-origin"
		},
		// Referrer should be filled automatically.
		"body": JSON.stringify({
		  "startDate": new Date().toISOString(), // TODO: consider reporting the actual start time, but inform the user
				"submitDate": new Date().toISOString(),
				"answers": JSON.stringify(answersArr)
		}),
		"method": "POST",
		"mode": "cors"
	});
}
// descriptiveQuestions show up in the response but shouldn't be in the request






async function main() {
	const formDataUrl = getFormDataUrl();
	const sessionId = getSessionId();
	const verificationToken = getJsonFromScript("window.OfficeFormServerInfo").antiForgeryToken;
	const formJson = await getForm(formDataUrl, sessionId, verificationToken);
	alert("Warning: This script is incomplete, and things may break in ways that cause problems for you! Like answer choices being hidden or questions marked as required not enforcing that, possibly also invisible questions! Maybe don't use this unless you're willing to spend the time looking at the network log to see if it worked the way you expected!");
	if (formJson.error && (formJson.error.code == 701 || formJson.error.code == 711)) {
		/* Redirect to getJsonFromScript("window.OfficeFormServerInfo").signInLink but change the ru
		query parameter to the current URL plus the session ID as an additional query parameter within 
		the url stored in ru. Error code 701 is a missing token when login is
		required; 711 is a bad token. Error code 500 means the form is closed.
		5003 means the form was ended (guessing automatically instead of
		manually?). */
		// The below regex assumes there will be exactly one query parameter before ru.
		const matchArr = getJsonFromScript("window.OfficeFormServerInfo").signInLink.match(/([^&]*&ru=).*/);
		/* TIL Chrome doesn't strictly enforce const in the dev console (i.e.
			you can redefine const variables if you say const again.). */
		const redirectUrl = matchArr[1] + encodeURIComponent(document.URL);
		document.location.assign(redirectUrl);
	} else if (formJson.error) {
		throw new Error(`Error: ${JSON.stringify(formJson.error)}`);
	} else {
		const startTime = new Date().toISOString(); // currently not sent; we should ask users?
		const ownerTenantId = formJson.form.ownerTenantId;
		const ownerId = formJson.form.ownerId;
		const formId = formJson.form.id;
		
		document.getElementById("content-root").innerHTML = ""; // remove the loading icon
		const questionsDiv = document.createElement("div");
		questionsDiv.id = "questions";
		document.getElementById("content-root").appendChild(questionsDiv);
		for (question of formJson.form.questions) {
			renderQuestion(question);
		}
		const submitButton = document.createElement("button");
		submitButton.innerText = "Submit";
		submitButton.addEventListener("click", function(){
			const answersArr = getAnswersArr();
			// TODO: enforce which questions are required
			submit(sessionId, verificationToken, ownerTenantId, ownerId, formId, answersArr);
			submitButton.remove();
		});
		document.getElementById("content-root").appendChild(submitButton);
		
		// Allow scrolling the page
		for (overflowElem of document.querySelectorAll("html, body, #content-root")) {
			overflowElem.style["overflow-y"] = "revert";
		}
	}
}

main();
