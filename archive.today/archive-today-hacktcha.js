/* SPDX-License-Identifier: CC0-1.0
 *
 * Copyright (C) 2022 Wojtek Kosior
 * Copyright (C) 2023 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

"use strict";

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

function getRecaptchaDiv() {
	return document.getElementById("g-recaptcha");
}

function getSiteKey() {
	for (let script of document.scripts) {
	  const regMatch = script.textContent.match(/  'sitekey': '([^']*)'/);
	  if (regMatch) {
	  	return regMatch[1];
	  }
	}
	return null;
}

async function checkCaptcha(recaptchaToken) {
	const response = await fetch(`${document.location.origin}/cdn-cgi/l/chk_captcha`, {
		"credentials": "omit",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/x-www-form-urlencoded",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": `response=${formUrlEncode(recaptchaToken)}&location=${formURlEncode(document.URL)}`,
		"method": "POST",
		"mode": "cors"
	});
	const responseText = await response.text();
	if (responseText == "{}") {
		return true;
	} else {
		return false;
	}
}

(async () => {
	const recaptcha_div = getRecaptchaDiv();

    if (recaptcha_div === null)
	return;

    recaptcha_div.style.display = "block";

    const site_key = getSiteKey();

    const token_field = document.createElement("textarea");
    token_field.name = "g-recaptcha-response";
    token_field.style.display = "none";
    recaptcha_div.before(token_field);

    let authenticated = false;
    
    for await (const token of HCHA.run(recaptcha_div, site_key)) {
	if (token === null) {
	    authenticated = false;

	    for (const button of submit_buttons)
		button.style.backgroundColor = "lightgray";
	} else {
	    token_field.value = token; // unnecessary
	    
	    authenticated = await checkCaptcha(token);
	    if (authenticated) {
	    	document.location.reload();
	    }
	}
    }
})();
