// SPDX-FileCopyrightText: 2018 saeid ezzati <https://stackoverflow.com/users/3539776/saeid-ezzati>
// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: Apache-2.0
// SPDX-License-Identifier: CC-BY-SA-4.0
// SPDX-License-Identifier: CC0-1.0
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright 2023 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


// TODO: better licensing for this file (Apache/CC licenses don't apply to most of the file, but IDK how to make that clear in the metadata. REUSE says to use separate tags <https://reuse.software/faq/#multi-licensing>.)

// TODO: handle Cloudflare-encoded emails


/*
redriver-events-intervarsity-org
https://redriver.events.intervarsity.org/*
e.g.
https://redriver.events.intervarsity.org/waco-spring-break-plunge
https://redriver.events.intervarsity.org/recweek-2023
depends on 
https://github.com/kjur/jsrsasign/archive/refs/tags/7.2.1.tar.gz 
	src/asn1-1.0.js
	ext/yahoo.js
*/
/* Warning: bugs may result in incorrect input. e.g. a failure to count properly
	could lead to input that looks like question1=answer1, question2=answer2...
	actually send question1=answer1,question2=answer3... I've not actually run
	into this problem but it seems like a strong possibility given the script's
	design. Anyway, you should consider manually looking over your JSON by
	calling getJsonPayloadFromPage() yourself and looking over it. Proper
	deposits are not supported, as the page I'm looking at seems to use fake
	ones. */
/* Notice: To figure out some things related to billing, I decompiled the
	free-licensed part of bundle.js. See reverse-engineering-notes.txt for more
	details. */
/* The IV forms use webconnex, so other sites that use webconnex may be able to
	reuse code from this script. */

const packageIdentifier = "redriver-events-intervarsity-org";

/* The following function is based on code from
	a Stack Overflow answer <https://stackoverflow.com/a/7053514> by
	user113716 <https://stackoverflow.com/users/113716/user113716>.
		This function gets a JSON object from an inline script, and the input
	is a string directly preceeding the JSON you want. So, for example, if you
	want the JSON value assigned like "varName = {...}, then the input would be
	"varName = ". */
function getJsonFromScriptString(leadingText) {
	let jsonObj = null;
	const regEx = new RegExp(`${leadingText}(.*)`); /* Will
		regex search for the leadingText value and then JSON directly after. */
	for (const script of document.scripts) {
		const matchArr = script.textContent.match(regEx);
		if (!matchArr)
		continue;
		const trailingCommaRemoved = matchArr[1].slice(0, -1); /* This seems
			likely to break if InterVarsity removes the trailing comma from the
			last object. */
		jsonObj = JSON.parse(JSON.parse(trailingCommaRemoved));
	}
	return jsonObj;
}

function getUserToken() {
	const appSettings = getJsonFromScriptString("appSettings: ");
	if (appSettings.userToken != appSettings.logger.meta.userToken) {
		console.warn(`Haketilo - ${packageIdentifier}: userToken is defined twice with 2 different values!`);
	}
	return appSettings.userToken;
}

function getBirthDateString(documentRef) {
	let monthString = documentRef.querySelector(`select[name="month"]`).value;
	if (monthString.length == 1) {
		monthString = '0' + monthString;
	}
	let dayString = documentRef.querySelector(`select[name="day"]`).value;
	if (dayString.length == 1) {
		dayString = '0' + dayString;
	}
	const yearString = documentRef.querySelector(`select[name="year"]`).value;
	return `${yearString}-${monthString}-${dayString}`;
}

// TODO: implement "not" from activation, and probably integrate activations with this
function checkCondition(condition) {
	if (condition.all) {
		for (conditionPart of condition.all) { // every subcondition must be met
			if (!checkCondition(conditionPart)) {
				return false; // return false if even one subcondition is incorrect
			}
		}
		return true;
	} else if (condition.selectedOptions) { // all options must be selected
		for (optId of condition.selectedOptions.options) {
			if (!document.querySelector(`[value="${optId}"`).checked) { // if not checked
				return false;
			} // otherwise, continue
		}
		return true; // if all options passed, then return true
	} else if (condition.orderDateIsBefore) { // current date must be before listed date
		if (new Date() < new Date(condition.orderDateIsBefore)) { // if before
			return true;
		} else {
			return false;
		}
	} else if (condition.orderDateIsBetween) { // current date must be between listed dates
		if (new Date() > new Date(condition.orderDateIsBetween.min) && new Date() < new Date(condition.orderDateIsBetween.max)) {
			return true;
		} else {
			return false;
		}
	} else if (condition.ageIsLess) { // age must be less than the given value
		/* TODO: Check for an off-by-one error here, and consider finding a
			simpler way to do this. */
		if (new Date(new Date() - new Date(getBirthDateString(document))).getYear() - 70 < condition.ageIsLess.age) {
			return true;
		} else {
			return false;
		}
	} else {
		throw new Error(`condition type not known, condition: ${JSON.stringify(conditionPart)}`);
		/* Among conditions not handled are "isFullPayment", which has nothing
			but a target in the condition object, and "equals", which has a tag
			and a value. In the waco form, equals is used to tread people as a
			minor if they are under 21 in the state of "MS", but the tag is
			"region" and it's not clear where the best place to get the tag name
			is. I suspect the answer may be elsewhere in the formData instead of
			in the HTML. */
	}
}

function checkTrigger(triggerId) {
	const trigger = formData.triggers.find(searchTrigger => searchTrigger.id == triggerId);
	return checkCondition(trigger.condition);
}

function applyModifier(startingValue, modifier) {
	/* In regex, '^' represents the beginning of the string, '\d' represents a
		digit, '+' means the previous character can be repeated any amount of
		times (but it must exist at least once), and '$' matches the end of a
		string. */
	let matchArr = null;
	if (matchArr = modifier.match(/^=(\d+)$/)) { // match exactly '=' then one or more digits
		return new Number(matchArr[1]);
	} else {
		throw new Error(`modifier string not recognized: ${modifier}`);
	}
}

/* string, number, numString, numString, numString, numString, numString, boolean */
function genRegItem(path, quantity, amount, discount, deposit, deductible, adjustment, startsToday) {
	// Different objects have different displayed orders, but that's not SUPPOSED to matter.
	return {
		"path": path,
		"quantity": Number(quantity),
		"amount": amount.toString(),
		"discount": discount.toString(),
		"deposit": deposit.toString(),
		"deductible": deductible.toString(),
		"adjustment": adjustment.toString(),
		"startsToday": startsToday
	}
}

function genInputBox(boxName) {
	const inputElem = document.createElement("input");
	inputElem.classList.add(`${boxName}`);
	inputElem.placeholder = boxName;
	return inputElem;
}

function fillInBillingSection() {
	const billingSection = document.getElementById("billing-section");
	const cardDiv = document.createElement("div");
	for (cardInput of ["cardNumber", "expMonth", "expYear", "cvvCode"]) {
		cardDiv.appendChild(genInputBox(cardInput));
	}
	billingSection.appendChild(cardDiv);
	const nameDiv = document.createElement("div");
	const nameHeader = document.createElement("h4");
	nameHeader.innerText = "name";
	for (nameInput of ["first", "last"]) {
		nameDiv.appendChild(genInputBox(nameInput));
	}
	billingSection.appendChild(nameDiv);
	const addressDiv = document.createElement("div");
	const addressHeader = document.createElement("h4");
	addressHeader.innerText = "address";
	for (addressInput of ["street1",/* "street2",*/ "city", "state", "postalCode", "country"]) {
		addressDiv.appendChild(genInputBox(addressInput));
	}
	billingSection.appendChild(addressDiv);
	billingSection.appendChild(genInputBox("email"));
	billingSection.appendChild(genInputBox("token"));
}

async function tokenize(userToken) {
	await fetch("https://precheck-02.webconnex.com/v1/tokenize", {
		"credentials": "include",
		"headers": {
//		    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Authorization": `Simple token="${userToken}"`,
		    "X-WBX-PAGE-URL": document.URL,
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "cross-site"
		},
//		"referrer": "https://redriver.events.intervarsity.org/",
		"body": JSON.stringify({
			"encrypted": null,
			"session": userToken
		}),
		"method": "POST",
		"mode": "cors"
	});
}
/* Response is JSON with a token. */

async function getBillingObjFromPage() {
	const appSettings = getJsonFromScriptString("appSettings: ");
	const billingSection = document.getElementById("billing-section");
	const userToken = getUserToken();
//	const tokenResp = await tokenize(userToken);
//	const token = tokenResp.token;
	const token = "PUT_TOKEN_HERE";
	let json = {
			"paymentMethod": "card",
			"paymentGatewayId": appSettings.gateways[0].id,
			"card": {
				"cardNumber": `CARD-${billingSection.getElementsByClassName("cardNumber")[0].value.substr(-4)}`, /* Mine said "MAST" before the number
					and the only place I could find "MAST" was in bundle.js, and
					it was in a switch statement with "CARD" as the default, so 
					I'm going to try just always sending that and see what
					happens. */
				"expMonth": Number(billingSection.getElementsByClassName("expMonth")[0].value), // should be a 2 digit number
				"expYear": Number(billingSection.getElementsByClassName("expYear")[0].value), // should be a 4 digit number
				"cvvCode": billingSection.getElementsByClassName("cvvCode")[0].value,
				"saveCard": false // TODO: allow setting true
			},
			"token": token/*billingSection.getElementsByClassName("token")[0].value*/,
			"name": {
				"first": billingSection.getElementsByClassName("first")[0].value,
				"last": billingSection.getElementsByClassName("last")[0].value
			},
			"address": {
				"street1": billingSection.getElementsByClassName("street1")[0].value,
				"city": billingSection.getElementsByClassName("city")[0].value,
				"state": billingSection.getElementsByClassName("state")[0].value, // should be 2 letter all-caps
				"postalCode": billingSection.getElementsByClassName("postalCode")[0].value,
				"country": billingSection.getElementsByClassName("country")[0].value // should be 2 letter allcaps string
			},
			"email": billingSection.getElementsByClassName("email")[0].value,
			"meta": {
				"riskToken": crypto.randomUUID()
			}
	};
	return json;
}

/* This function severly needs to be split apart and reorganized. */
async function getJsonPayloadFromPage() {
	const appSettings = getJsonFromScriptString("appSettings: ");
	const formData = getJsonFromScriptString("formData: ");
	let json = {};
	json.$type = "form";
	json.$key = "$form";
	let fields = [];
	let regItems = [];
	let totalPrice = 0;
	let items = [];
	let currentRegistrant = 0; /* Having to keep track of this seems werid, but
		I don't see a better way to do it. */
	for (const elem of formData.elements) {
		console.log("loading formData element");
		if (elem.tag == "billing") {
			continue; // billing is in a different section
		}
		if (elem.visible == false) {
			continue;
		}
		if (elem.type == "spacer" || elem.type == "paragraph" || elem.type == "location" || elem.type == "heading" || elem.type == "copyRegInfo" || elem.type == "lineBreak" || elem.type == "subtotal" || elem.type == "submit") {
			currentRegistrant++;
		} else if (elem.type == "banner") {
			continue; // no need to do anything for these, it seems
		} else if (elem.type == "regOptions" || elem.type == "multipleChoice" || elem.type == "multipleResponse") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			if (elem.type == "regOptions") {
				regItems.push(genRegItem(elem.legacyPath, 1, 0, 0, 0, 0, 0, true));
			}
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			field.$value = null;
			field.$options = [];
			for (opt of elem.options) {
				const optElem = document.querySelector(`[value="${opt.id}"`);
				if (optElem.checked) {
					field.$value = opt.legacyPath.split(".")[2];
					let optObj = {};
					if (elem.type == "multipleChoice") {
						optObj.$type = "multipleChoiceOption";
					} else {
						optObj.$type = field.$type;
					}
					optObj.$key = field.$value;
					optObj.$label = opt.label;
					optObj.$value = true;
					optObj.$description = opt.description;
					if (opt.computedPrice) {
						optObj.$amount = opt.computedPrice;
					} else {
						optObj.$amount = 0;
					}
					if (opt.priceModifiers) {
						for (mod of opt.priceModifiers) {
							if (checkTrigger(mod.when.trigger)) {
								optObj.$amount = applyModifier(optObj.$amount, mod.modifier);
							}
						}
					}
					if (opt.computedDiscount) {
						optObj.$discount = opt.computedDiscount;
					} else {
						optObj.$discount = 0;
					}
					if (opt.discountModifiers) { // just guessing on this one
						for (mod of opt.priceModifiers) {
							if (checkTrigger(mod.when.trigger)) {
								optObj.$discount = applyModifier(optObj.$discount, mod.modifier);
							}
						}
					}
					regItems.push(genRegItem(elem.legacyPath, 1, optObj.$amount, optObj.$discount, 0, 0, 0, true)); // TODO: know when to put a value here for deposit
					totalPrice = totalPrice + optObj.$amount - optObj.$discount;
					items.push({
						"amount": optObj.$amount.toString(),
						"deductible": "0",
						"discounted": optObj.$discount.toString(),
						"insurable": optObj.$amount.toString(),
						"description": optObj.$description,
						"source": [elem.id, opt.id],
						"context": []
					});
					field.$options.push(optObj);
				}
			}
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "name") {
			const currentRegistrantElem = document.querySelector(`.field-name.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			if (elem.collectMiddle) {
				throw new Error(`Middle name collection is not supported, sorry.`);
			}
			field.first = {
				"$type": "nameField",
				"$label": currentRegistrantElem.querySelector(".first").getAttribute("aria-label"),
				"$value": currentRegistrantElem.querySelector(".first").value
			}
			field.last = {
				"$type": "nameField",
				"$label": currentRegistrantElem.querySelector(".last").getAttribute("aria-label"),
				"$value": currentRegistrantElem.querySelector(".last").value
			}
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "textField") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			const inputElem = currentRegistrantElem.querySelector("input");
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			field.$value = inputElem.value;
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "email") {
			const currentRegistrantElem = document.querySelector(`.field-email.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			const emailInputElems = currentRegistrantElem.querySelectorAll(`input[type="email"]`);
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			if (elem.doubleEntry) {
				if (emailInputElems.length != 2) {
					throw new Error(`Unexpected number of email input boxes: ${emailInputElems.length}`);
				}
				if (emailInputElems[0].value != emailInputElems[1].value) {
					alert(`Emails ${emailInputElems[0].value} and ${emailInputElems[1].value} do not match!`);
					throw new Error(`Emails ${emailInputElems[0].value} and ${emailInputElems[1].value} do not match!`);
				}
			}
			field.$value = emailInputElems[0].value;
			if (elem.optInEnabled && currentRegistrantElem.querySelector(`input[type="checkbox"`).checked) {
				alert(`Sorry, checking the box nex to the email is not supported.`);
				throw new Error(`Sorry, checking the box nex to the email is not supported.`);
			}
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "phone") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			const inputElem = currentRegistrantElem.querySelector(`input[type="tel"]`);
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			let str = inputElem.value;
			str = str.replace(/\(|\)|-/g, ""); // remove parenthesis and dashes
			if (str.search(/\+/) == 0) { // if the string begins with +
				// remove every space except the first one
				strArr = str.split(' '); // split the string by spaces
				str = strArr.shift(); // make the string the first element
				str = str + ' ' + strArr.join(""); // add to the string a space and the rest of the array without spaces
			} else {
				str = str.replace(/\s/g, ""); // remove all whitespace
				if (str.search(/\+/) != -1) {
					throw new Error(`Uh, I think you have a plus sign in the wrong place. The phone number should not have leading spaces. If your phone number actually has plus signs in the middle, sorry.`);
				}
				str = "+1 " + str; // Add the +1, defaulting to US
				// TODO: use the default country specified in the formData element
				// TODO: Also consider using <https://github.com/google/libphonenumber>.
			}
			field.$value = str;
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "address") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			field.street1 = {
				"$type": "textField",
				"$label": "Street Address",
				"$value": currentRegistrantElem.querySelector("input.street1").value
			};
			if (currentRegistrantElem.querySelector("input.street2").value) {
				field.street1 = {
					"$type": "textField",
					"$label": "Street Address 2",
					"$value": currentRegistrantElem.querySelector("input.street2").value
				};
			}
			field.city = {
				"$type": "textField",
				"$label": "City",
				"$value": currentRegistrantElem.querySelector("input.city").value
			};
			if (currentRegistrantElem.querySelector("select.state")) {
				field.state = {
					"$type": "textField",
					"$label": "State",
					"$value": currentRegistrantElem.querySelector("select.state").value
				};
			}
			field.postalCode = {
				"$type": "textField",
				"$label": "ZIP/Postal Code",
				"$value": currentRegistrantElem.querySelector("input.zip.postalCode").value
			};
			field.country = {
				"$type": "textField",
				"$label": "Country",
				"$value": currentRegistrantElem.querySelector("select.country").value
			};
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "birthdate") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			let field = {};
			field.$type = "birthDate"; // 😠️
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			field.$value = getBirthDateString(currentRegistrantElem); // operating on the current registrant element instead of the document ensures we will get values from the right part of the form, if there are multiple places to enter a birtthday.
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "tos") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			field.$value = currentRegistrantElem.querySelector(`input[type="checkbox"]`).checked;
			field.$signatue = false;
			fields.push(field);
			currentRegistrant++;
		} else if (elem.type == "codeBox") {
			const currentRegistrantElem = document.querySelector(`.field-${elem.type}.field-registrant-${currentRegistrant}`);
			if (!currentRegistrantElem) {
				throw new Error(`The corresponding HTML element seems to be missing. registrant number: ${currentRegistrant} formData element JSON: ${JSON.stringify(elem)}`);
			}
			let field = {};
			field.$type = elem.type;
			field.$key = elem.legacyPath.split(".")[1];
			field.$label = elem.label;
			field.$value = currentRegistrantElem.querySelector(`input[type="text"]`).value;
		} else {
			throw new Error(`element type ${elem.type} not recognized, element: ${JSON.stringify(elem)}`);
		}
	}
	json.registrants = [{}];
	json.registrants[0].$fields = fields;
	json.registrants[0].$items = regItems;
	json.registrants[0].$total = totalPrice.toString;
	json.billing = await getBillingObjFromPage();
	json.version = appSettings.version;
	json.versionUpdatedAt = appSettings.versionUpdatedAt;
	json.director = "2"; // TODO: ???
	json.metrics = {}; /*{ "transactionDuration": 13248 // random number I made up, should be time in miliseconds spent on page
	};*/
	json.preferences = {};
	json.metadata = [];
	json.coupons = [];
	json.referrals = [];
	json.notifications = [];
	json.items = items;
	return json;
}

/* jsonPayload is quite compelex. */
async function register(userToken, jsonPayload) {
	await fetch("https://redriver.events.intervarsity.org/api/reg/register", {
		"credentials": "include",
		"headers": {
	//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0",
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Authorization": `Simple token="${userToken}"`,
		    "X-WBX-PAGE-URL": document.URL,
		    "X-WBX-CLIENT-VERSION": "1.104.44",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
	//    "referrer": "https://redriver.events.intervarsity.org/waco-spring-break-plunge",
		"body": JSON.stringify(jsonPayload),
		"method": "POST",
		"mode": "cors"
	});
}

/*
Convert a string into an ArrayBuffer
from https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
licensed as Apache-2.0
*/
function str2ab(str) {
  const buf = new ArrayBuffer(str.length);
  const bufView = new Uint8Array(buf);
  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}


// function modified from https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/importKey
// license likely CC0, but might be "MIT" <https://opensource.org/licenses/mit-license.php>
// seems like it was added in this commit from 2020: <https://github.com/mdn/content/commit/cbe151a06d6e5b4d1fbb46081bd16e69ef4c1630>
  function importRsaKey(pem) {
    // fetch the part of the PEM string between header and footer
    const pemHeader = "-----BEGIN PUBLIC KEY-----";
    const pemFooter = "-----END PUBLIC KEY-----";
    const pemContents = pem;
    // base64 decode the string to get the binary data
    const binaryDerString = window.atob(pemContents);
    const binaryDer = str2ab(binaryDerString);
    // convert from a binary string to an ArrayBuffer

    return window.crypto.subtle.importKey(
      "spki",
      binaryDer,
      {
        name: "RSA-OAEP",
        hash: "SHA-256"
      },
      true,
      ["encrypt"]
    );
  }

// function from modified https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/importKey
// license likely CC0, but might be "MIT" <https://opensource.org/licenses/mit-license.php>
// seems like it was added in this commit from 2020: <https://github.com/mdn/content/commit/cbe151a06d6e5b4d1fbb46081bd16e69ef4c1630>
  function importRsaKey(pem) {
    // fetch the part of the PEM string between header and footer
    const pemHeader = "-----BEGIN PUBLIC KEY-----";
    const pemFooter = "-----END PUBLIC KEY-----";
    const pemContents = pem;
    // base64 decode the string to get the binary data
    const binaryDerString = window.atob(pemContents);
    // convert from a binary string to an ArrayBuffer
    const binaryDer = str2ab(binaryDerString);

    return window.crypto.subtle.importKey(
      "spki",
      binaryDer,
      {
        name: "RSA-OAEP",
        hash: "SHA-256"
      },
      true,
      ["encrypt"]
    );
  }



// based on code from https://stackoverflow.com/a/53246072
// Copyright (C) saeid ezzati <https://stackoverflow.com/users/3539776/saeid-ezzati> licensed CC-BY-SA-4.0
function getArrayBufferFromCryptoKey(derivedKey) {
        return crypto.subtle.exportKey('raw',derivedKey)
        }

// from https://stackoverflow.com/a/53246072
// Copyright (C) saeid ezzati <https://stackoverflow.com/users/3539776/saeid-ezzati> licensed CC-BY-SA-4.0
function convertArrayBufferToHexaDecimal(buffer) 
{
    var data_view = new DataView(buffer)
    var iii, len, hex = '', c;

    for(iii = 0, len = data_view.byteLength; iii < len; iii += 1) 
    {
        c = data_view.getUint8(iii).toString(16);
        if(c.length < 2) 
        {
            c = '0' + c;
        }

        hex += c;
    }

    return hex;
}

const appSettings = getJsonFromScriptString("appSettings: ");
const formData = getJsonFromScriptString("formData: ");
const userToken = getUserToken();
const publicCryptoKey = await importRsaKey(appSettings.publicKey)

document.getElementById("root").querySelector("div.campaign-form").querySelector("div").style.removeProperty("display");

fillInBillingSection();

// TODO: repeatedly make the form consistent, based on "activation" properties in the form data elements

// TODO: implement copyRefInfo copy portions of the document to other portions
