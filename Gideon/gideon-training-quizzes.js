/*
gideon-training-quizzes
https://www.gideon.training/quizzes/*
e.g.
https://www.gideon.training/quizzes/class-basics/
*/

// SPDX-FileCopyrightText: 2023-2024 Jacob K
//
// SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions

// Quizzes are done by sending a series of POST form requests to https://www.gideon.training/wp-admin/admin-ajax.php

// It seems like all of the questions are already in the page HTML, and the randomization of question/answer order is done server-side.

// TODO: check that this is implemented: randomize question and answer order

// TODO: show instant feedback per question

// TODO: investigate <https://www.gideon.training/topic/intro-to-check-out/> to make sure the slideshows are working properly

// TODO: Is it possible to get 5/5 on <https://www.gideon.training/quizzes/phase-5-tg/>? One of the questions may be broken.

// TODO: Investigate <https://www.gideon.training/lessons/codebusters-letter-learners/>

// function from: https://stackoverflow.com/a/12646864
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function shuffleElemChildren(elem) {
	// modified from: https://stackoverflow.com/a/11972692
	for (let i = elem.children.length; i >= 0; i--) {
		elem.appendChild(elem.children[Math.random() * i | 0]);
	}
}

async function adminAjax(bodyString) {
	const response = await fetch("https://www.gideon.training/wp-admin/admin-ajax.php", {
		"credentials": "include",	
		"headers": {
//		    ":authority": "www.gideon.training", // `Uncaught (in promise) TypeError: Window.fetch: :authority is an invalid header name.`
//		    ":method": "POST", // `Uncaught (in promise) TypeError: Window.fetch: :method is an invalid header name.`
//		    ":path": "/wp-admin/admin-ajax.php", // `Uncaught (in promise) TypeError: Window.fetch: :path is an invalid header name.`
// 		    ":scheme": "https", // `Uncaught (in promise) TypeError: Window.fetch: :scheme is an invalid header name.`
		    "accept": "application/json, text/javascript, */*; q=0.01",
		    "accept-language": "en-US,en;q=0.9",
		    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
//		    "sec-ch-ua": "\"Microsoft Edge\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
		    "sec-ch-ua-mobile": "?0",
//		    "sec-ch-ua-platform": "\"Windows\"",
		    "sec-fetch-dest": "empty",
		    "sec-fetch-mode": "cors",
		    "sec-fetch-site": "same-origin",
//		    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.50",
		    "x-requested-with": "XMLHttpRequest"
		},
//		"referrer": "https://www.gideon.training/quizzes/booklet-basics/",
		"body": bodyString,
		"method": "POST",
		"mode": "cors"
	});
	const responseJson = await response.json();
	return responseJson;
}

/* form encoding function from papiro on stackoverflow
	(https://stackoverflow.com/questions/35325370/how-do-i-post-a-x-www-form-urlencoded-request-using-fetch)
	(https://stackoverflow.com/a/52205804) */
const formUrlEncode = str => {
  return str.replace(/[^\d\w]/g, char => {
    return char === " " 
      ? "+" 
      : encodeURIComponent(char);
  })
}

// In the official app, this call is done on page load
async function checkQuiz(quizId, quizNonce, quizNumber, courseId) {
	console.log(`Checking quiz with quizId ${quizId}, quizNonce ${quizNonce}, quizNumber ${quizNumber}, courseId ${courseId}.`);
	return await adminAjax(`action=wp_pro_quiz_check_lock&quizId=${quizId}&quiz=${quizNumber}&course_id=${courseId}&quiz_nonce=${quizNonce}`);
}
// `[]` is the normal return value

async function startQuiz(quizId, quizNonce, quizNumber, courseId) {
	console.log(`Starting quiz with quizId ${quizId}, quizNonce ${quizNonce}, quizNumber ${quizNumber}, courseId ${courseId}.`);
	return await adminAjax(`action=wp_pro_quiz_load_quiz_data&quizId=${quizId}&quiz_nonce=${quizNonce}&quiz=${quizNumber}&course_id=${courseId}`);
}

async function advanceQuiz(quizId, quizNonce, quizNumber, courseId, quizResponses) {
	console.log(`Advancing quiz with quizId ${quizId}, quizNonce ${quizNonce}, quizNumber ${quizNumber}, courseId ${courseId}, and quizResponses:`, quizResponses);
	return await adminAjax(`action=ld_adv_quiz_pro_ajax&func=checkAnswers&${formUrlEncode("data[quizId]")}=${quizId}&${formUrlEncode("data[quiz]")}=${quizNumber}&${formUrlEncode("data[course_id]")}=${courseId}&${formUrlEncode("data[quiz_nonce]")}=${quizNonce}&${formUrlEncode("data[responses]")}=${formUrlEncode(JSON.stringify(quizResponses))}&quiz=${quizNumber}&course_id=${courseId}&quiz_nonce=${quizNonce}`);
}

/* lessonId is quizWidget.quizJson.lesson_id. topicId is
	quizWidget.quizJson.topic_id. timeSpent is in seconds with 3 digits after
	the decimal point, I think. quizNonce is quizWidget.quizJson.quiz_nonce. */
async function completeQuiz(quizId, quizNonce, quizNumber, courseId, lessonId, topicId, quizResults, timeSpent) {
	console.log(`Completing quiz with quizId ${quizId}, quizNonce ${quizNonce}, quizNumber ${quizNumber}, courseId ${courseId}, lessonId ${lessonId}, topicId ${topicId}, timeSpent ${timeSpent}, and quizResults:`, quizResults);
	return await adminAjax(`action=wp_pro_quiz_completed_quiz&course_id=${courseId}&lesson_id=${lessonId}&topic_id=${topicId}&quiz=${quizNumber}&quizId=${quizId}&results=${formUrlEncode(JSON.stringify(quizResults))}&timespent=${timeSpent}&quiz_nonce=${quizNonce}`);
}

class quizWidget {
		elem;
		metaJson;
		quizJson;
		questionsArr = [];
		quizResponses = {};
		quizResults = {
			comp: {
				correctQuestions: 0,
				points: 0,
			}
		};
		possiblePoints = 0;

		constructor(quizElem) {
			this.elem = quizElem;
			this.metaJson = JSON.parse(this.elem.getAttribute("data-quiz-meta"));
			
			for (const script of document.scripts) {
				const matchArr = script.textContent.match(/jQuery\('#(.*)'\).wpProQuizFront\(({[^\)]*)/);
				if (matchArr && matchArr[1] == this.elem.id) {
					this.quizJson = JSON5.parse(matchArr[2]);
				}
			}
			
			const selfRef = this;
			
			// populate questionsArr
			let questionCount = 0;
			for (let questionElem of this.elem.querySelector(".wpProQuiz_quiz").querySelectorAll(".wpProQuiz_listItem")) {
				const questionObj = new question(questionElem, this);
				this.questionsArr.push(questionObj);
			}
			shuffleArray(this.questionsArr);
			
			this.elem.querySelector(".wpProQuiz_button[name=startQuiz]").addEventListener("click", function() {
				selfRef.showQuiz(); /* I'm using selfRef instead of this
					directly because the this keyword within an event refers to
					the element, not the object that added the event listener. */
			});
		};
		
		showQuiz() {
			this.elem.querySelector(".wpProQuiz_quiz").style.display = "revert";
			startQuiz(this.quizJson.quizId, this.quizJson.quiz_nonce, this.quizJson.quiz, this.quizJson.course_id);
			this.quizResults.comp.quizStartTimestamp = new Date().getTime();
			this.questionsArr[0].showQuestion();
		}
		
		async submitQuiz() {
			const percentScore = Math.round((this.quizResults.comp.points/this.possiblePoints)*100);
			this.quizResults.comp.cat = { 0: percentScore};
			this.quizResults.comp.quizEndTimestamp = new Date().getTime();
			this.quizResults.comp.quizTime = Math.round((this.quizResults.comp.quizEndTimestamp - this.quizResults.comp.quizStartTimestamp)/1000); // Should this be Math.round or Math.floor?
			this.quizResults.comp.result = percentScore;
			const quizCompleteJson = await completeQuiz(this.quizJson.quizId, this.quizJson.quiz_nonce, this.quizJson.quiz, this.quizJson.course_id, this.quizJson.lesson_id, this.quizJson.topic_id, this.quizResults, (this.quizResults.comp.quizEndTimestamp - this.quizResults.comp.quizStartTimestamp)/1000);
		}
		
		// At the end of a quiz, you can click "View Questions" to see all questions at once with correct answers highlighted
}

class question {
	elem;
	type;
	metaJson;
	quizObj;
	answers = [];
	questionStartTime;
	questionEndTime;
	
	constructor(questionElem, quizRef) {
		this.elem = questionElem;
		this.type = this.elem.getAttribute("data-type");
		this.metaJson = JSON.parse(this.elem.getAttribute("data-question-meta"));
		this.quizObj = quizRef;
	}
	
	// generate a json for the response to a particular question
	genQuestionResponseJson() {
		let questionRespJson = { 
			"question_post_id": this.metaJson.question_pro_id,
			"question_pro_id": this.metaJson.question_post_id
		}
		questionRespJson.response = {};
		if (this.type == "multiple" || this.type == "single") {
			for (const optionElem of this.elem.querySelectorAll("li.wpProQuiz_questionListItem")) {
				questionRespJson.response[optionElem.getAttribute("data-pos")] = optionElem.querySelector("input.wpProQuiz_questionInput").checked;
			}
		} else if (this.type == "matrix_sort_answer") {
			for (const answerLi of this.elem.querySelectorAll("li.wpProQuiz_questionListItem")) {
				questionRespJson.response[answerLi.querySelector("select").value] = answerLi.getAttribute("data-pos");
			}
		} else if (this.type == "sort_answer") {
			const selectElems = this.elem.querySelectorAll("select");
			for (let i = 0; i < selectElems.length; i++) {
				questionRespJson.response[i] = selectElems[i].value;
			}
		}
		return questionRespJson;
	}
	
	async checkQuestion() {
		this.questionEndTime = new Date().getTime();
	
		this.elem.querySelector("input.wpProQuiz_QuestionButton[name='check']").style.display = "none";
		/* this.metaJson.question_pro_id may not stay a string, which may be a
				problem if the server expects a string, but hopefully not. */
		const questionInfoJson = {};
		const questionResponseJson = this.genQuestionResponseJson();
		questionInfoJson[this.metaJson.question_pro_id] = questionResponseJson;
		
		
		
		
		const advanceJson = await advanceQuiz(this.quizObj.quizJson.quizId, this.quizObj.quizJson.quiz_nonce, this.quizObj.quizJson.quiz, this.quizObj.quizJson.course_id, questionInfoJson);
		const innerAdvanceJson = advanceJson[this.metaJson.question_pro_id];
		
		// JSON that will only get sent after the quiz is over
		const quizQuestionInfoJson = {};
		quizQuestionInfoJson.a_nonce = innerAdvanceJson.a_nonce;
		if (innerAdvanceJson.c == true) {
			quizQuestionInfoJson.correct = 1;
			this.quizObj.quizResults.comp.correctQuestions++;
		} else {
			quizQuestionInfoJson.correct = 0;
		}
		quizQuestionInfoJson.data = questionResponseJson.response;
		quizQuestionInfoJson.p_nonce = innerAdvanceJson.p_nonce;
		quizQuestionInfoJson.points = innerAdvanceJson.p;
		this.quizObj.quizResults.comp.points += innerAdvanceJson.p;
		quizQuestionInfoJson.possiblePoints = innerAdvanceJson.e.possiblePoints;
		this.quizObj.possiblePoints += innerAdvanceJson.e.possiblePoints;
		quizQuestionInfoJson.time = Math.round((this.questionEndTime - this.questionStartTime)/1000); // Should this be Math.round or Math.floor?
		this.quizObj.quizResults[this.metaJson.question_pro_id] = quizQuestionInfoJson;
		
		this.elem.querySelector("input.wpProQuiz_QuestionButton[name='next']").style.display = "revert";
		const selfRef = this;
		this.elem.querySelector("input.wpProQuiz_QuestionButton[name='next']").addEventListener("click", function() {
			selfRef.elem.style.display = "none";
			if (selfRef.quizObj.questionsArr[selfRef.quizObj.questionsArr.indexOf(selfRef) + 1]) {
				selfRef.quizObj.questionsArr[selfRef.quizObj.questionsArr.indexOf(selfRef) + 1].showQuestion();
			} else {
				selfRef.quizObj.submitQuiz();
			}
		});
	}
	
	showQuestion() {
		const selfRef = this;
	
		if (this.metaJson.type == "matrix_sort_answer"){
			for (let answerElem of this.elem.querySelectorAll(".wpProQuiz_sortStringItem")) {
				const answerObj = new answer(answerElem, this);
				this.answers.push(answerObj);
			}
			shuffleArray(this.answers);
			for (let subQuestionElem of this.elem.querySelectorAll("li.wpProQuiz_questionListItem")) {
				const ulElem = subQuestionElem.querySelector("ul.wpProQuiz_maxtrixSortCriterion");
				const newSelectElem = document.createElement("select");
				for (let answerObj of this.answers) {
					const newOptionElem = document.createElement("option");
					newOptionElem.value = answerObj.dataPos;
					newOptionElem.innerText = answerObj.text;
					newSelectElem.appendChild(newOptionElem);
				}
				ulElem.appendChild(newSelectElem);
			}
		} else if (this.metaJson.type == "sort_answer") {
			for (let answerElem of this.elem.querySelectorAll("li.wpProQuiz_questionListItem")) {
				const answerObj = new answer(answerElem, this);
				this.answers.push(answerObj);
			}
			shuffleArray(this.answers);
			for (let answerElem of this.elem.querySelectorAll("li.wpProQuiz_questionListItem")) {
				const answerDiv = answerElem.querySelector("div.wpProQuiz_sortable");
				answerDiv.innerText = "";
				const selectElem = document.createElement("select");
				for (let answerObj of this.answers) {
					const newOptionElem = document.createElement("option");
					newOptionElem.value = answerObj.dataPos;
					newOptionElem.innerText = answerObj.text;
					selectElem.appendChild(newOptionElem);
				}
				answerDiv.appendChild(selectElem);
			}
		} else if (this.metaJson.type == "multiple" || this.metaJson.type == "single") {
			shuffleElemChildren(this.elem.querySelector("ul.wpProQuiz_questionList"));
		}
		
		this.questionStartTime = new Date().getTime();
		
		this.elem.style.display = "revert";
		// Should show "Check" button and then clicking that checks the answer and replaces itself with the "Next" button
		this.elem.querySelector("input.wpProQuiz_QuestionButton[name='check']").style.display = "revert";
		this.elem.querySelector("input.wpProQuiz_QuestionButton[name='check']").addEventListener("click", function() {
			selfRef.checkQuestion();
		});
		// There is no "Back" button normally
	}
	
}

class answer {
	elem;
	questionObj;
	origClass;
	dataPos;
	text;
	
	constructor(elem, questionObj) {
		this.elem = elem;
		this.questionObj = questionObj;
		this.origClass = this.elem.className;
		this.dataPos = this.elem.getAttribute("data-pos");
		this.text = this.elem.innerText;
	}
}

const quizObjArr = [];

for (quizElem of document.querySelectorAll(".wpProQuiz_content")) {
	const quizObj = new quizWidget(quizElem);
	quizObjArr.push(quizObj);
}
