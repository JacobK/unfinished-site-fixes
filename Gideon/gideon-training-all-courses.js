/*
gideon-training-all-courses
https://www.gideon.training/all-courses/
*/

// SPDX-FileCopyrightText: 2024 Jacob K
//
// SPDX-License-Identifier: GPL-3.0-or-later

document.querySelector("div.uo-grid-wrapper.uo-clear-all").style.display = "revert";
