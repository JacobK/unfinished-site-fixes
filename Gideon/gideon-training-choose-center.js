/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright 2023 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
gideon-training-choose-center
https://www.gideon.training/choose-center/
*/

/* The following function is based on code from
	docs-google-com-fix-sheets-display by Wojtek Kosior <koszko@koszko.org>
	and a Stack Overflow answer <https://stackoverflow.com/a/7053514> by
	user113716 <https://stackoverflow.com/users/113716/user113716>.
		This function gets a JSON object from an inline script, and the input
	is a string directly preceeding the JSON you want. So, for example, if you
	want the JSON value assigned like "varName = {...}, then the input would be
	"varName = ". */
function getJsonFromScript(leadingText) {
	let jsonObj = null;
	const regEx = new RegExp(`${leadingText}([^;]*);`); /* Will
		regex search for the leadingText value and then anything directly after.
		Assumes there is a trailing semicolon. */
	for (const script of document.scripts) {
		const matchArr = script.textContent.match(regEx);
		if (!matchArr)
		continue;
		try {
			jsonObj = JSON.parse(matchArr[1]);
		} catch (err) {
			console.error(err);
			return null;
		}
	}
	return jsonObj;
}

const linkData = getJsonFromScript("var et_link_options_data = ");
for (const linkInfo of linkData) {
	document.getElementsByClassName(linkInfo.class)[0].addEventListener("click", function() {
		/* It seems like it should be easy to do this without requring event
			listeners, using "a" tags that contain links, but apparently it's
			not so easy: <https://stackoverflow.com/questions/13389751/change-tag-using-javascript>
			The primary reason to avoid event listeners would be so that a
			non-scripting browser could parse HTML that has been pre-processed
			by a script like this. */
		document.location.assign(linkInfo.url);
	});
}
