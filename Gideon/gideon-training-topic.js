/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright 2023 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
gideon-training-topic
https://www.gideon.training/topic/*
https://www.gideon.training/
*/

// fix invisible (white-on-white) text
for (elem of document.querySelectorAll(".et_pb_bg_layout_dark, h1, h2, h3, h4, h5, h6")) { // might miss some important text
	elem.style = "color: black !important;";
}

// function to hide all tabs, typically in preparation to show just one tab
function hideAllTabs(controlsElem, tabsElem) {
	for (elem of controlsElem.querySelectorAll(".et_pb_tab_active")) {
		elem.classList.remove("et_pb_tab_active");
	}
	for (elem of tabsElem.querySelectorAll(".et_pb_active_content")) {
		elem.classList.remove("et_pb_active_content");
	}
}

// enable switching between tabs
for (tabsModule of document.querySelectorAll(".et_pb_module.et_pb_tabs")) {
	const controlsElem = tabsModule.querySelector(".et_pb_tabs_controls");
	const tabsElem = tabsModule.querySelector(".et_pb_all_tabs");
	for (elem of controlsElem.querySelectorAll("li")) {
		elem.addEventListener("click", function(ev){
			hideAllTabs(controlsElem, tabsElem);
			const onlyClass = ev.target.parentElement.className; // assumes there is only one class
			const tabIndex = onlyClass.split("_")[onlyClass.split("_").length - 1];
			ev.target.parentElement.classList.add("et_pb_tab_active");
			tabsElem.querySelector(`.et_pb_tab_${tabIndex}`).classList.add("et_pb_active_content");
			ev.preventDefault();
		});
	}
}

// auto-open all accordian dropdowns
for (accordianModule of document.querySelectorAll(".et_pb_module.et_pb_accordion")) {
	for (elem of accordianModule.querySelectorAll(".et_pb_toggle_close")) {
		elem.classList.remove("et_pb_toggle_close");
		elem.classList.add("et_pb_toggle_open");
	}
}

for (slidesModule of document.querySelectorAll("div.et_pb_slides")) {
	for (slideDiv of slidesModule.querySelectorAll("div.et_pb_slide")) {
		slideDiv.querySelector("img").style.opacity = 1;
		// TODO: figure out how to make images show up completely
		const buttonElem = document.createElement("button");
		buttonElem.innerText = "NEXT SLIDE";
		slideDiv.appendChild(buttonElem);
		buttonElem.addEventListener("click", function(ev) {
			ev.target.parentElement.remove();
		});
	}
}
