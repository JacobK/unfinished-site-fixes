/*
	identifier: craiyon-img
	URL pattern: https://www.craiyon.com/
*/

let lastResponseValue = null;
const imgEncodingString = "data:image/png;base64,";
// imgContainerDiv is the container that holds all 9 images
let imgContainerDiv = document.getElementById("app").querySelector("div").querySelector("div").querySelector("div.items-center");

document.body.style = "display: revert !important;";

// I do not understand CORS
function generate(prompt) {
	fetch("https://backend.craiyon.com/generate", {
		"credentials": "omit",
		"headers": {
		    "Accept": "application/json",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-site",
		},
		"body": JSON.stringify({
			"prompt": prompt
		}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response) {
		const responseJson = await response.json();
		lastResponseValue = responseJson;
		if (responseJson.images) {
			imgContainerDiv.className = "grid grid-cols-3 gap-2";
			for (imageString of responseJson.images) {
				let newDiv = document.createElement("div");
				newDiv.className = "aspect-w-1 aspect-h-1";
				let newImg = document.createElement("img");
				newImg.className = "h-full w-full rounded-lg border border-gray-500 object-cover object-center hover:ring-4 hover:ring-orange-400";
				newImg.alt = prompt;
				newImg.src = imgEncodingString + imageString;
				newDiv.appendChild(newImg);
				imgContainerDiv.appendChild(newDiv);
			}
		}
	});
}
