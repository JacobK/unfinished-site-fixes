/*
	identifier: pariconnect-demographics
	long name: Demographics for PariConnect
	description: enable PariConnect demographic form
	URL pattern: https://iadmin.pariconnect.com/Assessment/Demographics
*/

/* TODO: actually allow editing the inputs that should be editable (which is
	only a few of them, and saving each box seems to make that box
	uneditable...) */

const requestVerificationToken = document.getElementsByName("__RequestVerificationToken")[0].value; // or document.getElementsByTagName("input")[0].value

let demoKey = null;
for (const script of document.scripts) {
	const match = /Demo.Key = (\d*);/.exec(script.textContent); // look for  "Demo.Key = " and then grab digits until ";".
	if (match) {
		demoKey = parseInt(match[1]);
	}
}

let cancelUrl = null; // URL to go to if one wants to cancel the form
for (const script of document.scripts) {
	const match = /Demo.onCancel = function\(\) { window.location.href = '([^']*)'; }/.exec(script.textContent); /* 
		TODO: Can a better match regex be used here? This is pretty long but we
		don't want to grab just any assignment of window.location.href. */
	if (match) {
		cancelUrl = match[1];
	}
}

let continueUrl = null; // URL to continue to after a successful save of data
for (const script of document.scripts) {
	const match = /if \(rsp.ErrorCode == 0\)\n\t\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\twindow.location.href = '([^']*)';\n\t\t\t\t\t\t\t}/.exec(script.textContent); /* 
		TODO: Can a better match regex be used here? This is pretty long but we
		don't want to grab just any assignment of window.location.href. */
	if (match) {
		continueUrl = match[1];
	}
}

let lastResponseValue = null; // for debugging

// function to get the current demo data (i.e. inputs) from the page
function getDemoData(idxType) {
	// get information from the form and put it into the demo_data array
	let demoData = [];
	let idx = 0;
	/* iterate through idx values, continuing until we reach an idx value with
		no elements */
	while (document.querySelectorAll(`[${idxType}="${idx}"]`).length) {
		/* if-then list for different kinds of data (We check for length of 1
			when we expect length to be 1 because I don't want to miss important
			data. */
		if ((document.querySelectorAll(`[${idxType}="${idx}"]`)[0].type == "text" || document.querySelectorAll(`[${idxType}="${idx}"]`)[0].type == "hidden") && document.querySelectorAll(`[${idxType}="${idx}"]`).length == 1) { // if (type "text" or "hidden") and length 1
			demoData.push(document.querySelectorAll(`[${idxType}="${idx}"]`)[0].value); // simply add the value to the array
		} else if (document.querySelectorAll(`[${idxType}="${idx}"]`)[0].type == "radio") { // if radio type
			let val = null;
			/* check all the radio buttons, to find the one to submit and to
				make sure only 1 is checked */
			for (const input of document.querySelectorAll(`[${idxType}="${idx}"]`)) {
				if (input.checked == true) {
					if (val == null) {
						val = input.value;
					} else {
						return `Error: multiple selected radio buttons at idx ${idx}!`;
					}
				}
			}
			demoData.push(val);
		} else if (document.querySelectorAll(`[${idxType}="${idx}"]`)[0].type == "select-one" && document.querySelectorAll(`[${idxType}="${idx}"]`).length == 1) { // if dropdown type and length 1
			let val = null;
			/* check all the options, to find the one to submit and to
				make sure only 1 is checked */
			for (const option of document.querySelectorAll(`[${idxType}="${idx}"]`)[0].children) {
				if (option.selected == true) {
					if (val == null) {
						val = option.value;
					} else {
						return `Error: multiple selection options at idx ${idx}!`;
					}
				}
			}
			demoData.push(val);
		} else if (document.querySelectorAll(`[${idxType}="${idx}"]`)[0].type == "number" && document.querySelectorAll(`[${idxType}="${idx}"]`).length == 1) { // if number type and length 1 (same code as for type "text")
			demoData.push(document.querySelectorAll(`[${idxType}="${idx}"]`)[0].value); // simply add the value to the array
		} else if (document.querySelectorAll(`[${idxType}="${idx}"]`)[0].type == "checkbox" && document.querySelectorAll(`[${idxType}="${idx}"]`).length == 1) { // if checkbox type and length 1
			// if checked, add 1 to the array, and if not add 0
			if (document.querySelectorAll(`[${idxType}="${idx}"]`)[0].checked == true) {
				demoData.push(1);
			} else {
				demoData.push(0);
			}
		} else { /* If we don't know how to handle this data, throw an error,
				and do not submit the form. */ 
			return `Error at idx ${idx}!`; // ERROR!
		}
		idx++;
	}
	return demoData;
}

/* function to send data to the server, after calling functions to get the data
	from the page. */
function saveDemoData() {
	const demoData = getDemoData("data-demo-idx");
	const testInfo = getDemoData("data-info-idx");

	fetch("https://iadmin.pariconnect.com/Assessment/SaveDemoData", {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json; charset=utf-8",
		    "X-RequestVerificationToken": requestVerificationToken,
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": JSON.stringify({
			"key": demoKey,
			"demo_data": demoData,
			"test_info": testInfo,
			"rater_name": null // TODO: understand what this means
		}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response) {
		const responseJson = await response.json();
		lastResponseValue = responseJson;
		if (responseJson.ErrorCode == 0) {
			window.location.href = continueUrl;
		} else { 
			console.error("There's been an error!", `Error code: ${responseJson.ErrorCode}`, `Error message: ${responseJson.ErrorMsg}`);
		}
	});
}

// hook functions to buttons
document.getElementById("CancelBtn").addEventListener("click", function() {
	window.location.href = cancelUrl;
});
document.getElementById("SubmitBtn").addEventListener("click", saveDemoData);
