/*
	identifier: pariconnect-assess
	long name: Assess for PariConnect
	description: enable PariConnect assessment form
	URL pattern: https://iadmin.pariconnect.com/Assessment/Assess
*/

/* Fun fact: If you disable JavaScript and CSS, then you immediately get an
	"Assessment is Complete!" message even if you haven't started yet. */

/* The assessment progress is stored using "Local Storage". As a consequence of
	this, people who want to prevent others using the same computer from seeing
	their answers might want to use Private Browsing mode. */

/* TODO: warn before refreshing (not really necessary IMO since it saves to
	cookies often). */

const urlParams = new URLSearchParams(window.location.search);
const arg = urlParams.get("arg");
const localStorageLocation = `Admin:${arg}`;
let localStorageValue = JSON.parse(localStorage.getItem(localStorageLocation));
if (!localStorageValue) {
	localStorageValue = {"currSec": 0, "currItem": 0, "responses": [], "secTime": []};
}

const requestVerificationToken = document.getElementsByName("__RequestVerificationToken")[0].value;

let questionsArray = null; // store questions from getQuestions

let lastRessponseValue = null; // for debugging

function getQuestions() {
	fetch("https://iadmin.pariconnect.com/AssessmentContent/StimCache/PAI.stim.js?2018-10-22", {
		"credentials": "include",
		"headers": {
		    "Accept": "*/*",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Sec-Fetch-Dest": "script",
		    "Sec-Fetch-Mode": "no-cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"method": "GET",
		"mode": "cors"
	}).then(async function(response){
		responseText = await response.text();
		lastRessponseValue = responseText;
		// bad way of cutting the string, and bad way of using global variables
		questionsArray = JSON.parse(lastRessponseValue.split("PAR.stimData=")[1].split(";")[0].replaceAll("'", '\"'));
	});
}

// get the current question string
function getCurrentQuestionString() {
	return decodeURIComponent(atob(questionsArray[localStorageValue.currItem]));
}

// print the current question string
function printCurrentQuestionString() {
	console.info("Current Question:", getCurrentQuestionString());
	qDisp.innerText = getCurrentQuestionString();
}

// print the current question string and the answer choices
function printCurrentQuestion() {
	printCurrentQuestionString();
	console.info(`Answer using advance(1) for "False", advance(2) for "Slightly True", advance(3) for "Mainly True", or advance(4) for "Very True".`);
	if (localStorageValue.currItem < localStorageValue.responses.length) {
		console.info(`Your current answer for this question is ${localStorageValue.responses[localStorageValue.currItem]}.`);
	}
}

// save the localStorageValue to actual localStorage
function save() {
	localStorage.setItem(localStorageLocation, JSON.stringify(localStorageValue));
}

// answer the question and advance to the next one
function advance(answerChoice) {
	if (answerChoice != 1 && answerChoice != 2 && answerChoice != 3 && answerChoice != 4) {
		console.error("That is an invalid choice. Try again.");
		printCurrentQuestion();
		return;
	} else if (localStorageValue.currItem >= questionsArray.length) {
		console.error("There are no more questions left to answer!");
	} else {
		console.info(`You answered choice ${answerChoice}!`);
		localStorageValue.responses[localStorageValue.currItem] = answerChoice;
		localStorageValue.currItem++; // go to the next question
		save();
		if (localStorageValue.currItem < questionsArray.length) { // if still questions left
			printCurrentQuestion();
		} else {
			console.info("You are done with all of the questions! Now call finish() to save for good.");
		}
	}
}

// go back a question
function previous() {
	localStorageValue.currItem--;
	save();
	printCurrentQuestion();
}

// jump to a particular question
function jump(questionNumber) {
	if (questionNumber < 1 || questionNumber > questionsArray.length) {
		console.error("That's an invalid question number!");
	} else if (questionNumber > localStorageValue.responses.length + 1) {
		console.error("You cannot jump beyond the first question you haven't answered!");
	} else {
		localStoraeValue.currItem = questionNumber - 1; // because of arrays starting at index 1
		save();
		printCurrentQuestion();
	}
}

// shorter start command
function start() {
	printCurrentQuestion();
}

/* There are 4 choices for each question and I think line 278
	(`PAR.adminCfg.rspSet=[new PAR.RspSet(0,null,null,null,PAR.adminCfg.rspUI[0],true,null,[new PAR.Rsp("False","1","1Ff"),new PAR.Rsp("Slightly True","2","2Ss"),new PAR.Rsp("Mainly True","3","3Mm"),new PAR.Rsp("Very True","4","4Vv")],null)];`)
	describes them, but getting that data seems hard so I'll probably hardcode
	it... */

function saveResponseData() {
	if (localStorageValue.responses.length != questionsArray.length) {
		console.error("The number of questions you answered does not match the number of questions that exist.");
		return;
	}
	fetch("https://iadmin.pariconnect.com/Assessment/SaveResponseData", {
		"credentials": "include",
		"headers": {
		    "Accept": "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language": "en-US,en;q=0.5",
		    "Content-Type": "application/json; charset=utf-8",
		    "X-RequestVerificationToken": requestVerificationToken,
		    "X-Requested-With": "XMLHttpRequest",
		    "Sec-Fetch-Dest": "empty",
		    "Sec-Fetch-Mode": "cors",
		    "Sec-Fetch-Site": "same-origin"
		},
		"body": JSON.stringify({
			"token": arg,
			"final": true,
			"rsps": localStorageValue.responses
		}),
		"method": "POST",
		"mode": "cors"
	}).then(async function(response) {
		const responseJson = await response.json();
		lastResponseValue = responseJson;
		if (responseJson.ErrorCode == 0) {
			console.info("Success! You have finished the survey!");
			// TODO: also show the big success element already included in the HTML
		} else {
			console.error(`Error code: ${responseJson.ErrorCode} Error message: ${responseJson.ErrorMsg}`);
		}
	});
}

function finish() {
	saveResponseData();
}


getQuestions();

// wait a bit due to the time to convert from response to response text

console.info("To go back to a previous question, you can use the command previous() or jump(#), where # represents the question number you want to jump to. To start, from the beginning or from where you left off, use start()."); 

// question display
let qDisp = document.createElement("h3");
document.body.appendChild(qDisp);

// buttons to control the console
let b1 = document.createElement("button");
b1.innerText = "False";
b1.addEventListener("click", function(){ advance(1); });
document.body.appendChild(b1);

let b2 = document.createElement("button");
b2.innerText = "Slightly True";
b2.addEventListener("click", function(){ advance(2); });
document.body.appendChild(b2);

let b3 = document.createElement("button");
b3.innerText = "Mainly True";
b3.addEventListener("click", function(){ advance(3); });
document.body.appendChild(b3);

let b4 = document.createElement("button");
b4.innerText = "Very True";
b4.addEventListener("click", function(){ advance(4); });
document.body.appendChild(b4);

/* TODO: more UI stuff (actually shouldn't be that hard to more or less finish
	up a usable version of this script) */
